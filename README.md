charistools
---
---

## Development Quickstart

This project uses [conda](http://conda.pydata.org/miniconda.html) python. It must be installed to run the tool.

1. Clone the repository
2. `cd charistools/source`
3. `conda env create --file environment.yaml`
4. `. activate charistools`

## Development/Release Workflow

This project uses [github flow](https://guides.github.com/introduction/flow/). To begin a feature:

1. start a new branch
2. from the directory with the .bumpversion.cfg file, update the version.  Do a dry run to check it with `bumpversion --dry-run --verbose patch --allow-dirty` and repeat the command without the --dry-run option.  Version can be bumped for [major|minor|patch].
This will allow you to set the version for the expected next release.
3. Make changes, add unit tests, run tests with `fab test.all` etc etc
4. Make a pull request, merge branch back to master, this will kick off CI (see below)
5. Make a release candidate (see below)
6. Make new release (see below)

### Continuous Integration

On a merge to master the CI jobs will run a do the following:

1. `CI` Clone repository and checkout reference (default: `master`)
2. `CI` Create build a `dev` conda package, that is labeled {version}dev
3. `CI` Deploy the package to anaconda.org (or local_nsidc) on the `dev` channel
4. `CI` Provision an `Integration` VM
5. `CI` Install the `dev` package to the `Integration` VM
6. `CI` Run all tests for the package on `Integration`.

When a package is ready to be tested. Manually run the first `RC` job triggering this chain:

1. `RC` Clone repository and checkout reference (default: `master`)
2. `RC` Create build a conda package, that is labeled {version}
3. `RC` Deploy the package to anaconda.org (or local_nsidc) on the `dev` channel
4. `RC` Provision an `QA` VM
5. `RC` Install the `RC` package to the `QA` VM
6. `RC` Run all tests for the package on `QA`.

### Create A Release

When satisfied with the testing of the RC job, and are ready to create a production release. From Jenkins manually run the first RELEASE job.
The following will run:

1. `RELEASE` Clone repository and Checkout TAG to release
2. `RELEASE` Create build a conda package, labeled {version}
3. `RELEASE` Deploy the package to anaconda.org (or local_nsidc) on the `main` channel
4. `RELEASE` Clean up all `dev` packages of this release on the `dev` channel.

### Use the package

Once the package has been deployed to anaconda.org, it can be used with

    `conda install -c 'https://conda.anaconda.org/nsidc/channel/main' charistools=0.1.6`

The "=0.1.6" specifies a particular version, and is optional.  Without it, you
get the latest version.

It is more convenient to add the NSIDC channel to your conda configuration, by running:

    `conda config --add channels 'https://conda.anaconda.org/nsidc/channel/main'

This adds the NSIDC/main channel to the $HOME/.condarc file.

To install the package after adding the channel, the -c option to `conda install` is not needed.


### Ending A Project

An IMPLODE job has been provided and should be used when the package work is completed. The job will attempt to destroy all of the vm's and the notify you to destroy the CI vm.
