hiera_include('classes')

# Add this package to get libGL.so.1 that appears to be
# needed by python's PyQt5 imports that are needed at runtime
# by fab test.all, this seems to fix this error
# ImportError: libGL.so.1: cannot open shared object file: No such file or directory
package { 'libgl1-mesa-glx':
  ensure => present,
}
