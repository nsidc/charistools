classes:
  - nsidc_jenkins

nsidc_miniconda::install:
  /opt/miniconda:
    build: true

nsidc_nfs::sharemounts:
  /share/sw/packages:
    project: sw
    share: packages

nsidc_jenkins::use_vagrant: true

nsidc_jenkins::jobs:

# Integration

  "%{hiera('project')}_1_CI_1_Checkout":
    git:
      repo: "%{hiera('gitrepo')}"
      poll_scm: true
    parameters:
      - type: string
        name: ref
        description: git ref (branch, tag, commit SHA) to checkout
        default: master
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/ci
    description: Clone the repository and checkout ref.
    command: git checkout $ref
    trigger_job: "%{hiera('project')}_1_CI_2_Build_Snapshot"
    trigger_threshold: SUCCESS

  "%{hiera('project')}_1_CI_2_Build_Snapshot":
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/ci/source
    description: Build a snapshot conda package on the CI VM.
    command: |
      . /etc/profile.d/miniconda.sh
      fab build:recipe,dev
    trigger_job: "%{hiera('project')}_1_CI_3_Deploy_Snapshot"
    trigger_threshold: SUCCESS

  "%{hiera('project')}_1_CI_3_Deploy_Snapshot":
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/ci/source
    description: Deploy the snapshot conda package.
    command: |
      . /etc/profile.d/miniconda.sh
      export ANACONDA_TOKEN=`sudo cat /home/vagrant/anaconda/anaconda.token`
      fab deploy.assert_new_package:main,%{hiera('project')}
      fab deploy.anaconda:dev,${ANACONDA_TOKEN}
    trigger_job: "%{hiera('project')}_1_CI_4_Provision_Integration"
    trigger_threshold: SUCCESS

  "%{hiera('project')}_1_CI_4_Provision_Integration":
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/ci
    description: (Re)build the integration VM.
    command: |
      rm -rf .vagrant-integration
      vagrant nsidc hijack --env=integration || true
      vagrant nsidc destroy --env=integration || true
      vagrant nsidc up --env=integration
    trigger_job: "%{hiera('project')}_1_CI_5_Install_on_Integration"
    trigger_threshold: SUCCESS

  "%{hiera('project')}_1_CI_5_Install_on_Integration":
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/ci
    description: Install the snapshot conda package on the integration VM.
    command: |
      export PKG_VERSION=`cat source/__conda_version__.txt`
      uninstall="conda uninstall -q %{hiera('project')} || true"
      clean="conda clean --all || true"
      install="conda install -q -c nsidc/channel/dev %{hiera('project')}==${PKG_VERSION}"
      vagrant nsidc ssh --env=integration -c "$uninstall && $clean && $install"
    trigger_job: "%{hiera('project')}_1_CI_6_Run_All_Tests_on_Integration"
    trigger_threshold: SUCCESS

  "%{hiera('project')}_1_CI_6_Run_All_Tests_on_Integration":
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/ci
    description: Run all package tests on the integration VM.
    command: |
      vagrant nsidc ssh --env=integration -c "python -c 'from charistools.hypsometry import Hypsometry'"

# Release

  "%{hiera('project')}_2_RELEASE_1_Checkout":
    git:
      repo: "%{hiera('gitrepo')}"
      poll_scm: false
    parameters:
      - type: string
        name: ref
        description: git tag to checkout
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/release
    description: Clone the repository and checkout ref.
    command: git checkout $ref
    trigger_job: "%{hiera('project')}_2_RELEASE_2_Build_Package"
    trigger_threshold: SUCCESS

  "%{hiera('project')}_2_RELEASE_2_Build_Package":
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/release/source
    description: Build a release conda package on the CI VM.
    command: |
      . /etc/profile.d/miniconda.sh
      fab build:${WORKSPACE}/recipe,""
    trigger_job: "%{hiera('project')}_2_RELEASE_3_Deploy_Package"
    trigger_threshold: SUCCESS

  "%{hiera('project')}_2_RELEASE_3_Deploy_Package":
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/release/source
    description: Remove any prior conda packages, and deploy the new release package.
    command: |
      . /etc/profile.d/miniconda.sh
      export ANACONDA_TOKEN=`sudo cat /home/vagrant/anaconda/anaconda.token`
      fab clean.remove_dev_packages:%{hiera('project')},$ANACONDA_TOKEN
      fab deploy.assert_new_package:main,%{hiera('project')}
      fab deploy.anaconda:main,${ANACONDA_TOKEN}
    trigger_threshold: SUCCESS

# Cleanup

  "%{hiera('project')}_3_CLEANUP_Destroy_VMs":
    git:
      repo: git@bitbucket.org:nsidc/gsx-vm.git
    workspace: /var/lib/jenkins/workspaces/%{hiera('project')}/cleanup
    description: Clean up project (destroy all the VMs).
    command: |
      for env in integration qa staging blue; do
        rm -rf .vagrant-$env
        vagrant nsidc hijack --env=$env || true
        vagrant nsidc destroy --env=$env || true
      done
      echo 'When this job completes, you can destroy the ci environment from'
      echo 'your workstation with the commands:'
      echo ' %  rm -rf .vagrant-ci'
      echo ' %  vagrant nsidc hijack --env=ci'
      echo ' %  vagrant nsidc destroy --env=ci'
