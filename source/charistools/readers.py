#!/usr/bin/env python
"""Library for reading CHARIS melt modelling files.

The CHARIS melt modelling uses remote sensing data from many
different sources.  The modelling assumes that input data will be
projected into sinusoidal MODIS 500m tiles.  This module provides
simple readers for raster format MODIS tile data.

Use the ModisTileCube class to open and read MODIS tiled data
that has a date component, like mod10a1_gf, albedo_mod10a1 and
temperature data that are in HDF5 format (file extensions .h5).

Use the simple read_tile function to read a single MODIS tile
data, like the basin_masks, dems and modice_min05yr data.
read_tile uses the file extension (.bin, .tif, .nc, .hdf) to
decide how to read the data.

"""

from __future__ import print_function

from netCDF4 import Dataset   # noqa
import numpy as np   # noqa
import os   # noqa
import rasterio   # noqa
import re  # noqa
import sys   # noqa

from modelEnv import ModelEnv   # noqa


class ModisTileCube():
    """ModisTileCube class reads 3-dimensional MODIS tile .h5 data files.

    MODIS tile yearly cube files can be very large (2400 rows x
    2400 cols x 365 days).  This class provides an efficient
    interface for opening the cube file and only reading the tile
    for a specific day at a time.

    Public attributes:

    - filename: The name of the .h5 tile cube file
    - f: the Dataset pointer
    - d: the variable pointer
    - ndays: the number of days in the data cube
    - nrows: the number of rows in the data cube
    - ncols: the number of cols in the data cube

    """
    filename = None
    f = None
    d = None
    ndays = None
    nrows = None
    ncols = None

    def __init__(self, filename, varname, groupname='500m'):
        """Initialize a tile object to the group/variable in the file.

        Opens filename and creates Dataset and variable pointers to
        the requested variable.  Tiles for a specific day-of-year can
        then be read efficiently using the read method.

        Raises any errors encountered in the HDF file open function.

        Args:
          filename: string name of CHARIS HDF5 data cube file to
          read.

          groupname: string groupname to access in the file.

          varname: string variable to access in the file.

        Returns:
          Initialized object ready to read a daily tile layer.

        """
        self.filename = filename
        try:
            self.f = Dataset(filename, 'r', 'HDF5')
        except RuntimeError:
            print("%s : Error opening %s" % (__name__, filename),
                  file=sys.stderr)
            raise

        # if input data are "MODSCAG_GF_Snow.v0.8", and varname is 'fsca'
        # then get scf variable instead
        p = re.compile(r"MODSCAG_GF_Snow.v0.8")
        if p.search(filename):
            if varname == 'fsca':
                varname = 'scf'

        self.d = self.f.groups[groupname].variables[varname]
        (self.ndays, self.nrows, self.ncols) = self.d.shape

    def read(self, doy):
        """Reads the tile for a day-of-year.

        If data are stored with scale/offset attributes, the returned
        array will be scaled into the expected data range.

        Raises IndexError if doy is out of range for ndays in the file.

        Args:
          doy: integer in range 1-366 of the day-of-year to read.

        Returns:
          nrows x ncols numpy ndarray tile of data for this day-of-year.

        """
        # Extract the tile for the requested dayofyear
        # python will automatically unscale the data using variable
        # attributes for scale_factor and add_offset
        try:
            return self.d[doy-1, :, :]
        except IndexError:
            print("%s : doy=%d should be <= %d" %
                  (__name__, doy, self.ndays),
                  file=sys.stderr)
            raise

    def close(self):
        """Closes the ModisTileCube object.

        Frees the memory used to read this cube file.

        """
        self.f.close()


def read_tile(filename, varname=None,
              rows=ModelEnv.modis_tile_500m_rows,
              cols=ModelEnv.modis_tile_500m_cols,
              verbose=False):
    """Reads a single-tile CHARIS data file.

    Reads tile data files with any of the following filename extensions:

    .bin : assumes a single layer of flat binary 32-bit floating-point data
    .tif : assumes a single layer of data
    .nc  : assumes a single layer of data, reads varname
    .hdf : assumes HDF5 file, reads varname

    Args:
      filename: string name of tile file to read

      varname: the variable to read (only needed for .nc/.hdf files)

      rows: number of rows to read (only needed for .bin files)

      cols: number of cols to read (only needed for .bin files)

      verbose: boolean to turn on verbose output to stderr.

    Returns:
      nrows x ncols numpy ndarray tile of data.

    """
    # Turn off verbose rasterio logging, limit logging to actual errors only
    log = rasterio.logging.getLogger()
    log.setLevel(rasterio.logging.ERROR)

    name, extension = os.path.splitext(filename)
    if extension == '.bin':

        try:
            data = np.fromfile(filename, dtype=np.dtype('f4')).reshape(
                (rows, cols))
        except IOError:
            print("%s : Error reading %s" % (__name__, filename),
                  file=sys.stderr)
            raise

    elif extension == '.tif':

        try:
            with rasterio.open(filename) as src:
                data = np.squeeze(src.read())
        except IOError:
            print("%s : Error reading %s" % (__name__, filename),
                  file=sys.stderr)
            raise

        if verbose:
            print("> %s : read %s" % (__name__, filename),
                  file=sys.stderr)

    elif extension == '.nc':

        try:
            f = Dataset(filename, 'r', "NETCDF4")
        except RuntimeError:
            print("%s : Error reading %s" % (__name__, filename),
                  file=sys.stderr)
            raise

        data = f.variables[varname][:]

        f.close()

        if verbose:
            print("> %s : read %s from %s" %
                  (__name__, varname, filename),
                  file=sys.stderr)

    elif extension == '.hdf':

        try:
            f = Dataset(filename, 'r', 'HDF5')
        except RuntimeError:
            raise RuntimeError("%s : Error opening %s" % (
                __name__, filename))

        data = f.variables[varname][:]

        f.close()

        if verbose:
            print("> %s : read %s from %s" %
                  (__name__, varname, filename),
                  file=sys.stderr)

    else:

        raise ValueError("%s : unrecognized extension=%s" %
                         (__name__, extension))

    return data


def read_evapotranspiration_tile(filename,
                                 verbose=False):
    """Reads a 1km MODIS evapotranspiration data file, and returns
    500m ET array.

    Reads ET_1km, eliminates out-of-range data, reshapes to 500m
    tile dimensions and converts ET from mm to km^3.

    Args:
      filename: string name of ET tile file to read

      verbose: boolean to turn on verbose output to stderr.

    Returns:
      500m tile with numpy ndarray tile of ET data, in km^3.

    """
    # Read the data
    varname = 'ET_1km'
    data = read_tile(filename,
                     varname=varname,
                     verbose=verbose)

    # Fetch the valid range and scaling factor
    try:
        f = Dataset(filename, 'r', 'HDF5')
    except RuntimeError:
        raise RuntimeError("%s : Error opening %s" % (
            __name__, filename))

    # check for values that are either negative or
    # out of top of valid range
    scaled_max = (
        f.variables[varname].valid_range[1] *
        f.variables[varname].scale_factor)
    f.close()
    if verbose:
        print("> %s : %s setting %s values < 0 or > %f to 0." %
              (__name__, filename, varname, scaled_max),
              file=sys.stderr)

    top_mask = data > scaled_max
    top_count = data[top_mask].shape[0]
    if top_count != 0:
        if verbose:
            print("> %s : %s (%s): upper values found at %d pixels" %
                  (__name__, filename, varname, top_count),
                  file=sys.stderr)
        data[top_mask] = 0.

    # Warn about possible negative values
    neg_mask = data < 0.
    neg_count = data[neg_mask].shape[0]
    if neg_count != 0:
        if verbose:
            print("> %s : %s (%s): negative values found at %d pixels" %
                  (__name__, filename, varname, neg_count),
                  file=sys.selftderr)
        data[neg_mask] = 0.

    # Reshape the array to 500m, duplicating each column and row
    if data.shape == (1200, 1200):
        data = np.tile(data, 2).repeat(2).reshape(
            ModelEnv.modis_tile_500m_rows,
            ModelEnv.modis_tile_500m_cols)
        if verbose:
            print("> %s : %s (%s): resized to 2400x2400" %
                  (__name__, filename, varname),
                  file=sys.stderr)
    else:
        raise RuntimeError("%s : %s (%s) unexpected shape=%s" %
                           (__name__, filename, varname, data.shape))

    # Convert units from mm ET to km^3
    mm_per_m = 1000.
    m_per_km = 1000.
    data = (data / mm_per_m) * (
        ModelEnv.modis_tile_500m_y_resolution_m *
        ModelEnv.modis_tile_500m_x_resolution_m) / (
            m_per_km * m_per_km * m_per_km)

    if verbose:
        print("> %s : read %s from %s" %
              (__name__, varname, filename),
              file=sys.stderr)

    return data
