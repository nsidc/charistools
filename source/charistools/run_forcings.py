#! /usr/bin/env python
from __future__ import print_function
import click
import os
import sys

from modelEnv import ModelEnv
from convertors import ForcingsForDrainageID


@click.command()
@click.argument('drainages', nargs=-1)
@click.option('-t', '--threshold', default=205,
              help="grsize or albedo threshold for surface discrimination")
@click.option('-f', '--use_daily_threshold_file', default=False,
              help="use daily thresholds from threshold file")
@click.option('-n', '--nstrikes', type=click.IntRange(1, 3),
              default=1,
              help='MODICE nstrikes')
@click.option('-y', '--year', default=2001,
              help="year to process")
@click.option('-s', '--skip_temperatures', default=False,
              help="skip temperatures, only processes surface types")
@click.option('-e', '--stop_doy', type=int, default=None,
              help="Day of year to stop")
@click.option('-v', '--verbose', count=True,
              help="increase verbosity (may be repeated)")
def run_forcings(year, threshold, use_daily_threshold_file, nstrikes,
                 stop_doy, skip_temperatures, verbose, drainages):
    """Creates sets of temperature/SOL/SOI/EGI forcings for DRAINAGEIDS.

    Uses environment $CHARIS_CONFIG_FILE for modelEnv.
    Uses environment $CHARIS_DATA_DIR for value of model topDir.
    """

    # Initialize the processing environment and location of data
    try:
        configFile = os.environ['CHARIS_CONFIG_FILE']
    except KeyError:
        print("%s : Requires environment CHARIS_CONFIG_FILE for model" %
              (__file__), file=sys.stderr)
        raise

    try:
        topDir = os.environ['CHARIS_DATA_DIR']
    except KeyError:
        print("%s : Requires environment CHARIS_DATA_DIR for model" %
              (__file__), file=sys.stderr)
        raise

    modelEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir)
    if use_daily_threshold_file:
        threshold = 'fromFile'
    for id in drainages:
        if verbose > 0:
            print("> %s : configFile=%s, topDir=%s, "
                  "drainage=%s, year=%d, stop_doy=%s, nstrikes=%d, "
                  "skip_temperatures=%s, threshold=%s, "
                  "use_daily_threshold_file=%s" %
                  (__file__, configFile, topDir,
                   id, year, stop_doy, nstrikes, skip_temperatures,
                   threshold, use_daily_threshold_file),
                  file=sys.stderr)
        try:
            if not ForcingsForDrainageID(
                    drainageID=id,
                    year=year,
                    modelEnv=modelEnv,
                    fsca_type='modscag_gf',
                    ablation_method='grsize_scag',
                    thresholds=[threshold],
                    modice_nstrikes=nstrikes,
                    stop_doy=stop_doy,
                    skip_temperatures=skip_temperatures,
                    verbose=verbose > 1):
                raise RuntimeError
        except RuntimeError:
            print("%s : RuntimeError for %s, %s, %s, %s, skipping..." %
                  (__file__, id, year, threshold, nstrikes), file=sys.stderr)


if __name__ == '__main__':
    run_forcings()
