#!/usr/bin/env python
"""Library for keeping track of ice discriminator threshold
used for CHARIS melt modelling.

The discriminator threshold can be constant, or may vary by doy.

"""
from __future__ import print_function

from configobj import ConfigObj   # noqa
import numpy as np  # noqa
import os   # noqa
import pandas as pd  # noqa
import re   # noqa
import sys   # noqa

daysPerYear = 366


class Threshold():
    """Threshold class to manage ice discriminator threshold value.

    Public attributes:

    - filename: The threshold file used to set daily values.

    """
    filename = None
    comments = []
    data = None

    def __init__(self, filename=None, threshold=205, verbose=False):
        """Initialize a threshold object.

        If threshold is set, uses this value as a time-invariant threshold.

        If filename is set, opens and reads it into state data.

        The filename is expected be ASCII and to look like:
        # Comment
        # Comment
        # COLUMNS: DOY GSthreshold
        1 124
        2 124
        ...
        7 125
        ...
        366 124

        Raises RuntimeError if error occurs reading filename

        Args:
          filename: string of threshold file name to read
            If None, just return modelEnv object with modis tile state
            data (dimensions, resolution).

          constant: float/int, default 205
            constant threshold to use for every doy.

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Initialized daily threshold object.

        """
        if filename is None:
            self.data = pd.DataFrame(
                data=np.full((daysPerYear), threshold, dtype='float'),
                index=np.arange(daysPerYear) + 1,
                columns=['value'])
            if verbose:
                print("> %s : using constant threshold = %f" % (
                    __name__, threshold),
                      file=sys.stderr)
        else:
            try:
                self.read_threshold_file(filename, verbose=verbose)
            except:
                raise

            if verbose:
                print("> %s : read daily thresholds from %s" %
                      (__name__, self.filename),
                      file=sys.stderr)

    def read_threshold_file(self, filename, verbose=False):
        """Reads the contents of filename into state data.

        Raises any file error exceptions from reading filename.

        Args:
          filename: string of file name to read into this
            object's state data.

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          True if successful.

        """
        self.filename = filename

        expected_units = None
        if '_GS_' in filename:
            expected_units = 'microns'

        try:
            with open(filename, 'r') as fh:
                while True:
                    line = fh.readline()
                    if line.strip() == '':
                        continue

                    if line.startswith('#'):
                        if 'Units' in line:
                            units = line.strip().lstrip('# Units: ')
                            if expected_units != units:
                                sys.stderr.write(
                                    "Warning: Units from file {f}: '{u}'.  "
                                    "Expected '{e}'\n".format(
                                        f=self.filename,
                                        u=units,
                                        e=expected_units))

                        if 'COLUMNS' in line:
                            colnames = line.strip().lstrip('# COLUMNS:').lstrip().lower().split()

                        self.comments.append(line.strip())
                        continue

                    else:
                        fh.seek(-len(line), 1)
                        break

                # Done with header lines.  Use pandas to read the rest.
                self.data = pd.read_csv(fh,
                                        delim_whitespace=True,
                                        header=None,
                                        names=colnames)
                self.data.set_index('doy', inplace=True)
                self.data.columns = ['value']

        except Exception as e:
            print(__name__ + ": Error({0})".format(e), file=sys.stderr)
            raise

        if verbose:
            print("> %s : read daily thresholds from %s" %
                  (__name__, self.filename),
                  file=sys.stderr)

    def value(self, doy):
        """Returns the threshold data value for the requested doy.

        Raises KeyError is doy is out of range.

        Args:
          doy: integer day of year to get value for (1-366)

        Returns:
          Threshold data value for requested day of year.
        """
        try:
            return(self.data.at[doy, 'value'])
        except KeyError:
            print("%s : doy=%s out of range for current threshold data" % (
                __name__, doy),
                  file=sys.stderr)
            raise
