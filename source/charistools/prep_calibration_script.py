#! /usr/bin/env python
from __future__ import print_function
import click
import numpy as np
import sys


@click.command()
@click.option('-d', '--drainageid', default='IN_Hunza_at_DainyorBridge',
              help="DrainageID, e.g. 'IN_Hunza_at_DainyorBridge'")
@click.option('-n', '--nstrikes', default=1,
              help="MODICE nstrikes, 1-3")
@click.option('-m', '--ablation_method', default='albedo_mod10a1',
              help="ablation_method, "
              "e.g. 'albedo_mod10a1' (default) or 'grsize_modscag'")
@click.option('-t', '--threshold', default=0.40,
              help="ablation_method threshold, "
              "e.g. 0.0 - 1.0 for albedo, 0 - 1100 for grsize")
@click.option('-f', '--use_daily_threshold_file', default=False,
              help="use forcings from daily thresholds from threshold file")
@click.option('-c', '--calibration_source', default="Aphrodite",
              help="source of calibration rainfall data, "
              "'Aphrodite' (default) or 'MERRA'")
@click.option('--min_snow_ddf', default=(2.0, 4.0, 3),
              help='min snow ddf (mm / degC / day)')
@click.option('--max_snow_ddf', default=(5.0, 7.0, 3),
              help='max snow ddf (mm / degC / day)')
@click.option('--min_ice_ddf', default=(2.0, 4.0, 3),
              help='min ice ddf (mm / degC / day)')
@click.option('--max_ice_ddf', default=(7.0, 9.0, 3),
              help='max ice ddf (mm / degC / day)')
@click.option('--rainfall_col', default='rainfall',
              help='rainfall column name')
def prep_calibration_script(drainageid,
                            nstrikes,
                            ablation_method, threshold,
                            use_daily_threshold_file,
                            calibration_source,
                            min_snow_ddf, max_snow_ddf,
                            min_ice_ddf, max_ice_ddf,
                            rainfall_col):

    years_by_drainageid = {}
    if 'Aphrodite' == calibration_source:
        # The following are years to calibrate with APHRODITE rainfall
        # data, which is only available through 2007
        # We can change these if/when we use some other source for rainfall
        years_by_drainageid['AM_Vakhsh_at_Komsomolabad'] = [2004, 2005, 2007]
        years_by_drainageid['GA_Karnali_at_Benighat'] = [2001, 2003, 2004]
        years_by_drainageid['GA_Narayani_at_Devghat'] = [2001, 2004, 2005]
        years_by_drainageid['GA_SaptaKosi_at_Chatara'] = [2003, 2004, 2005]
        years_by_drainageid['IN_Hunza_at_DainyorBridge'] = [2001, 2002, 2003]
        years_by_drainageid['SY_Naryn_at_NarynTown'] = [2003, 2005, 2007]
    elif 'MERRA' == calibration_source:
        # The following are years to calibrate with MERRA rainfall,
        # determined as half the available years with streamflow data,
        # and covering a representative statistical spread of
        # streamflow variability
        # Ref notes at: https://docs.google.com/spreadsheets/d/
        # 1_bYKlJH5mTo_ljVbSN619nSUBBgbA6YHW6ZstmTrb_A/edit#gid=0
        years_by_drainageid['AM_Vakhsh_at_Komsomolabad'] = [
            2001, 2002, 2004, 2005, 2008, 2009]
        years_by_drainageid['GA_Karnali_at_Benighat'] = [2001, 2003, 2004]
        years_by_drainageid['GA_Narayani_at_Devghat'] = [2001, 2002, 2005]
        years_by_drainageid['GA_SaptaKosi_at_Chatara'] = [2001, 2002, 2005]
        years_by_drainageid['IN_Hunza_at_DainyorBridge'] = [2002, 2008, 2009, 2010]
    else:
        raise ValueError("%s: invalid calibration_source=%s" % (
            __file__, calibration_source))

    min_snows = np.linspace(min_snow_ddf[0], min_snow_ddf[1], min_snow_ddf[2], endpoint=True)
    max_snows = np.linspace(max_snow_ddf[0], max_snow_ddf[1], max_snow_ddf[2], endpoint=True)
    min_ices = np.linspace(min_ice_ddf[0], min_ice_ddf[1], min_ice_ddf[2], endpoint=True)
    max_ices = np.linspace(max_ice_ddf[0], max_ice_ddf[1], max_ice_ddf[2], endpoint=True)

    header_option = "--header=True "
    first_time = True

    if use_daily_threshold_file:
        threshold_str = "--use_daily_threshold_file=True"
    else:
        threshold_str = "--threshold=%s" % threshold

    for year in years_by_drainageid[drainageid]:
        for min_snow in min_snows:
            for max_snow in max_snows:
                for min_ice in min_ices:
                    for max_ice in max_ices:
                        if (min_snow <= max_snow and min_ice <= max_ice):
                            print("run_calibration_stats.py %s--drainageid=%s "
                                  "--year=%d "
                                  "--nstrikes=%d "
                                  "--ablation_method=%s "
                                  "%s "
                                  "--min_snow_ddf=%4.2f "
                                  "--max_snow_ddf=%4.2f "
                                  "--min_ice_ddf=%4.2f "
                                  "--max_ice_ddf=%4.2f "
                                  "--rainfall_col=%s "
                                  "$CHARIS_CONFIG_FILE" % (
                                      header_option,
                                      drainageid, year,
                                      nstrikes,
                                      ablation_method,
                                      threshold_str,
                                      min_snow, max_snow,
                                      min_ice, max_ice,
                                      rainfall_col))
                            if first_time:
                                header_option = ""
                                first_time = False
                        else:
                            print("Overlap DDF error : --drainageid=%s "
                                  "--year=%d --min_snow_ddf=%4.2f "
                                  "--max_snow_ddf=%4.2f "
                                  "--min_ice_ddf=%4.2f "
                                  "--max_ice_ddf=%4.2f " % (
                                      drainageid, year,
                                      min_snow, max_snow,
                                      min_ice, max_ice),
                                  file=sys.stderr)


if __name__ == '__main__':
    prep_calibration_script()
