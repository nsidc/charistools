#! /usr/bin/env python
from __future__ import print_function
import click
import re
import sys


@click.command()
@click.argument('input', nargs=-1, type=click.Path(exists=True))
@click.option('-o', '--outfile', default=None,
              help="merged output file, with 1 header line at top")
@click.option('-v', '--verbose', count=True,
              help="increase verbosity (may be repeated)")
def merge_calibration_files(input, outfile, verbose):
    """
    Merges multiple calibration output files, eliminating any
    duplicate headers.  Header in first file is moved to the
    beginning of the output.
    """

    if outfile:
        outf = open(outfile, 'w')
    else:
        outf = sys.stdout

    re_header = re.compile(r'DRAINAGE')
    for i, file in enumerate(input):
        if verbose > 0:
            print("%s: %s" % (__file__, file), end=" ", file=sys.stderr)

        with open(file) as f:
            lines = f.readlines()

        # Find the header line and its position
        header_pos = -1
        for j, line in enumerate(lines):
            if re_header.match(line):
                header_pos = j
                header = line
                break

        # Delete header if one is found
        if 0 <= header_pos:
            if verbose > 0:
                print("Deleting header at position=%d" % (header_pos),
                      file=sys.stderr)
            del lines[header_pos]
        else:
            if verbose > 0:
                print("No header found", file=sys.stderr)

        # Insert the header at the beginning of the first file only
        if i == 0:
            lines.insert(0, header)

        # Write to stdout
        for line in lines:
            outf.write(line)

    outf.flush()


if __name__ == '__main__':
    merge_calibration_files()
