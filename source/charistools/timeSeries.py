#!/usr/bin/env python
"""Library for handling time series data for CHARIS melt modelling files.

The CHARIS melt modelling uses various time series data sets for
calibration and validation, including series of runoff and rainfall.
This module provides a simple reader for our time series type data.

"""

from __future__ import print_function
import csv  # noqa
import datetime as dt  # noqa
import pandas as pd  # noqa
import re  # noqa
import sys  # noqa


DEBUG = False


class TimeSeries(object):
    """TimeSeries class manages CHARIS timeseries data (discharge, runoff,
    precipitation, etc.).

    Public attributes:

    - comments: List of strings with metadata about the data, can
      include provenance, units, history of any other relevant
      description of the data contents

    - data: Pandas DataFrame with time series data, possible multiple
      columns, in row representing dates.

    """
    comments = []
    data = pd.DataFrame()

    def __init__(self, filename):
        """Initialize a timeSeries object.

        Returns an object containing a pandas timeseries object in "data".

        Comments are saved with the leading hash ('#') characters retained.

        Expected input file format is ASCII text, with header
        lines beginning with '#', and data in two whitespace-separated columns:
        year and annual runoff (km^3) or mean annual discharge (m^3/s).

        Args:
          filename : filename to open and read from

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Initialized CHARIS TimeSeries object.

        """
        self.expected_units = None
        self.units = None
        self.comments = []
        self.colnames = None
        self._datetime_cols = []

        _parse = None

        self.filename = filename
        if 'annual_discharge' in filename:
            self.expected_units = 'm**3 s**-1'
        elif 'annual_runoff' in filename:
            self.expected_units = 'km**3'

        if self.expected_units != self.units:
            sys.stderr.write("Warning: Units from file {f}: '{u}'.  Expected '{e}'\n".format(
                f=self.filename, u=self.units, e=self.expected_units))

        with open(filename, 'r') as fh:
            while True:
                line = fh.readline()
                if line.strip() == '':
                    continue

                if line.startswith('#'):
                    if 'Units' in line:
                        self.units = line.strip().lstrip('# Units: ')
                    if 'COLUMNS' in line:
                        self.colnames = line.strip().lstrip('# COLUMNS:').lstrip().lower().split()

                        parse_date_format = []
                        fmt_by_name = {'year': '%Y', 'month': '%m', 'day': '%d'}
                        for col in ['year', 'month', 'day']:
                            if col in self.colnames:
                                self._datetime_cols.append(col)
                                parse_date_format.append(fmt_by_name[col])

                        if DEBUG:
                            print('DEBUG %s : parse_date_format: %s' %
                                  (__name__, parse_date_format), file=sys.stderr)

                        def _parse(x): return dt.datetime.strptime(x, ' '.join(parse_date_format))

                    self.comments.append(line.strip())
                    continue

                else:
                    fh.seek(-len(line), 1)
                    break

            # Done with header lines.  Use pandas to read the rest.
            if DEBUG:
                print("DEBUG %s : colnames: %s" % (__name__, self.colnames),
                      file=sys.stderr)
                print("DEBUG %s : _datetime_cols: %s " % (__name__, self._datetime_cols),
                      file=sys.stderr)

            self.data = pd.read_csv(fh, delim_whitespace=True, header=None, names=self.colnames,
                                    parse_dates={'datetime': self._datetime_cols},
                                    date_parser=_parse, index_col='datetime')

    def write(self, filename, decimal_places=6, verbose=False):
        """Writes the timeSeries contents to ASCII file.

        If object comments do not begin with '#', then they will
        be prepended by this character in the output stream.

        Args:
          filename: string name of filename to write

          decimal_places: number of decimal places for formatted float
            data values in output file

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          True if successful

        """
        try:
            fh = open(filename, 'w')
        except:
            print("%s : Error opening %s" % (__name__, filename),
                  file=sys.stderr)
            raise

        # Write any comments first, one comment per line
        # Don't write a comment that contains the string "COLUMNS",
        # this will be created later using the actual column names
        regex_leading_comment = re.compile(r'#')
        regex_trailing_newline = re.compile(r'\n$')
        regex_columns_line = re.compile(r'COLUMNS')
        for line in self.comments:
            if regex_columns_line.search(line):
                continue
            prefix = ''
            suffix = ''
            if regex_leading_comment.match(line) is None:
                prefix = '# '
            if regex_trailing_newline.match(line) is None:
                suffix = '\n'
            fh.write(prefix + line + suffix)

        # Make a temporary copy of the DataFrame,
        # and add back columns for year, month, day and doy
        tmp = self.data.copy(deep=True)
        tmp.drop('doy', axis=1, inplace=True)
        tmp['year'] = tmp.index.year
        tmp['month'] = tmp.index.month
        tmp['day'] = tmp.index.day
        tmp['doy'] = tmp.index.dayofyear
        tmp = tmp.reindex_axis(['year', 'month', 'day', 'doy'] + list(tmp.columns[:-4]),
                               axis=1)

        # Convert column names to lowercase
        tmp.columns = map(str.lower, tmp.columns)

        # Zero-pad month, day and doy columns
        tmp['month'] = tmp.month.map("{:02}".format)
        tmp['day'] = tmp.day.map("{:02}".format)
        tmp['doy'] = tmp.doy.map("{:03}".format)

        # Compile and write the COLUMNS line
        # Close the file so DataFrame.to_csv method can append to it
        line = " ".join(tmp.columns)
        fh.write("# COLUMNS: " + line + '\n')
        fh.close()

        format = "%." + str(decimal_places) + "f"
        tmp.to_csv(filename, mode='a', header=False, index=False, sep=" ",
                   float_format=format, quoting=csv.QUOTE_NONE)

        return True

    def summary(self):
        """Return the DataFrame description of the TimeSeries data."""
        return repr(self.data.describe())

    def as_text(self):
        """Return the TimeSeries as text."""
        return repr(self.data)
