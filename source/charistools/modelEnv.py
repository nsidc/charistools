#!/usr/bin/env python
"""Library for keeping track of filenames used for CHARIS melt modelling.

The CHARIS melt modelling uses remote sensing data from many
different sources.  This module provides a mechanism to specify
directory and filename patterns in a configuration "ini" file
that can be read and set at runtime with various wildcard values.

The main work is done by the ModelEnv class, which also keeps track
of some values specific to MODIS tile data.

"""
from __future__ import print_function

from configobj import ConfigObj   # noqa
import glob   # noqa
import os   # noqa
import re   # noqa
import sys   # noqa

from hypsometry import Hypsometry


class ModelEnv():
    """ModelEnv class to keep track of filenames used in CHARIS melt modelling.

    Public attributes:

    - tileConfigFile: The ini file describing files used for
    modelling.  This file is assumed to be in Windows "ini"
    format, which is well-defined, human-readable, and allows for
    hierarchical relationships between objects.

    - modis_tile_500m_rows: number of rows in a 500m MODIS tile
    - modis_tile_500m_cols: number of columns in a 500m MODIS tile
    - modis_tile_500m_x_resolution_m = MODIS 500m tile spatial resolution along
      x axis, in meters
    - modis_tile_500m_y_resolution_m = MODIS 500m tile spatial resolution along
      y axis, in meters
    - modis_tile_500m_pixel_area_km2 = MODIS 500m tile pixel area, in square km

    """
    tileConfigFile = 'modis_tiles_config.ini'
    modis_tile_500m_cols = 2400
    modis_tile_500m_rows = 2400
    modis_tile_500m_x_resolution_m = 463.312717
    modis_tile_500m_y_resolution_m = 463.312717
    modis_tile_500m_pixel_area_km2 = (
        modis_tile_500m_x_resolution_m *
        modis_tile_500m_y_resolution_m) / (1000. * 1000.)

    def __init__(self, tileConfigFile=None, topDir=None, verbose=False):
        """Initialize a melt model environment object.

        Opens and reads the tileConfigFile into state data, which
        can then be accessed using the various *_filename
        functions to retrieve model filenames with wildcard substitution.

        The following values are expected in the tileConfigFile, with
        a "dir" and "pattern value for each::
        [input]
          [[forcing]]
            [[[mod10a1_gf]]]
            dir = ...
            pattern = ...
            [[[albedo_mod10a1]]] ...
            [[[threshold]]] ...
            [[[temperature]]]
          [[fixed]]
            [[[basin_mask]]]
            [[[dem]]]
            [[[modice_min05yr]]]
        [hypsometry]
          [[modice_min05yr_by_elevation]]
          [[snow_on_land_by_elevation]]
          [[snow_on_ice_by_elevation]]
          [[exposed_glacier_ice_by_elevation]]
          [[temperature_by_elevation]]
          [[area_by_elevation]]

        To customize the tileConfigFile with additional fields
        that will be available from an initialized modelEnv
        object as, for example:
        [level1]
          [[level2]]
          value = 123

        which will be available as the value of

        myEnv.tileConfig['level1']['level2']['value']

        Args:
          tileConfigFile: string of ini file name to read with melt model
            configuration.
            If None, just return modelEnv object with modis tile state
            data (dimensions, resolution).

          topDir: string withe top-level directory for filename patterns
            that contain the wildcard %model_top_dir%.

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Initialized melt model environment object.

        """
        if tileConfigFile is None:
            self.tileConfigFile = None
            if verbose:
                print("> %s : using default ModelEnv" % __name__,
                      file=sys.stderr)
        else:

            try:
                self.set_model_config_file(tileConfigFile, verbose=verbose)
            except:
                raise

            if topDir is not None:
                self.set_model_top_dir(topDir, verbose=verbose)

            if verbose:
                print("> %s : read MODIS tile configuration from %s" %
                      (__name__, self.tileConfigFile),
                      file=sys.stderr)

    def set_model_config_file(self, tileConfigFile, verbose=False):
        """Reads the contents of tileConfigFile into state data.

        Raises any file error exceptions from reading tileConfigFile.

        Args:
          tileConfigFile: string of ini file name to read into this
            object's state data.

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          True if successful.

        """
        self.tileConfigFile = tileConfigFile

        try:
            self.tileConfig = ConfigObj(self.tileConfigFile, file_error=True)
        except Exception as e:
            print(__name__ + ": Error({0})".format(e), file=sys.stderr)
            raise

        if verbose:
            print("> %s : read MODIS tile configuration from %s" %
                  (__name__, self.tileConfigFile),
                  file=sys.stderr)

        return(True)

    def set_model_top_dir(self, topDir, verbose=False):
        """Change value of %MODEL_TOP_DIR% in subsequent filename calls.

        Change the top-level directory for any paths in the config that use
        the %MODEL_TOP_DIR% wildcard.

        Raises OSError if the directory does not exist.

        Args:
          topDir: pathname string, e.g. '/path/to/data'

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          True if successful.

        """
        try:
            os.stat(topDir)
        except OSError, e:
            print(__name__ + ": Error({0})".format(e), file=sys.stderr)
            print(e, file=sys.stderr)
            raise

        self.tileConfig['model_top_dir'] = topDir
        if verbose:
            print("> %s : model_top_dir changed to %s" %
                  (__name__, self.tileConfig['model_top_dir']),
                  file=sys.stderr)

        return(True)

    def fixed_filename(self, type=None, drainageID=None, tileID=None,
                       modice_nstrikes=1, verbose=False):
        """Fetch the requested type of fixed input filename.

        Fixed input data do not vary with time, but may have other wildcards
        for drainageID or tileID.

        File types and required wildcards:
          basin_mask:     drainageID and tileID
          dem:            tileID
          modice_min05yr: tileID and modice_nstrikes

        Raises LookupError if called before tileConfigFile has been set.
        Raises ValueError if required wildcards are not included for the
          requested file type.

        Args:
          type: required string with filename type to retrieve;
            must correspond to a string in the tileConfigFile,
            directly under [input][fixed] level,
            e.g. 'basin_mask', 'dem', 'modice_min05yr'

          drainageID: drainage name string, used for wildcard %DRAINAGEID%
            e.g. "IN_Hunza_at_Danyour" or "IN_Hunza_GDBD"

          tileID: MODIS tileID string, used for wildcard %TILEID%
            e.g. 'h23v05'

          modice_nstrikes : modice files differ by number of strikes,
            1, 2 or 3.

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Requested input fixed_filename, with any wildcards substituted.

        """
        if self.tileConfigFile is None:
            raise LookupError(__name__ +
                              ": no tileConfigFile set")
        if type is None:
            raise ValueError(__name__ +
                             ": type is required")
        if tileID is None:
            raise ValueError(__name__ +
                             ": tileID is required")
        if type == "basin_mask" and drainageID is None:
            raise ValueError(__name__ +
                             ": drainageID is required for basin_mask data")

        file = os.path.join(self.tileConfig['input']['fixed'][type]['dir'],
                            self.tileConfig['input']['fixed'][type]['pattern'])
        p = re.compile('%MODEL_TOP_DIR%')
        file = p.sub(self.tileConfig['model_top_dir'], file)
        p = re.compile('%NSTRIKES%')
        file = p.sub(str(modice_nstrikes), file)
        p = re.compile('%TILEID%')
        file = p.sub(tileID, file)

        if drainageID is not None:
            p = re.compile('%DRAINAGEID%')
            file = p.sub(drainageID, file)

        if verbose:
            print("> %s: fixed_filename is %s; file_exists=%s" %
                  (__name__, file, os.path.isfile(file)),
                  file=sys.stderr)

        return(file)

    def forcing_filename(self, type=None, tileID=None, year=None,
                         month=None,
                         verbose=False):
        """Fetch the requested type of forcing input filename.

        Forcing input data vary with time and possibly tileID.

        File types and required wildcards:
          mod10a1_gf:     tileID and year
          modscag_gf:     tileID and year
          albedo_mod10a1: tileID and year
          grsize_scag: tileID and year
          temperature:    tileID and year
          threshold:      no wildcards required
          mod16a2:        tileID, year and month

        Raises ValueError if required wildcards are not included for the
          requested file type.

        Args:
          type: required string with filename type to retrieve;
            must correspond to a string in the tileConfigFile,
            directly under [input][forcing] level,
            e.g. 'mod10a1_gf', 'albedo_mod10a1', 'temperature'

          tileID: MODIS tileID string, used for wildcard %TILEID%
            e.g. 'h23v05'

          year: integer, 4-digit year, used for wildcard %YYYY%
            e.g. 2001

          month: integer, month of year, 1=January, etc, used for
            wildcard %MM%, e.g. 1

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Requested input forcing_filename, with any wildcards substituted.
          mod16a2 files have retained the changing process id; the actual
          filename will be determined and returned

        """
        if self.tileConfigFile is None:
            raise LookupError(__name__ + ": no tileConfigFile set")
        if type != 'threshold' and tileID is None:
            raise ValueError(__name__ + ": tileID is required")
        if type != 'threshold' and year is None:
            raise ValueError(__name__ + ": year is required")
        if type == 'mod16a2' and month is None:
            raise ValueError(__name__ + ": month is required")

        file = os.path.join(
            self.tileConfig['input']['forcing'][type]['dir'],
            self.tileConfig['input']['forcing'][type]['pattern'])
        p = re.compile('%MODEL_TOP_DIR%')
        file = p.sub(self.tileConfig['model_top_dir'], file)
        if tileID:
            p = re.compile('%TILEID%')
            file = p.sub(tileID, file)
        if year:
            p = re.compile('%YYYY%')
            file = p.sub(str(year).zfill(4), file)
        if month:
            p = re.compile('%MM%')
            file = p.sub(str(month).zfill(2), file)

        # For MOD16A2, get the actual filename
        if type == 'mod16a2':
            list = glob.glob(file)
            if list:
                file = list[0]

        if verbose:
            print("> %s: forcing_filename is %s; file_exists=%s" %
                  (__name__, file, os.path.isfile(file)),
                  file=sys.stderr)

        return(file)

    def hypsometry_filename(self, type=None,
                            drainageID=None,
                            year=None,
                            contour_m=100,
                            modice_nstrikes=1,
                            ablation_method=None,
                            threshold=None,
                            et_source=None,
                            verbose=False):
        """Fetch the requested type of hypsometry filename.

        Hypsometry data vary with drainageID, time and other parameters used
        to derive the hypsometry.

        File types and required wildcards:
          modice_min05yr_by_elevation: drainageID, contour_m and modice_nstrikes
          snow_on_land_by_elevation:   drainageID, contour_m, year
          snow_on_ice_by_elevation:    drainageID, contour_m, year
            ablation_method and threshold
          exposed_glacier_ice_by_elevation: drainageID, contour_m, year,
            ablation_method and threshold
          temperature_by_elevation:    drainageID, contour_m, year
          area_by_elevation:           drainageID, contour_m
          custom_snow_ddf:             drainageID, contour_m
          custom_ice_ddf:              drainageID, contour_m
          et_by_elevation:             drainageID, contour_m, et_source

        Raises LookupError if called before tileConfigFile has been set.
        Raises ValueError if required wildcards are not included for the
          requested file type.

        Args:
          type: required string with filename type to retrieve;
            must correspond to a string in the tileConfigFile,
            directly under [hypsometry] level, e.g. 'area_by_elevation'

          drainageID: drainage name string, used for wildcard %DRAINAGEID%
            e.g. "IN_Hunza_at_Danyour" or "IN_Hunza_GDBD"

          tileID: MODIS tileID string, used for wildcard %TILEID%
            e.g. 'h23v05'

          year: integer, 4-digit year, used for wildcard %YYYY%
            e.g. 2001

          contour_m: integer, contour size, in meters, used for wildcard
            %CONTOUR%

          modice_nstrikes: modice files differ by number of strikes,
            1, 2 or 3.

          ablation_method: string name of ablation_method used to derive
            snow_on_ice or exposed_glacier_ice by elevation, one of
            'grsize_scag' or 'albedo_mod10a1'

          threshold: value of ablation_method threshold used to derive
            snow_on_ice or exposed_glacier_ice by elevation.  Grain size
            threshold ranges from 1-1100 microns, albedo threshold ranges
            from 0.01 to 1.00.  Can also be set to 'fromFile' for variable
            thresholds.

          et_source: string name of ET source, one of 'merra' or 'mod16'

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Requested hypsometry_filename, with any wildcards substituted.

        """
        if self.tileConfigFile is None:
            raise LookupError(__name__ + ": no tileConfigFile set")
        if drainageID is None:
            raise ValueError(__name__ + ": drainageID is required")
        if contour_m is None:
            raise ValueError(__name__ + ": contour_m is required")
        if type in ['snow_on_land_by_elevation',
                    'snow_on_ice_by_elevation',
                    'exposed_glacier_ice_by_elevation',
                    'temperature_by_elevation'] and year is None:
            raise ValueError(__name__ +
                             ": year required " +
                             "for " + type + " files")
        if type in ['snow_on_ice_by_elevation',
                    'exposed_glacier_ice_by_elevation'] and (
                        year is None or threshold is None):
            raise ValueError(__name__ +
                             ": year and threshold required " +
                             "for snow_on_ice or exposed_glacier_ice files")

        if type in [
                'snow_on_ice_by_elevation',
                'exposed_glacier_ice_by_elevation'] and (
                    ablation_method not in [
                        'grsize_scag',
                        'albedo_mod10a1']):
            raise ValueError("%s: invalid ablation_method=%s "
                             "for snow_on_ice or exposed_glacier_ice files" %
                             (__name__, ablation_method))

        if type in ['et_by_elevation'] and (et_source not in [
                'merra',
                'mod16']):
            raise ValueError("%s: invalid et_source=%s "
                             "for et files" %
                             (__name__, et_source))

        file = os.path.join(self.tileConfig['hypsometry'][type]['dir'],
                            self.tileConfig
                            ['hypsometry'][type]['pattern'])

        try:
            surface = re.search('(.+)_by_elevation', type).group(1)
        except AttributeError:
            surface = ''
        p = re.compile('%SURFACE%')
        file = p.sub(surface, file)

        if self.tileConfig['model_top_dir']:
            p = re.compile('%MODEL_TOP_DIR%')
            file = p.sub(self.tileConfig['model_top_dir'], file)
        if drainageID:
            p = re.compile('%DRAINAGEID%')
            file = p.sub(drainageID, file)
        if ablation_method:
            p = re.compile('%ABLATION_METHOD%')
            file = p.sub(ablation_method.upper(), file)
        if et_source:
            p = re.compile('%ET_SOURCE%')
            file = p.sub(et_source, file)
        if modice_nstrikes:
            p = re.compile('%NSTRIKES%')
            file = p.sub(str(modice_nstrikes), file)
        if year:
            p = re.compile('%YYYY%')
            file = p.sub(str(year).zfill(4), file)
        if contour_m:
            p = re.compile('%CONTOUR%')
            file = p.sub("%04dm" % contour_m, file)
        if threshold:
            if threshold != 'fromFile':
                if ablation_method.lower() == 'albedo_mod10a1':
                    # input threshold will be decimal 0.0 - 1.0,
                    # so just save the 2 digits to the right of the
                    # decimal point
                    threshold = threshold * 100.
                threshold_str = "%04d" % threshold
            else:
                threshold_str = threshold
            p = re.compile('%THRESHOLD%')
            file = p.sub("%s" % threshold_str, file)

        if verbose:
            print("> %s: hypsometry_filename is %s; file_exists=%s" %
                  (__name__, file, os.path.isfile(file)),
                  file=sys.stderr)

        return(file)

    def calibration_filename(self, type=None,
                             drainageID=None,
                             verbose=False):
        """Fetch the requested type of calibration filename.

        Calibration data vary with type and drainageID only.

        File types:
          rainfall : rainfall data to add to modeled melt
          runoff : runoff data to compare to melt+rainfall

        Raises LookupError if called before tileConfigFile has been set.
        Raises ValueError if required wildcards are not included for the
          requested file type.

        Args:
          type: required string with filename type to retrieve;
            must correspond to a string in the tileConfigFile,
            directly under [calibration] level, e.g. 'rainfall'

          drainageID: drainage name string, used for wildcard %DRAINAGEID%
            e.g. "IN_Hunza_at_Danyour" or "IN_Hunza_GDBD"

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Requested calibration_filename, with any wildcards substituted.

        """
        if self.tileConfigFile is None:
            raise LookupError(__name__ + ": no tileConfigFile set")
        if drainageID is None:
            raise ValueError(__name__ + ": drainageID is required")
        if type not in ['rainfall', 'runoff']:
            raise ValueError(__name__ +
                             ": type must be 'rainfall' or 'runoff'")

        file = os.path.join(self.tileConfig['calibration'][type]['dir'],
                            self.tileConfig
                            ['calibration'][type]['pattern'])

        if self.tileConfig['model_top_dir']:
            p = re.compile('%MODEL_TOP_DIR%')
            file = p.sub(self.tileConfig['model_top_dir'], file)
        if drainageID:
            p = re.compile('%DRAINAGEID%')
            file = p.sub(drainageID, file)

        if verbose:
            print("> %s: calibration_filename is %s; file_exists=%s" %
                  (__name__, file, os.path.isfile(file)),
                  file=sys.stderr)

        return(file)

    def tileIDs_for_drainage(self, drainageID=None, verbose=False):
        """Fetches the tileIDs that overlap the input drainageID.

        Uses the existing basin_mask files to determine the tileIDs
        that correspond to a given drainageID.

        Args:
          drainageID: drainage name string to search for in list of
            existing basin_mask files, e.g. "IN_Hunza_at_Danyour" or
            "IN_Hunza_GDBD"

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          List of tileIDs of the form 'hXXvYY' that overlap the input
          drainageID.
          Returns None if no masks are found for the given drainageID.

        """
        maskPattern = self.fixed_filename(type='basin_mask',
                                          drainageID=drainageID,
                                          tileID='h??v??',
                                          verbose=verbose)
        maskList = glob.glob(maskPattern)
        if not maskList:
            print("%s: No drainageID mask(s) found for pattern=%s" % (
                __name__, maskPattern), file=sys.stderr)
            raise RuntimeError

        tileIDs = []
        regex_tileID = re.compile(r'(h[0-9]{2}v[0-9]{2})')
        for line in maskList:
            part = regex_tileID.search(line)
            if part is None:
                print("%s: Error parsing %s for tileID" % (__name__, line),
                      file=sys.stderr)
                raise IOError
            tileIDs.append(part.group(1))

        tileIDs.sort()

        return(tileIDs)

    def model_inputs(self, drainageID,
                     year,
                     ablation_method, threshold,
                     modice_nstrikes=1,
                     contour_m=100,
                     get_custom_snow_ddf=False,
                     get_custom_ice_ddf=False,
                     verbose=False):
        """Fetches the 4 inputs required to run the melt model.

        Args:
          drainageID: drainage name string to search for in list of
            existing basin_mask files, e.g. "IN_Hunza_at_Danyour" or
            "IN_Hunza_GDBD"

          year: integer, 4-digit year, used for wildcard %YYYY%
            e.g. 2001

          ablation_method: string name of ablation_method used to derive
            snow_on_ice or exposed_glacier_ice by elevation, one of
            'grsize_scag' or 'albedo_mod10a1'

          threshold: value of ablation_method threshold used to derive
            snow_on_ice or exposed_glacier_ice by elevation.  Grain size
            threshold ranges from 1-1100 microns, albedo threshold ranges
            from 0.01 to 1.00.  Can also be string 'fromFile' to read
            forcings derived from daily-varying thresholds.

          modice_nstrikes : modice files differ by number of strikes,
            1, 2 or 3, default=1

          contour_m: integer, contour size, in meters, default=100.

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          List of temperature, SOL, SOI and EGI filenames and Hypsometries
          needed to run the model for this drainageID.

        """
        out = {}
        filetypes = ['temperature_by_elevation',
                     'snow_on_land_by_elevation',
                     'snow_on_ice_by_elevation',
                     'exposed_glacier_ice_by_elevation']

        for filetype in filetypes:
            try:
                out[filetype + '_filename'] = self.hypsometry_filename(
                    type=filetype,
                    drainageID=drainageID,
                    year=year,
                    modice_nstrikes=modice_nstrikes,
                    ablation_method=ablation_method,
                    threshold=threshold,
                    contour_m=contour_m,
                    verbose=verbose)
                out[filetype + '_hyps'] = Hypsometry(
                    filename=out[filetype + '_filename'])
            except Exception as e:
                print(__name__ + ": Error({0})".format(e), file=sys.stderr)
                print("%s: Error getting hyps for %s" % (__name__, filetype),
                      file=sys.stderr)
                raise

        if get_custom_snow_ddf:
            try:
                out['custom_snow_ddf_filename'] = self.hypsometry_filename(
                    type='custom_snow_ddf',
                    drainageID=drainageID,
                    contour_m=contour_m,
                    verbose=verbose)
                out['custom_snow_ddf_hyps'] = Hypsometry(
                    filename=out['custom_snow_ddf_filename'])
            except Exception as e:
                print(__name__ + ": Error({0})".format(e), file=sys.stderr)
                print("%s: Error getting hyps for %s" % (__name__, filetype),
                      file=sys.stderr)
                raise

        if get_custom_ice_ddf:
            try:
                out['custom_ice_ddf_filename'] = self.hypsometry_filename(
                    type='custom_ice_ddf',
                    drainageID=drainageID,
                    contour_m=contour_m,
                    verbose=verbose)
                out['custom_ice_ddf_hyps'] = Hypsometry(
                    filename=out['custom_ice_ddf_filename'])
            except Exception as e:
                print(__name__ + ": Error({0})".format(e), file=sys.stderr)
                print("%s: Error getting hyps for %s" % (__name__, filetype),
                      file=sys.stderr)
                raise

        return(out)
