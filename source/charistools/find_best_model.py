#! /usr/bin/env python
from __future__ import print_function
import click
import pandas as pd
import sys

from meltModels import CalibrationCost


def get_calibration_stats(file, verbose=0):
    """
    Reads a model calibration output file into a dataframe.
    Combines stats from multiple calibration years into averages
    by model.

    Normalizes RMSE and voldiff to range of [0., 1.]
    so they can be combined and minimized. The returned stats
    data frame is sorted, so first element is best model.

    Ignores duplicate calibrations.

    Returns the original data frame and the new data frame
    with the calculated normalized stats by model.

    Args:
      file : string, name of file with merged
        calibration stats, assumes header is at the top of the file

      verbose: turns on verbose output to stderr,
        higher values are more verbose.

    Returns:
      Dict with two pandas DataFrames:
        data : calibration data read from calibration file
        stats : normalized data by model
    """
    out = {}

    this_func_name = sys._getframe().f_code.co_name

    # Read the calibration stats and calculate cost
    df = pd.read_table(file, sep='\s+')

    # If this is an old calibration file, make it compatible with
    # new format: lower-case column numbers, and model string column
    if 'model' not in df.columns:
        if verbose > 0:
            print("\n%s : Converting data in old-style calibration file=%s" % (
                this_func_name, file), file=sys.stderr)

        df.rename(columns={'Monthly_rmse_km3': 'monthly_rmse_km3',
                           'Annual_voldiff_pcent': 'annual_voldiff_pcent'},
                  inplace=True)
        df.loc[:, "model"] = (
            df["min_snow_ddf"].map(str) + "_" +
            df["max_snow_ddf"].map(str) + "_" +
            df["min_ice_ddf"].map(str) + "_" +
            df["max_ice_ddf"].map(str))

    df, stats_df = CalibrationCost(df, verbose=verbose > 0)

    out["data"] = df
    out["stats"] = stats_df

    return out


@click.command()
@click.argument('input', nargs=1, type=click.Path(exists=True))
@click.option('-v', '--verbose', count=True,
              help="increase verbosity (may be repeated)")
def find_best_model(input, verbose):
    """
    Finds the model that minimizes monthly RMSE and annual volume
    differences.  Duplicate model entries in input calibration files
    are ignored.
    """
    print("%s : Finding best model from calibration in: %s" % (
        __file__, input), file=sys.stderr)

    out = get_calibration_stats(input, verbose)

    print("\nBest model is %s" % out["stats"].index[0],
          file=sys.stdout)


if __name__ == '__main__':
    find_best_model()
