#!/usr/bin/env python
""" Routines to compute projected sinusoidal coordinates from h and
v coordinates of MODIS tiles
"""

from __future__ import print_function


def ul_x_from_h(h):
    """ Sinusoidal x coordinate from MODIS tile h-coordinate (upper-left corner of tile) """
    x_delta = 1111950.5196667
    return float(h-18) * x_delta


def ul_y_from_v(v):
    """ Sinusoidal y coordinate from MODIS tile v-coordinate (upper-left corner of tile) """
    y_delta = 1111950.5196667
    return float(9-v) * y_delta


def corners_from_h_v(h, v):
    assert(0 <= h <= 35)
    assert(0 <= v <= 17)
    x0 = ul_x_from_h(h)
    x1 = ul_x_from_h(h+1)
    y0 = ul_y_from_v(v)
    y1 = ul_y_from_v(v+1)
    corners = ((x0, y0),
               (x0, y1),
               (x1, y1),
               (x1, y0))
    return corners


def corners_from_label(label):
    """ Compute corners from label such as h24v05 """
    # Validate label here
    h, v = h_v_from_label(label)
    return corners_from_h_v(h, v)


def get_all_tile_labels():
    """ Return a list of tile labels, like h24v05 """
    h_coords = range(36)
    v_coords = range(18)
    labels = []
    for v in v_coords:
        for h in h_coords:
            labels.append(label_from_h_v(h, v))
    return labels


def get_all_charis_tile_labels():
    """ Return list of tile labels for CHARIS tiles """
    charis_labels = [
            'h23v06',
            'h24v05',
            'h25v06',
            'h23v05',
            'h26v06',
            'h23v03',
            'h23v04',
            'h25v05',
            'h26v05',
            'h24v04',
            'h24v06',
            'h22v03',
            'h22v04',
            'h22v05',
            'h21v03',
            'h21v04',
        ]
    return charis_labels


def print_corners_for_all_tiles():
    for label in get_all_tile_labels():
        print(label, corners_from_label(label))


def label_from_h_v(h, v):
    """ Return a label, like h24v05, from h and v """
    label = "h{:02d}v{:02d}".format(h, v)
    return label


def h_v_from_label(label):
    """ Return h, v, from a label like h24v05 """
    h = int(label[1:3])
    v = int(label[4:6])
    return (h, v)


if __name__ == '__main__':
    print_corners_for_all_tiles()
