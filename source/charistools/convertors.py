#!/usr/bin/env python
"""Library for converting CHARIS data between different formats.

The CHARIS modelling requires data in different formats at
different stages in the data processing.  This module provides
some of the format conversion routines that are required.

"""
from __future__ import print_function

import datetime as dt   # noqa
from dateutil.rrule import rrule, MONTHLY  # noqa
import math  # noqa
import numpy as np   # noqa
import pandas as pd   # noqa
import sys   # noqa

from modelEnv import ModelEnv   # noqa
from hypsometry import Hypsometry   # noqa
from readers import read_tile, ModisTileCube   # noqa
from readers import read_evapotranspiration_tile  # noqa
from threshold import Threshold  # noqa

# Offset for conversion of Kelvin temperatures to Celsius
KelvinOffset = 273.15


class NanElevationError(Exception):
    pass


def Csv2Hypsometry(src_filename, hyps_filename, comments=[], verbose=False):
    """Convert a one-line hypsometry output by ArcGIS to a CHARIS Hypsometry
    file.

    ArcGIS tools can output area-by-elevation data as
    comma-separated-value ASCII file with 4 columns, which are
    read and converted to the CHARIS Hypsometry file convention
    as follows:

    1st column: ignored
    2nd column: elevation in meters (at bottom of elevation band)
    3rd column: ignored
    4th column: area in square km for this elevation band

    Assumes first line in the csv file is column headers, and ignores it.

    N.B. We could make this method much more generic, by just giving it the
    column numbers to use for elevations and for data values.

    Args:
      src_filename : string, name of 4-column comma-separated value file
        to read

      hyps_filename : string, name of file to write converted Hypsometry
        data to

      comments : list of strings, comments to add to output file
        Today's date/time and src_filename will be written as the first
        line of comments to hyps_filename

      verbose: boolean to turn on verbose output to stderr.

    Returns:
      True if successful.

    """
    comments = [
        "Hypsometry converted on %s from %s" %
        (str(dt.datetime.now()), src_filename)] + comments
    hyps = Hypsometry(comments=comments)

    df = pd.read_csv(src_filename)
    if len(df.columns) != 4:
        print("%s : expected 4 columns in input file, but found %d" %
              (__name__, len(df.columns)), file=sys.stderr)
        raise IndexError

    # Ignore and drop the first and third columns
    df.drop(df.columns[[0, 2]], axis=1, inplace=True)

    # Convert the next column's values (elevations) to ints
    # Set this column to be the Data Frame index,
    # and transpose rows/cols
    df[df.columns[0]] = df[df.columns[0]].astype(int)
    df.set_index(df.columns[0], inplace=True)
    hyps.data = df.transpose().copy()

    # Set the index to "NoDate" so Hypsometry knows what to do
    hyps.data.index = ["NoDate"]

    # Write as a charistools hypsometry object
    hyps.write(hyps_filename, verbose=verbose)

    return True


def Dem2Hypsometry(drainageID, modelEnv, contour_m=100,
                   outfile=None,
                   decimal_places=2,
                   verbose=False):
        """Convert raster DEM to Hypsometry for a specific drainageID.

        Uses input ModelEnv to read the DEM and basin_mask MODIS
        tile(s) for this drainageID, and calculates
        area_by_elevation for this drainageID at the requested
        contour levels.

        Raises RuntimeError for tile read errors on basin_mask or dems.

        Args:
          drainageID: drainage name string to use in basin_mask
            and dem filenames, e.g. "IN_Hunza_at_Danyour" or "IN_Hunza_GDBD"

          modelEnv: CHARIS ModelEnv object, initialized for local filesystem

          contour_m: integer, contour size, in meters, to use for output
            Hypsometry

          outfile: string, name of output Hypsometry file to create

          decimal_places: number of decimal places for formatted data values
            in outfile

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Area by elevation Hypsometry derived for this drainageID.

        """
        out_hyps = Hypsometry(comments=[
            "Hypsometry created : " + str(dt.datetime.now()),
            "Elevations in meters, contour at bottom of elevation band",
            "Area in square km"])

        # Get list of tileIDs for this drainageID
        tileList = modelEnv.tileIDs_for_drainage(drainageID=drainageID)

        for tileID in tileList:
            # Read the basin_mask and dem
            dem_filename = modelEnv.fixed_filename(
                type='dem',
                tileID=tileID, verbose=verbose)
            mask_filename = modelEnv.fixed_filename(
                type='basin_mask',
                drainageID=drainageID,
                tileID=tileID,
                verbose=verbose)

            try:
                # Cast dem to double so we can use NaNs later on as fills
                dem = read_tile(dem_filename).astype('f8')
                mask = read_tile(mask_filename)

            except RuntimeError:
                raise

            # Set elevations outside the drainageID to NaN
            outside_drainage = mask != 1
            dem[outside_drainage] = np.nan
            mask[outside_drainage] = 0

            # Calculate the sum by_elev for this tile only
            try:
                (this_tile_sum_by_elev,
                 this_tile_count_by_elev) = Raster2SumCountByElevation(
                     mask, dem, contour_m=contour_m)
            except NanElevationError:
                print("%s: %s: "
                      "no data match criteria, skipping this tile" %
                      (drainageID, tileID),
                      file=sys.stderr)
                continue

            # Coerce the by_elev dict into a DataFrame
            this_tile_sum_by_elev['Date'] = ['NoDate']
            df = pd.DataFrame.from_dict(this_tile_sum_by_elev)
            df.set_index('Date', inplace=True)
            df = df * ModelEnv.modis_tile_500m_pixel_area_km2

            # Add it to the NoData row in the hypsometry
            out_hyps.comments = out_hyps.comments + [
                "%s: basin_mask : %s" % (tileID, mask_filename),
                "%s: dem        : %s" % (tileID, dem_filename)]

            # And aggregate it onto the total hypsometry
            out_hyps.data = out_hyps.data.add(df, fill_value=0.)

        if outfile:
            out_hyps.write(filename=outfile,
                           decimal_places=decimal_places,
                           verbose=verbose)

        return out_hyps


def Fsca2Hypsometry(drainageID,
                    year,
                    modelEnv,
                    fsca_type='mod10a1_gf',
                    fsca_area='total',
                    ablation_method='albedo_mod10a1',
                    threshold=0.46,  # albedo, ranges from 0.0-1.0
                    use_daily_threshold_file=False,
                    unclass_to_egi=None,
                    start_doy=1,
                    stop_doy=None,
                    contour_m=100,
                    modice_nstrikes=1,
                    outfile=None,
                    soi_outfile=None,
                    egi_outfile=None,
                    decimal_places=2,
                    verbose=False):
        """Convert raster fractional SCA to Hypsometry for a specific
        drainageID.

        Uses input ModelEnv to read required input raster tile(s) for
        this drainageID and calculates fSCA_by_elevation at the requested
        contour levels.

        Default start/stop_doy are dates in the fSCA snow tile cube.

        Raises ValueError for invalid input combinations.
        Raises RuntimeError for errors reading input raster tile data.

        Args:
          drainageID: drainage name string to use in choosing input
            files to read, e.g. "IN_Hunza_at_Danyour" or "IN_Hunza_GDBD"

          year: integer, 4-digit year to use in choosing input files to
            read

          modelEnv: CHARIS ModelEnv object, initialized for local filesystem

          fsca_type: string, source data set for input fSCA data, one of:
            'mod10a1_gf': MOD10A1 gap-filled, fractional snow cover
            'modscag_gf': MODSCAG gap-filled, fractional snow cover

          fsca_area: string, surface to calculate areas over, one of:
            'total': total fSCA, regardless of underlying surface,
            'SOL': snow-on-land, ignores area on modice_min05yr surfaces
            'SOI/EGI': snow-on-ice vs. exposed-glacier-ice, calculates
                   area on 'accumulation' and ablation surfaces
                   as determined by ablation_method and threshold

          ablation_method: string, one of:
            'albedo_mod10a1': use MOD10A1 shortwave albedo for surface
              discriminator
            'grsize_scag': use MODSCAG grain size for surface discriminator

          threshold: threshold to discriminate
            ablation/accumulation surfaces for ablation_method,
            floating point for albedo threshold (0.0 - 1.00), or
            integer for grain size threshold (1-1100 microns);
            this value will be ignored if use_daily_threshold_file=True

          use_daily_threshold_file: boolean, default False
            if True, use daily descriminator thresholds from threshold
            file in modelEnv

          unclass_to_egi: float, in range 0.0 - 1.0 or None.
            Proportion of unclassifiable on-ice pixels
            to allocate to EGI.  Default (None) means to use the
            proportions of SOI to EGI and allocate the unclassifiable
            pixels to SOI and EGI in the same proportion.  A value
            of 0.25 will apportion 25% (75%) of unclassified on-ice
            pixels to EGI (SOI).

          start_doy: integer, start day of year (1-366)

          stop_doy: integer, stop day of year (1-366)

          contour_m: integer, contour size, in meters, to use for output
            Hypsometry

          modice_nstrikes: modice files differ by number of strikes,
            1, 2 or 3.

          outfile: string, name of output Hypsometry file to create for
            fsca_area='total' or 'SOL'

          soi_outfile : string, name of SOI Hypsometry file to create for
            fsca_area='SOI/EGI'

          egi_outfile : string, name of EGI Hypsometry file to create for
            fsca_area='SOI/EGI'

          decimal_places: number of decimal places for formatted data values
            in outfile

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Dict with requested area by elevation Hypsometry derived
            for this drainageID:
            fsca_area    out Dict Hypsometry keys
            ---------    --------------------------------
            "total"        "total"
            "SOL"          "SOL"
            "SOI/EGI"      "SOI"
                           "EGI"
                           "UNCLASS"
                           "ice"

        """
        out = {}

        valid_area = {"total": "Total SCA",
                      "SOL": "Snow_on_land",
                      "SOI/EGI": "Snow_on_ice/Exposed_glacier_ice"}

        if fsca_area not in valid_area.keys():
            print("%s : invalid fsca_area %s" % (__name__, fsca_area),
                  file=sys.stderr)
            print("%s : should be one of: %s" % (__name__, str(valid_area.keys())),
                  file=sys.stderr)
            raise ValueError

        if unclass_to_egi:
            if unclass_to_egi < 0. or unclass_to_egi > 1.:
                print("%s : unclass_to_egi=%f out of range [0.,1.]" % (
                    __name__, unclass_to_egi),
                      file=sys.stderr)
                raise ValueError

        # The partitions for ice surfaces require an ablation_method and threshold
        if fsca_area == "SOI/EGI":
            if ablation_method == 'grsize_scag':
                ablation_method_varname = 'grain_size'
            elif ablation_method == 'albedo_mod10a1':
                ablation_method_varname = 'albedo_shortwave'
            else:
                print("%s : %s is not valid ablation_method for SOI/EGI"
                      % (__name__, ablation_method),
                      file=sys.stderr)
                raise ValueError

            if use_daily_threshold_file:
                threshold = "fromFile"
                daily_threshold = Threshold(
                    filename=modelEnv.forcing_filename(
                        type='threshold'))
            else:
                daily_threshold = Threshold(threshold=threshold)

        # For SOI/EGI there will be three hypsometries,
        # to keep track of SOI/EGI and unclassified areas of the glaciers
        # SOI and EGI will be output, but unclass_hyps will only
        # be temporary.
        # Otherwise there will just be one that is output.
        if fsca_area == "SOI/EGI":
            soi_hyps = Hypsometry(comments=[
                "Hypsometry created : " + str(dt.datetime.now()),
                "Elevations in meters, contour at bottom of elevation band",
                "Snow_on_ice area in square km",
                "Files used to derive this hypsometry data:"])
            egi_hyps = Hypsometry(comments=[
                "Hypsometry created : " + str(dt.datetime.now()),
                "Elevations in meters, contour at bottom of elevation band",
                "Exposed_glacier_ice area in square km",
                "Files used to derive this hypsometry data:"])
            unclass_hyps = Hypsometry()
            ice_hyps = Hypsometry()
        else:
            out_hyps = Hypsometry(comments=[
                "Hypsometry created : " + str(dt.datetime.now()),
                "Elevations in meters, contour at bottom of elevation band",
                valid_area[fsca_area] + " area in square km",
                "Files used to derive this hypsometry data:"])

        # Get list of tileIDs for this drainageID
        tileList = modelEnv.tileIDs_for_drainage(drainageID=drainageID)

        for tileID in tileList:
            # Read the basin_mask, dem and year of fSCA data for this tile
            dem_filename = modelEnv.fixed_filename(
                type='dem',
                tileID=tileID, verbose=verbose)
            mask_filename = modelEnv.fixed_filename(
                type='basin_mask',
                drainageID=drainageID,
                tileID=tileID,
                verbose=verbose)
            fsca_filename = modelEnv.forcing_filename(
                type=fsca_type,
                tileID=tileID,
                year=year,
                verbose=verbose)
            modice_filename = modelEnv.fixed_filename(
                type='modice_min05yr',
                drainageID=drainageID,
                tileID=tileID,
                modice_nstrikes=modice_nstrikes,
                verbose=verbose)
            comments = ["%s: basin_mask   : %s" % (tileID, mask_filename),
                        "%s: dem          : %s" % (tileID, dem_filename),
                        "%s: modice       : %s" % (tileID, modice_filename),
                        "%s: fSCA         : %s" % (tileID, fsca_filename)]
            if fsca_area == "SOI/EGI":
                soi_hyps.comments = soi_hyps.comments + comments
                egi_hyps.comments = egi_hyps.comments + comments
            else:
                out_hyps.comments = out_hyps.comments + comments

            if fsca_area == "SOI/EGI":
                ablation_method_filename = modelEnv.forcing_filename(
                    type=ablation_method,
                    tileID=tileID,
                    year=year,
                    verbose=verbose)
                comments = [
                    "%s: ablation_method (threshold=%s, unclass_to_egi=%s): %s"
                    % (tileID, threshold, unclass_to_egi,
                       ablation_method_filename)]
                soi_hyps.comments = soi_hyps.comments + comments
                egi_hyps.comments = egi_hyps.comments + comments

            try:
                # Cast dem to double so we can use NaNs later on
                # Just read the mask data
                # Initialize the reader for the fSCA cube data
                dem = read_tile(dem_filename).astype('f8')
                mask = read_tile(mask_filename)
                cube = ModisTileCube(fsca_filename, 'fsca')
                modice = read_tile(modice_filename, 'modice_min_year_mask')
                if fsca_area == "SOI/EGI":
                    ablation_method_cube = ModisTileCube(
                        ablation_method_filename, ablation_method_varname)

            except RuntimeError:
                raise

            if stop_doy is None:
                stop_doy = cube.ndays

            # Allocate an array for a tile of fSCA data
            data = np.zeros((cube.nrows, cube.ncols))

            # Set elevations outside the drainageID to NaN
            outside_drainage = mask != 1
            dem[outside_drainage] = np.nan

            # For snow_on_land, set elevations
            # on the modice ice locations to NaN
            # For SOI/EGI, set elevations
            # off the modice ice locations to NaN
            if fsca_area == 'SOL':
                on_ice = modice == 2
                dem[on_ice] = np.nan
            elif fsca_area == 'SOI/EGI':
                off_ice = modice != 2
                dem[off_ice] = np.nan

            # Loop for requested doy range
            for doy in np.arange(stop_doy - start_doy + 1) + start_doy:
                if verbose:
                    print("> %s(%s): %s: nstrikes(%d): year=%s, doy= %s" %
                          (drainageID, fsca_area, tileID, modice_nstrikes, year, doy),
                          file=sys.stderr)

                # Set fsca values outside the drainageID to zero
                data = cube.read(doy)
                data[outside_drainage] = 0.

                # For snow_on_land, set scag values on ice to zero
                # For SOI/EGI, set scag values off_ice to zero
                # And partition the rest using the requested ablation_method
                if fsca_area == "SOL":
                    data[on_ice] = 0.
                elif fsca_area == "SOI/EGI":

                    data[off_ice] = 0.

                    # Read the discriminator data (albedo or grsize)
                    # for this tile/day
                    disc_data = ablation_method_cube.read(doy)
                    disc_data_mask = np.ma.getmaskarray(disc_data)

                    # Make 3 copies of the fsca_array to figure out
                    # what's happening on the bright/dark/unclassified
                    # areas on-glacier
                    bright_data = data.copy()
                    dark_data = data.copy()

                    # Make a mask of 0/1, with 1s set anyplace where the
                    # mask is True (so there's no albedo/grainsize)
                    # and modice is on.  Restrict these to the drainage only.
                    unclass_data = np.logical_and(
                        disc_data.mask, modice == 2).astype(float)
                    unclass_data[outside_drainage] = 0.

                    # Depending on ablation_method, turn off
                    # the "darker" areas so we are only
                    # counting the "brighter" part of the surface
                    this_threshold = daily_threshold.value(doy=doy)
                    if ablation_method == 'grsize_scag':
                        bright_data[np.logical_or(
                            disc_data_mask,
                            disc_data > this_threshold)] = 0.
                        dark_data[np.logical_or(
                            disc_data_mask,
                            disc_data <= this_threshold)] = 0.
                    elif ablation_method == 'albedo_mod10a1':
                        bright_data[np.logical_or(
                            disc_data_mask,
                            disc_data < this_threshold)] = 0.
                        dark_data[np.logical_or(
                            disc_data_mask,
                            disc_data >= this_threshold)] = 0.

                if fsca_area != "SOI/EGI":
                    # Calculate the fsca_by_elev for this surface's tile/doy only
                    try:
                        (this_tile_sum_by_elev,
                         this_tile_count_by_elev) = Raster2SumCountByElevation(
                             data, dem, contour_m=contour_m)
                    except NanElevationError:
                        print("%s(%s): %s: nstrikes(%d): year=%s, doy= %s: "
                              "no data match criteria, skipping this tile" %
                              (drainageID, fsca_area, tileID, modice_nstrikes,
                               year, doy),
                              file=sys.stderr)
                        continue

                    # Coerce the dict into a DataFrame for this date
                    df = pd.DataFrame.from_dict(this_tile_sum_by_elev)
                    df['Date'] = [dt.datetime.strptime(
                        "%4d%03d" % (year, doy), '%Y%j')]
                    df.set_index('Date', inplace=True)
                    df = df * ModelEnv.modis_tile_500m_pixel_area_km2

                else:
                    # Bright data:
                    # Calculate the fsca_by_elev for this surface's tile/doy only
                    # The count that's returned is the total MODICE area at
                    # each elevation band
                    try:
                        (this_tile_bright_sum_by_elev,
                         this_tile_ice_by_elev) = Raster2SumCountByElevation(
                             bright_data, dem, contour_m=contour_m)
                    except NanElevationError:
                        print("%s(%s): %s: nstrikes(%d): year=%s, doy= %s: "
                              "no data match criteria, skipping this tile" %
                              (drainageID, fsca_area, tileID, modice_nstrikes,
                               year, doy),
                              file=sys.stderr)
                        continue

                    # Coerce the dict into a DataFrame for this date
                    bright_df = pd.DataFrame.from_dict(this_tile_bright_sum_by_elev)
                    bright_df['Date'] = [dt.datetime.strptime(
                        "%4d%03d" % (year, doy), '%Y%j')]
                    bright_df.set_index('Date', inplace=True)
                    bright_df = bright_df * ModelEnv.modis_tile_500m_pixel_area_km2

                    ice_df = pd.DataFrame.from_dict(this_tile_ice_by_elev)
                    ice_df['Date'] = [dt.datetime.strptime(
                        "%4d%03d" % (year, doy), '%Y%j')]
                    ice_df.set_index('Date', inplace=True)
                    ice_df = ice_df * ModelEnv.modis_tile_500m_pixel_area_km2

                    # Dark data:
                    # Calculate the fsca_by_elev for this surface's tile/doy only
                    try:
                        (this_tile_dark_sum_by_elev,
                         this_tile_count_by_elev) = Raster2SumCountByElevation(
                             dark_data, dem, contour_m=contour_m)
                    except NanElevationError:
                        continue

                    # Coerce the dict into a DataFrame for this date
                    dark_df = pd.DataFrame.from_dict(this_tile_dark_sum_by_elev)
                    dark_df['Date'] = [dt.datetime.strptime(
                        "%4d%03d" % (year, doy), '%Y%j')]
                    dark_df.set_index('Date', inplace=True)
                    dark_df = dark_df * ModelEnv.modis_tile_500m_pixel_area_km2

                    # Unclassified data:
                    # Calculate the fsca_by_elev for this surface's tile/doy only
                    try:
                        (this_tile_unclass_sum_by_elev,
                         this_tile_count_by_elev) = Raster2SumCountByElevation(
                             unclass_data, dem, contour_m=contour_m)
                    except NanElevationError:
                        continue

                    # Coerce the dict into a DataFrame for this date
                    unclass_df = pd.DataFrame.from_dict(this_tile_unclass_sum_by_elev)
                    unclass_df['Date'] = [dt.datetime.strptime(
                        "%4d%03d" % (year, doy), '%Y%j')]
                    unclass_df.set_index('Date', inplace=True)
                    unclass_df = unclass_df * ModelEnv.modis_tile_500m_pixel_area_km2

                # Aggregate this tile/date onto the total hypsometry
                # for this year
                if fsca_area == "SOI/EGI":
                    ice_hyps.data = ice_hyps.data.add(ice_df, fill_value=0.)
                    soi_hyps.data = soi_hyps.data.add(bright_df, fill_value=0.)
                    egi_hyps.data = egi_hyps.data.add(dark_df, fill_value=0.)
                    unclass_hyps.data = unclass_hyps.data.add(unclass_df, fill_value=0.)
                else:
                    out_hyps.data = out_hyps.data.add(df, fill_value=0.)

            cube.close()
            if fsca_area == "SOI/EGI":
                ablation_method_cube.close()

        if fsca_area == "total":
            if outfile:
                out_hyps.write(filename=outfile,
                               decimal_places=decimal_places,
                               verbose=verbose)
            out["total"] = out_hyps
        elif fsca_area == "SOL":
            if outfile:
                out_hyps.write(filename=outfile,
                               decimal_places=decimal_places,
                               verbose=verbose)
            out["SOL"] = out_hyps
        else:
            # Do final accounting for the unclassifiable area on-glacier
            # First, find out the proportion of fSCA that was on the
            # classified on-ice area this will be a value from 0.0 - 1.0
            # Only this proportion of the unclassified area will be
            # considered.  This uses the nearby data to estimate
            # how much of the unclassifiable data that we should
            # re-assign.  Re-assign the remainder either
            # proportionately to EGI/SOI (the default), or according
            # to optional input argument
            soi_and_egi = soi_hyps.data + egi_hyps.data
            proportion = soi_and_egi / (ice_hyps.data - unclass_hyps.data)
            if unclass_to_egi:
                extra_soi = (unclass_hyps.data * proportion * (
                    1. - unclass_to_egi)).fillna(value=0.)
                extra_egi = (unclass_hyps.data * proportion * (
                    unclass_to_egi)).fillna(value=0.)
            else:
                extra_soi = (unclass_hyps.data * proportion * (
                    soi_hyps.data / soi_and_egi)).fillna(value=0.)
                extra_egi = (unclass_hyps.data * proportion * (
                    egi_hyps.data / soi_and_egi)).fillna(value=0.)

            soi_hyps.data = soi_hyps.data.add(extra_soi, fill_value=0.)
            egi_hyps.data = egi_hyps.data.add(extra_egi, fill_value=0.)

            # Return SOI and EGI hypsometries
            if soi_outfile:
                soi_hyps.write(filename=soi_outfile,
                               decimal_places=decimal_places,
                               verbose=verbose)
            if egi_outfile:
                egi_hyps.write(filename=egi_outfile,
                               decimal_places=decimal_places,
                               verbose=verbose)

            out["SOI"] = soi_hyps
            out["EGI"] = egi_hyps

        return out


def Modice2Hypsometry(drainageID,
                      modelEnv,
                      contour_m=100,
                      modice_nstrikes=1,
                      outfile=None,
                      decimal_places=2,
                      verbose=False):
        """Convert raster MODICE to Hypsometry for a specific drainageID.

        Uses input ModelEnv to read the DEM, basin_mask and MODICE tile(s)
        for this drainageID, and calculates MODICE area_by_elevation for
        this drainageID at the requested contour levels.

        Raises RuntimeError for tile read errors on input raster tiles.

        Args:
          drainageID: drainage name string to use in basin_mask, dem,
            and MODICE filenames, e.g. "IN_Hunza_at_Danyour" or "IN_Hunza_GDBD"

          modelEnv: CHARIS ModelEnv object, initialized for local filesystem

          contour_m: integer, contour size, in meters, to use for output
            Hypsometry

          modice_nstrikes: modice files differ by number of strikes,
            1, 2 or 3.

          outfile: string, name of output Hypsometry file to create

          decimal_places: number of decimal places for formatted data values
            in outfile

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          MODICE area by elevation Hypsometry derived for this drainageID.

        """
        out_hyps = Hypsometry(comments=[
            "Hypsometry created : " + str(dt.datetime.now()),
            "Elevations in meters, contour at bottom of elevation band",
            "MODICE area in square km"])

        # Get list of tileIDs for this drainageID
        tileList = modelEnv.tileIDs_for_drainage(drainageID=drainageID)

        for tileID in tileList:
            # Read the basin_mask, dem and modice data for this tile
            dem_filename = modelEnv.fixed_filename(
                type='dem',
                tileID=tileID, verbose=verbose)
            mask_filename = modelEnv.fixed_filename(
                type='basin_mask',
                drainageID=drainageID,
                tileID=tileID,
                verbose=verbose)
            modice_filename = modelEnv.fixed_filename(
                type='modice_min05yr',
                tileID=tileID,
                modice_nstrikes=modice_nstrikes,
                verbose=verbose)

            try:
                # Cast dem to double so we can use NaNs later on
                dem = read_tile(dem_filename).astype('f8')
                mask = read_tile(mask_filename)
                modice = read_tile(modice_filename, 'modice_min_year_mask')

            except RuntimeError:
                raise

            # modice values of 2 are ice
            # 1=water and 0 is non-ice
            # Set everything but ice to zeros
            # and set ice to 1 for full weighted area
            modice_ice_val = 2
            data = np.zeros(modice.shape)
            data[modice == modice_ice_val] = 1.

            # Set data values outside the drainageID to zero
            # Set elevations outside the drainageID to NaN
            outside_drainage = mask != 1
            data[outside_drainage] = 0.
            dem[outside_drainage] = np.nan

            # Calculate the modice_by_elev for this tile only
            try:
                (this_tile_sum_by_elev,
                 this_tile_count_by_elev) = Raster2SumCountByElevation(
                     data, dem, contour_m=contour_m)
            except NanElevationError:
                print("%s: %s: nstrikes(%d): "
                      "no data match criteria, skipping this tile" %
                      (drainageID, tileID, modice_nstrikes),
                      file=sys.stderr)
                continue

            # Coerce the dict into a DataFrame
            this_tile_sum_by_elev['Date'] = ['NoDate']
            df = pd.DataFrame.from_dict(this_tile_sum_by_elev)
            df.set_index('Date', inplace=True)
            df = df * ModelEnv.modis_tile_500m_pixel_area_km2

            # Add it to the NoData row in the hypsometry
            out_hyps.comments = out_hyps.comments + [
                "%s: basin_mask : %s" % (tileID, mask_filename),
                "%s: dem        : %s" % (tileID, dem_filename),
                "%s: modice     : %s" % (tileID, modice_filename)]

            # And aggregate it onto the total hypsometry
            out_hyps.data = out_hyps.data.add(df, fill_value=0.)

        if outfile:
            out_hyps.write(filename=outfile,
                           decimal_places=decimal_places,
                           verbose=verbose)

        return out_hyps


def Temperature2Hypsometry(drainageID, year, modelEnv,
                           start_doy=1, stop_doy=None,
                           contour_m=100, kelvin_input=True,
                           outfile=None,
                           decimal_places=2,
                           verbose=False):
        """Convert raster temperature data to Hypsometry for a specific
        drainageID.

        Uses input ModelEnv to read temperature tiles(s) for this drainageID
        and calculates average temperature_by_elevation at the requested
        contour levels.

        Default start/stop_doy are dates in the temperature tile cube.

        Raises RuntimeError for errors reading input raster tile data.

        Args:
          drainageID: drainage name string to use in choosing input
            files to read, e.g. "IN_Hunza_at_Danyour" or "IN_Hunza_GDBD"

          year: integer, 4-digit year to use in choosing input files to
            read

          modelEnv: CHARIS ModelEnv object, initialized for local filesystem

          start_doy: integer, start day of year (1-366)

          stop_doy: integer, stop day of year (1-366)

          contour_m: integer, contour size, in meters, to use for output
            Hypsometry

          kelvin_input: boolean, True if temperature cube units are Kelvins,
            False if Celcius (this switch should be removed when units
            attribute is populated in temperature cubes)

          outfile: string, name of output Hypsometry file to create

          decimal_places: number of decimal places for formatted data values
            in outfile

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Average temperature by elevation Hypsometry derived for this
          drainageID.

        """
        # Define 3 hypsometry objects:
        # 1) for the sum of temperatures by elevation (across multiple tiles)
        # 2) for the count of pixels by elevation (across multiple tiles)
        # 3) for the output data that will be the average temperature by elevation
        sum_hyps = Hypsometry()
        count_hyps = Hypsometry()
        out_hyps = Hypsometry(comments=[
            "Hypsometry created : " + str(dt.datetime.now()),
            "Elevations in meters, contour at bottom of elevation band",
            "Average temperature in degrees Celsius",
            "Files used to derive this hypsometry data:"])

        # Get list of tileIDs for this drainageID
        tileList = modelEnv.tileIDs_for_drainage(drainageID=drainageID)

        for tileID in tileList:
            # Read the basin_mask, dem and year of data for this tile
            dem_filename = modelEnv.fixed_filename(
                type='dem',
                tileID=tileID, verbose=verbose)
            mask_filename = modelEnv.fixed_filename(
                type='basin_mask',
                drainageID=drainageID,
                tileID=tileID,
                verbose=verbose)
            temperature_filename = modelEnv.forcing_filename(
                type='temperature',
                tileID=tileID,
                year=year,
                verbose=verbose)
            out_hyps.comments = out_hyps.comments + [
                "%s: basin_mask   : %s" % (tileID, mask_filename),
                "%s: dem          : %s" % (tileID, dem_filename),
                "%s: temperature  : %s" % (tileID, temperature_filename)]

            try:

                # Cast dem to double so we can use NaNs later on
                # Just read the mask data
                # Initialize the reader for the modscag_gf cube data
                dem = read_tile(dem_filename).astype('f8')
                mask = read_tile(mask_filename)
                cube = ModisTileCube(temperature_filename, 'tsurf')

            except RuntimeError:
                raise

            if stop_doy is None:
                stop_doy = cube.ndays

            # Allocate an array for a tile of temperature data
            data = np.zeros((cube.nrows, cube.ncols))

            # Set elevations outside the drainageID to NaN
            outside_drainage = mask != 1
            dem[outside_drainage] = np.nan

            # Loop for requested doy range
            for doy in np.arange(stop_doy - start_doy + 1) + start_doy:
                yyyydoy = dt.datetime.strptime("%4d%03d" % (year, doy), '%Y%j')
                yyyydoy_str = yyyydoy.strftime('%Y%j')
                yyyymmdd_str = yyyydoy.strftime('%Y%m%d')
                if verbose:
                    print("> Temperature: "
                          "TileID: %s, date: yyyydoy=%s, yyyymmdd=%s" %
                          (tileID, yyyydoy_str, yyyymmdd_str),
                          file=sys.stderr)

                # Read temperature data for this doy
                data = cube.read(doy)

                # Set temperature values outside the drainageID to NaN
                data[outside_drainage] = np.nan

                # Calculate the sum of temperatures and number of
                # pixels for this tile/doy only
                try:
                    (this_tile_sum_by_elev,
                     this_tile_count_by_elev) = Raster2SumCountByElevation(
                         data, dem, contour_m=contour_m)
                except NanElevationError:
                    print("%s: %s: year=%s, doy= %s: "
                          "no data match criteria, skipping this tile" %
                          (drainageID, tileID, year, doy),
                          file=sys.stderr)
                    continue

                # Coerce the returned sum and count dictionaries
                # into DataFrames for this date and add them
                # to the cumulative DataFrames
                df_sum = pd.DataFrame.from_dict(this_tile_sum_by_elev)
                df_sum['Date'] = [yyyydoy]
                df_sum.set_index('Date', inplace=True)
                sum_hyps.data = sum_hyps.data.add(df_sum, fill_value=0.)

                df_count = pd.DataFrame.from_dict(this_tile_count_by_elev)
                df_count['Date'] = [yyyydoy]
                df_count.set_index('Date', inplace=True)
                count_hyps.data = count_hyps.data.add(df_count, fill_value=0.)

            cube.close()

        # Do the final calculation of average temperature for all tiles
        count_hyps.data[count_hyps.data == 0] = 1
        out_hyps.data = sum_hyps.data / count_hyps.data

        # If input units are Kelvins,
        # convert temperature units from Kelvins to Celsius
        if kelvin_input:
            out_hyps.data = out_hyps.data - KelvinOffset

        if outfile:
            out_hyps.write(filename=outfile,
                           decimal_places=decimal_places,
                           verbose=verbose)

        return out_hyps


def ET2Hypsometry(drainageID, modelEnv,
                  start_yyyymm=200101, stop_yyyymm=200112,
                  contour_m=100,
                  outfile=None,
                  decimal_places=2,
                  verbose=False):
        """Convert monthly raster evapotranspiration (ET) data to Hypsometry
        for a specific drainageID.

        Uses input ModelEnv to read ET tiles(s) for this drainageID
        and calculates sum of ET at the requested contour levels.

        Default start_yyyymm is beginning of MOD16 record (200101),
        stop_yyyymm is end of record (default=200112)

        Raises ValueError if stop_yyyymm < start_yyyymm.
        Raises RuntimeError for errors reading input raster tile data.

        Args:
          drainageID: drainage name string to use in choosing input
            files to read, e.g. "IN_Hunza_at_Danyour" or "IN_Hunza_GDBD"

          modelEnv: CHARIS ModelEnv object, initialized for local filesystem

          start_yyyymm: integer, beginning year/month default 200101

          stop_yyyymm: integer, ending year/month default is last file in ET folder

          contour_m: integer, contour size, in meters, to use for output
            Hypsometry

          outfile: string, name of output Hypsometry file to create

          decimal_places: number of decimal places for formatted data values
            in outfile

          verbose: boolean to turn on verbose output to stderr.

        Returns:
          Monthly sum of ET by elevation Hypsometry derived for this drainageID.

        """
        if stop_yyyymm < start_yyyymm:
            raise ValueError("%s : stop=%d precedes start=%d" % (
                __name__, stop_yyyymm, start_yyyymm))

        out_hyps = Hypsometry(comments=[
            "Hypsometry created : " + str(dt.datetime.now()),
            "Elevations in meters, contour at bottom of elevation band",
            "MOD16 ET in km^3",
            "Files used to derive this hypsometry data:"])

        # Get list of tileIDs for this drainageID
        tileList = modelEnv.tileIDs_for_drainage(drainageID=drainageID)

        # Make a list of yyyymm values
        yyyymmList = monthList(start_yyyymm, stop_yyyymm)

        # Fore each tile, read the DEM and drainage mask once,
        # Then loop through each month and year to calculate ET from
        # this tile
        for tileID in tileList:

            # Read the basin_mask, dem and year of data for this tile
            dem_filename = modelEnv.fixed_filename(
                type='dem',
                tileID=tileID, verbose=verbose)
            mask_filename = modelEnv.fixed_filename(
                type='basin_mask',
                drainageID=drainageID,
                tileID=tileID,
                verbose=verbose)
            out_hyps.comments = out_hyps.comments + [
                "%s: basin_mask   : %s" % (tileID, mask_filename),
                "%s: dem          : %s" % (tileID, dem_filename)]

            try:
                # Cast dem to double so we can use NaNs later on
                # Just read the mask data
                dem = read_tile(dem_filename).astype('f8')
                mask = read_tile(mask_filename)

            except RuntimeError:
                raise

            # Allocate an array for a tile of et data
            # data = np.zeros((cube.nrows, cube.ncols))

            # Set elevations outside the drainageID to NaN
            outside_drainage = mask != 1
            dem[outside_drainage] = np.nan

            for yyyy, mm in yyyymmList:

                this_yyyymm_dt = dt.datetime(yyyy, mm, 1)

                if verbose:
                    print("> Calculating ET for tile=%s and yyyymm=%d, %d, %s" % (
                        tileID, yyyy, mm, this_yyyymm_dt), file=sys.stderr)

                et_filename = modelEnv.forcing_filename(
                    type='mod16a2',
                    tileID=tileID,
                    year=yyyy,
                    month=mm,
                    verbose=verbose)

                out_hyps.comments = out_hyps.comments + [
                    "%s: ET  : %s" % (tileID, et_filename)]

                # Read ET data for this yyyymm
                data = read_evapotranspiration_tile(et_filename)

                # Set ET values outside the drainageID to NaN
                data[outside_drainage] = np.nan

                # Calculate the total ET for this tile/month only
                try:
                    (this_tile_sum_by_elev,
                     this_tile_count_by_elev) = Raster2SumCountByElevation(
                         data, dem, contour_m=contour_m)
                except NanElevationError:
                    print("%s: %s: year=%s, mm= %s: "
                          "no data match criteria, skipping this tile" %
                          (drainageID, tileID, yyyy, mm),
                          file=sys.stderr)
                    continue

                # Coerce the returned sum and count dictionaries
                # into DataFrames for this date and add them
                # to the cumulative DataFrames
                df_sum = pd.DataFrame.from_dict(this_tile_sum_by_elev)
                df_sum['Date'] = [this_yyyymm_dt]
                df_sum.set_index('Date', inplace=True)
                out_hyps.data = out_hyps.data.add(df_sum, fill_value=0.)

        if outfile:
            out_hyps.write(filename=outfile,
                           decimal_places=decimal_places,
                           verbose=verbose)

        return out_hyps


def monthList(start_yyyymm, stop_yyyymm):
    """Return a list of months from start to stop.

    Args:
      start_yyyymm: integer start month and year

      stop_yyyymm: integer stop month and year

      Raises ValueError if stop_yyyymm < start_yyyymm.

      Returns: list with each month from start to stop
    """
    start_yyyy = start_yyyymm / 100
    start_mm = start_yyyymm - (start_yyyy * 100)
    dt_start = dt.datetime(start_yyyy, start_mm, 1)
    stop_yyyy = stop_yyyymm / 100
    stop_mm = stop_yyyymm - (stop_yyyy * 100)
    dt_stop = dt.datetime(stop_yyyy, stop_mm, 1)

    return [(d.year, d.month) for d in rrule(MONTHLY,
                                             dtstart=dt_start,
                                             until=dt_stop)]


def Raster2SumCountByElevation(raster_data, raster_elevation,
                               contour_m=100,
                               min_contour_m=None,
                               max_contour_m=None,
                               verbose=False):
        """Convert raster data to count by elevation.

        Workhorse function for *2Hypsometry functions.  This function
        sums the input raster_data by the requested elevation bands.

        Raises ValueError if raster_elevation is all nans.

        Args:
          raster_data: ndarray with raster data to sum by elevation

          raster_elevation: ndarray with corresponding elevation data,
            must be same shape as raster_data

          contour_m: integer, contour size, in meters, to use for output
            Hypsometry

          min_contour_m: integer, bottom of lowest contour level, in meters
            If None, determine min_contour_m from elevation data

          max_contour_m: integer, top of highest contour level, in meters
            If None, determine max_contour_m from elevation data

        Returns:
            2 dictionaries, with
            { el: sum of raster_data at each elevation band }
            { el: pixel count of raster-data at each elevation band }

        """
        # Input raster shapes need to match for the rest to work
        # This behavior should probably change to raising an appropriate
        # exception
        assert np.shape(raster_elevation) == np.shape(raster_data)

        if (math.isnan(np.nanmin(raster_elevation)) and math.isnan(
                np.nanmax(raster_elevation))):
            raise NanElevationError('raster_elevation data is all Nans')

        # Figure out what elevation bands to use
        # User inputs override defaults:
        # Default is:
        # bottom of lowest contour interval that has elevation data here
        # and top of highest contour interval that has elevation data here
        if min_contour_m is None:
            min_contour_m = np.floor(
                np.nanmin(raster_elevation) / contour_m) * contour_m

        if max_contour_m is None:
            max_contour_m = (np.ceil(
                (np.nanmax(raster_elevation) + 1.) / contour_m) * contour_m)

        bins = np.arange(min_contour_m, max_contour_m, step=contour_m)

        # Get the indices of raster elements at these elevation bands
        # digitize returns an array the same size as the elevation raster
        # array, populated with the bin number (bins start at 1 for lowest
        # elevation contour) for that element
        ind = np.digitize(raster_elevation, bins)
        data_sum = {}
        data_count = {}
        for i in np.arange(len(bins)):
            data_in_contour = raster_data[ind == i+1]
            elevations_in_contour = raster_elevation[ind == i+1]

            # Filter for the non-Nan elevations at this contour level
            data_in_contour = data_in_contour[np.isfinite(elevations_in_contour)]

            # Handle case where sum is nan, (no values at this elevation)
            this_sum = data_in_contour.sum()
            if np.isfinite(this_sum):
                data_sum[bins[i]] = [data_in_contour.sum()]
            else:
                data_sum[bins[i]] = [0.]

            data_count[bins[i]] = [data_in_contour.size]

        return(data_sum, data_count)


def ForcingsForDrainageID(drainageID,
                          year,
                          modelEnv,
                          fsca_type='mod10a1_gf',
                          ablation_method='albedo_mod10a1',
                          thresholds=[0.46],
                          start_doy=1, stop_doy=None,
                          contour_m=100,
                          modice_nstrikes=1,
                          decimal_places=2,
                          skip_temperatures=False,
                          verbose=False):
    """Create a set of forcing Hypsometry files for the input drainageID
    and set of discriminator thresholds.

    Uses input ModelEnv to read required input raster tiles(s) for this
    drainageID, and calculates the following Hypsometry forcing files at
    the requested contour levels:
      temperature_by_elev
      SOI_by_elev
      SOL_by_elev
      EGI_by_elev

    Default start/stop_doy are the dates in the temperature tile cube and
    fSCA snow tile cube.

    Raises RuntimeError if any hypsometry file error occurs.

    Args:
      drainageID: string, used for drainageID wildcard in input and
         output filenames

      year: 4-digit year to process

      modelEnv: CHARIS ModelEnv object, initialized for local filesystem

      fsca_type: string, source data set for input fSCA data, one of:
         'mod10a1_gf': MOD10A1 gap-filled, fractional snow cover
         'modscag_gf': MODSCAG gap-filled, fractional snow cover

      ablation_method: string, one of:
         'albedo_mod10a1': use MOD10A1 shortwave albedo for surface
             discriminator
         'grsize_scag': use MODSCAG grain size for surface discriminator

      thresholds: list of discriminator thresholds
         ablation/accumulation surfaces for ablation_method,
         floating point for albedo threshold (0.0 - 1.00), or
         integer for grain size threshold (1-1100 microns);
         can also be 'fromFile'

      start_doy: integer, day of year to begin, default=1 (Jan 1)

      stop_doy: integer, day of year to stop, default=None (all data
         in temperature/fsca data cubes will be processed

      contour_m: float, contour size in meters, default=100.

      modice_nstrikes: integer, 1-3 MODICE strikes to use for ice map
         default=1

      decimal_places: integer, number of decimal places to write to
         output hypsometries, default=2

      skip_temperatures: Boolean, don't produce temperatures, only
         surface areas, default=False

      verbose: Boolean, verbose output to stderr, default=False

    Returns: True on success, when all 4 files are created.  False otherwise.
    """
    if not skip_temperatures:

        # Do temperature2Hypsometry
        temperature_hyps_filename = modelEnv.hypsometry_filename(
            type='temperature_by_elevation',
            drainageID=drainageID,
            year=year,
            contour_m=contour_m)
        if not Temperature2Hypsometry(
                drainageID=drainageID,
                year=year,
                modelEnv=modelEnv,
                start_doy=start_doy,
                stop_doy=stop_doy,
                contour_m=contour_m,
                outfile=temperature_hyps_filename,
                decimal_places=decimal_places,
                verbose=verbose):
            print("%s : Error making Hypsometry %s for %s" %
                  (__name__,
                   temperature_hyps_filename,
                   drainageID),
                  file=sys.stderr)
            raise RuntimeError

        if verbose:
            print("> %s : wrote %s" %
                  (__name__, temperature_hyps_filename),
                  file=sys.stderr)

    # Do SOL
    sol_hyps_filename = modelEnv.hypsometry_filename(
        type='snow_on_land_by_elevation',
        drainageID=drainageID,
        year=year,
        contour_m=contour_m,
        modice_nstrikes=modice_nstrikes)
    if not Fsca2Hypsometry(
            drainageID=drainageID,
            year=year,
            modelEnv=modelEnv,
            fsca_type=fsca_type,
            fsca_area='SOL',
            start_doy=start_doy,
            stop_doy=stop_doy,
            contour_m=contour_m,
            modice_nstrikes=modice_nstrikes,
            outfile=sol_hyps_filename,
            decimal_places=decimal_places,
            verbose=verbose):
        print("%s : Error making Hypsometry %s for %s" %
              (__name__,
               sol_hyps_filename,
               drainageID),
              file=sys.stderr)
        raise RuntimeError
    if verbose:
        print("> %s : wrote %s" %
              (__name__, sol_hyps_filename),
              file=sys.stderr)

    # Do SOI/EGI for the requested thresholds
    for threshold in thresholds:
        soi_hyps_filename = modelEnv.hypsometry_filename(
            type='snow_on_ice_by_elevation',
            drainageID=drainageID,
            year=year,
            contour_m=contour_m,
            modice_nstrikes=modice_nstrikes,
            ablation_method=ablation_method,
            threshold=threshold)
        egi_hyps_filename = modelEnv.hypsometry_filename(
            type='exposed_glacier_ice_by_elevation',
            drainageID=drainageID,
            year=year,
            contour_m=contour_m,
            modice_nstrikes=modice_nstrikes,
            ablation_method=ablation_method,
            threshold=threshold)
        use_daily_threshold_file = threshold == 'fromFile'
        if not Fsca2Hypsometry(
                drainageID=drainageID,
                year=year,
                modelEnv=modelEnv,
                fsca_type=fsca_type,
                fsca_area='SOI/EGI',
                ablation_method=ablation_method,
                threshold=threshold,
                use_daily_threshold_file=use_daily_threshold_file,
                start_doy=start_doy,
                stop_doy=stop_doy,
                contour_m=contour_m,
                modice_nstrikes=modice_nstrikes,
                soi_outfile=soi_hyps_filename,
                egi_outfile=egi_hyps_filename,
                decimal_places=decimal_places,
                verbose=verbose):
            print("%s : Error making Hypsometry %s for %s/%s" %
                  (__name__,
                   soi_hyps_filename,
                   egi_hyps_filename,
                   drainageID),
                  file=sys.stderr)
            raise RuntimeError
        if verbose:
            print("> %s : wrote %s" %
                  (__name__, soi_hyps_filename),
                  file=sys.stderr)
            print("> %s : wrote %s" %
                  (__name__, egi_hyps_filename),
                  file=sys.stderr)

    return(True)
