#!/usr/bin/env python
"""Library for running simple temperature index model.

This module provides methods for running a simple temperature
index model on CHARIS Hypsometry data.

"""

from __future__ import print_function

import calendar   # noqa
import datetime as dt   # noqa
import math   # noqa
import matplotlib.pyplot as plt  # noqa
import numpy as np   # noqa
import pandas as pd   # noqa
import random  # noqa
import sys  # noqa

from hypsometry import Hypsometry   # noqa
from timeSeries import TimeSeries   # noqa


def TempIndexMelt(area_hyps,
                  temperature_hyps,
                  min_ddf=2.0,
                  max_ddf=7.0,
                  custom_ddf_hyps=None,
                  verbose=False):
    """Run the CHARIS temperature index model on an area Hypsometry.

    Melt volume is calculated for those areas where temperatures
    are greater than 0. deg-C.

    Default behavior is to seasonally-adjust degree-day-factors
    (DDFs) with a sine wave, using input min and max ddf values.
    Alternative is for user to specify DDFs by date and elevation
    in a custom DDF Hypsometry file.

    Raises RuntimeError if input temperatures represent data from
    more than one year.

    Args:
      area_hyps: Hypsometry, surface area to melt

      temperature_hyps: Hypsometry, temperatures at corresponding
        elevations for area_hyps

      min_ddf: float, minimum DDF (DDF at winter solstice),
        mm per degC per day, ignored if custom_ddf_hyps is not False

      max_ddf: float, maximum DDF (DDF at summer solstice),
        mm per degC per day, ignored if custom_ddf_hyps is not False

      custom_ddf_hyps: Hypsometry, DDFs by date and elevation
        (assumes units are mm per degC per day)

      verbose: boolean to turn on verbose output to stderr.

    Returns:
      Hypsometry with melt volume by elevation, cubic km.
      If input area_hyps is empty, data part of returned melt hypsometry will
      be empty.

    """
    if verbose:
        print("%s : begin." % (__name__), file=sys.stderr)

    km_per_mm = 1E-6

    # Check for empty area data, if area is empty, just return empty
    # melt data (but populate the comments as they would look otherwise)
    if area_hyps.data.empty:

        melt_km3_df = pd.DataFrame()

    else:

        # Make local copies to work with
        # replace values with missing temperatures
        # (which are hyphen-filled strings) with NaNs
        area_km2_df = area_hyps.data.copy()
        t_C_df = temperature_hyps.data.copy().replace(r'-+', np.nan, regex=True)

        too_cold = t_C_df <= 0.0
        area_km2_df[too_cold] = 0.0

        if not custom_ddf_hyps:
            ddf_mm_pday_pdegC = _seasonal_ddfs(area_km2_df.index,
                                               min_ddf=min_ddf,
                                               max_ddf=max_ddf)
            fill_value = None
        else:
            ddf_mm_pday_pdegC = custom_ddf_hyps.data.copy()

            # Replace the year in the DDF dataFrame with
            # the year of the first element from the temperatures.
            # Verify that there's only one year in the temperatures!
            # (If there's more than one year in the temperatures, this
            # logic will need to be modified)
            if 1 != len(np.unique(t_C_df.index.year)):
                print(" %s : Error running model "
                      "temperatures contain more than one year." %
                      (__name__),
                      file=sys.stderr)
                raise RuntimeError

            # When the year of the temperature data is a non-leap-year,
            # drop the custom DDF line for Feb 29 before doing the
            # multiplication
            temperature_year = t_C_df.index[0].year
            if not calendar.isleap(temperature_year):
                ddf_mm_pday_pdegC.drop([pd.to_datetime("2000-02-29")],
                                       inplace=True)
            ddf_mm_pday_pdegC.index = ddf_mm_pday_pdegC.index.map(
                lambda t: t.replace(year=temperature_year))
            fill_value = 0.0

        melt_km3_df = area_km2_df.multiply(
            t_C_df, fill_value=0.0).multiply(
                ddf_mm_pday_pdegC,
                axis='index',
                fill_value=fill_value) * km_per_mm

    # Return hyps object, save DDFs used in the comments
    if custom_ddf_hyps:
        melt_km3_hyps = Hypsometry(
            data=melt_km3_df,
            comments=[
                "Hypsometry created : " + str(dt.datetime.now()),
                "Melt in km^3",
                "Custom DDF file has been used"])
    else:
        melt_km3_hyps = Hypsometry(
            data=melt_km3_df,
            comments=[
                "Hypsometry created : " + str(dt.datetime.now()),
                "Melt in km^3",
                "min_DDF_mm_pday_pdegC : %f" % min_ddf,
                "max_DDF_mm_pday_pdegC : %f" % max_ddf])

    return melt_km3_hyps


def TriSurfTempIndexMelt(SOLFile,
                         SOIFile,
                         EGIFile,
                         temperatureFile,
                         min_snow_ddf=2.0,
                         max_snow_ddf=7.0,
                         min_ice_ddf=2.0,
                         max_ice_ddf=9.0,
                         customSnowDDFFile=False,
                         customIceDDFFile=False,
                         verbose=False):
    """Run the CHARIS temperature index model on 3-surface partition
    Hypsometry files.

    Melt volume is calculated for those areas where temperatures
    are greater than 0. deg-C for each of the 3 surface area
    Hypsometry files.

    Default behavior is to seasonally-adjust degree-day-factors
    (DDFs) with a sine wave, using input min and max ddf values.
    Alternative is for user to specify DDFs by date and elevation
    in a custom DDF Hypsometry file. Custom DDF file can be
    'minimal' in the sense that it only needs to include dates
    and/or elevations where the DDF changes.  If the first date
    in the custom DDF file is later than Jan 1, all DDFs from Jan
    1 to the first date in the file will be treated as unknown
    and returned melt for these dates will be NaNs.

    Args:
      SOLFile: string, filename of Snow-on-Land Hypsometry file

      SOIFile: string, filename of Snow-on-Ice Hypsometry file

      EGIFile: string, filename of Exposed-Glacier-Ice Hypsometry file

      temperatureFile: string, filename of temperature Hypsometry file

      min_snow_ddf: float, minimum snow DDF (DDF at winter solstice),
        mm per degC per day, used for SOL and SOI areas;
        this value is ignored if customSnowDDFFile is not False

      max_snow_ddf: float, maximum snow DDF (DDF at summer solstice),
        mm per degC per day, used for SOL and SOI areas;
        this value is ignored if customSnowDDFFile is not False

      min_ice_ddf: float, minimum ice DDF (DDF at winter solstice),
        mm per degC per day, used for EGI areas;
        this value is ignored if customIceDDFFile is not False

      max_ice_ddf: float, maximum ice DDF (DDF at summer solstice),
        mm per degC per day, used for EGI areas;
        this value is ignored if customIceDDFFile is not False

      customSnowDDFFile: string, filename for Hypsometry file with
        snow DDFs by date and elevation (assumes units are mm per degC
        per day)

      customIceDDFFile: string, filename for Hypsometry file with
        ice DDFs by date and elevation (assumes units are mm per degC
        per day)

      verbose: boolean to turn on verbose output to stderr.

    Returns:
      List with 3 Hypsometries, with melt volume by elevation, cubic km,
      respectively for SOL, SOI and EGI surfaces.

    """
    if verbose:
        print("%s : begin." % (__name__), file=sys.stderr)

    temperature_hyps = Hypsometry(filename=temperatureFile)
    area_hyps = Hypsometry(filename=SOLFile)

    if (customSnowDDFFile and not customIceDDFFile) or (
            customIceDDFFile and not customSnowDDFFile):
        print(" %s : Input error, both or neither "
              " snow/ice custom_DDFs must be input "
              "(%s, %s, %s, %s)." %
              (__name__, temperatureFile,
               SOLFile, SOIFile, EGIFile),
              file=sys.stderr)
        raise IOError

    elif customSnowDDFFile and customIceDDFFile:

        temperature_contour_m = temperature_hyps.get_contour()

        # Unfurl the custom DDFs using the contour size of
        # the temperature data
        custom_snow_ddf_hyps = _custom_ddfs(
            filename=customSnowDDFFile,
            contour_m=temperature_contour_m)
        custom_ice_ddf_hyps = _custom_ddfs(
            filename=customIceDDFFile,
            contour_m=temperature_contour_m)

    else:
        custom_snow_ddf_hyps = False
        custom_ice_ddf_hyps = False

    SOL_melt_hyps = TempIndexMelt(area_hyps=area_hyps,
                                  temperature_hyps=temperature_hyps,
                                  min_ddf=min_snow_ddf,
                                  max_ddf=max_snow_ddf,
                                  custom_ddf_hyps=custom_snow_ddf_hyps,
                                  verbose=verbose)
    SOL_melt_hyps.comments = SOL_melt_hyps.comments + [
        "Snow on land melt for:",
        "SOLFile         : %s" % SOLFile,
        "TemperatureFile : %s" % temperatureFile]

    area_hyps = Hypsometry(filename=SOIFile)
    SOI_melt_hyps = TempIndexMelt(area_hyps=area_hyps,
                                  temperature_hyps=temperature_hyps,
                                  min_ddf=min_snow_ddf,
                                  max_ddf=max_snow_ddf,
                                  custom_ddf_hyps=custom_snow_ddf_hyps,
                                  verbose=verbose)
    SOI_melt_hyps.comments = SOI_melt_hyps.comments + [
        "Snow on ice melt for:",
        "SOIFile         : %s" % SOIFile,
        "TemperatureFile : %s" % temperatureFile]

    area_hyps = Hypsometry(filename=EGIFile)
    EGI_melt_hyps = TempIndexMelt(area_hyps=area_hyps,
                                  temperature_hyps=temperature_hyps,
                                  min_ddf=min_ice_ddf,
                                  max_ddf=max_ice_ddf,
                                  custom_ddf_hyps=custom_ice_ddf_hyps,
                                  verbose=verbose)
    EGI_melt_hyps.comments = EGI_melt_hyps.comments + [
        "Exposed glacier ice melt for:",
        "EGIFile         : %s" % EGIFile,
        "TemperatureFile : %s" % temperatureFile]

    # Add provenance metadata when custom DDFs were used
    if customSnowDDFFile and customIceDDFFile:
        SOL_melt_hyps.comments = SOL_melt_hyps.comments + [
            "Custom DDF File : %s" % customSnowDDFFile]
        SOI_melt_hyps.comments = SOI_melt_hyps.comments + [
            "Custom DDF File : %s" % customSnowDDFFile]
        EGI_melt_hyps.comments = EGI_melt_hyps.comments + [
            "Custom DDF File : %s" % customIceDDFFile]

    if verbose:
        print(
            "%s : SOL melt volume   = %.2f km^3" % (
                __name__, SOL_melt_hyps.data.values.sum()),
            file=sys.stderr)
        print(
            "%s : SOI melt volume   = %.2f km^3" % (
                __name__, SOI_melt_hyps.data.values.sum()),
            file=sys.stderr)
        print(
            "%s : EGI melt volume   = %.2f km^3" % (
                __name__, EGI_melt_hyps.data.values.sum()),
            file=sys.stderr)
        print(
            "%s : Total melt volume = %.2f km^3" % (
                __name__, (
                    SOL_melt_hyps.data.values.sum() +
                    SOI_melt_hyps.data.values.sum() +
                    EGI_melt_hyps.data.values.sum())),
            file=sys.stderr)

    return [SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps]


def CalibrateTriSurfTempIndexMelt(SOLFile,
                                  SOIFile,
                                  EGIFile,
                                  temperatureFile,
                                  rainfallFile,
                                  runoffFile,
                                  rainfall_col='rainfall',
                                  runoff_col='runoff',
                                  min_snow_ddf=2.0,
                                  max_snow_ddf=7.0,
                                  min_ice_ddf=2.0,
                                  max_ice_ddf=9.0,
                                  verbose=False):
    """Run the CHARIS temperature index model on 3-surface partition Hypsometry
    files.

    Melt volume is calculated for those areas where temperatures
    are greater than 0. deg-C for each of the 3 surface area
    Hypsometry files.  Degree-day-factors are seasonally-adjusted
    using input min and max ddf values.

    Args:
      SOLFile: string, filename of Snow-on-Land Hypsometry file

      SOIFile: string, filename of Snow-on-Ice Hypsometry file

      EGIFile: string, filename of Exposed-Glacier-Ice Hypsometry file

      temperatureFile: string, filename of temperature Hypsometry file

      rainfallFile: string, filename of rainfall TimeSeries file
        Rainfall data to add to modeled melt before comparing to
        runoff data.  Assumes this file is readable as a TimeSeries
        object.
        Any rainfall data outside the period spanned by the
        SOL/SOI/EGI/temperature Hypsometry files is ignored.

      runoffFile: string, filename of runoff TimeSeries file
        Runoff data to compare to modeled melt+rainfall.
        Assumes this file is readable as a TimeSeries object.
        Any runoff data outside the period spanned by the
        SOL/SOI/EGI/temperature Hypsometry files is ignored.

      rainfall_col: string, column in rainfallFile to use

      runoff_col: string, column in runoffFile to use

      min_snow_ddf: float, minimum snow DDF (DDF at winter solstice),
        mm per degC per day, used for SOL and SOI areas

      max_snow_ddf: float, maximum snow DDF (DDF at summer solstice),
        mm per degC per day, used for SOL and SOI areas

      min_ice_ddf: float, minimum ice DDF (DDF at winter solstice),
        mm per degC per day, used for EGI areas

      max_ice_ddf: float, maximum ice DDF (DDF at summer solstice),
        mm per degC per day, used for EGI areas

      verbose: boolean to turn on verbose output to stderr.

    Returns:
      Tuple with rmse and volumetric difference percent calculated
      from modeled melt+rainfall vs. runoff.

    """
    if verbose:
        print("%s : begin." % (__name__), file=sys.stderr)

    # Run the model with requested parameters
    try:
        [SOL_melt_hyps,
         SOI_melt_hyps,
         EGI_melt_hyps] = TriSurfTempIndexMelt(
             SOLFile=SOLFile,
             SOIFile=SOIFile,
             EGIFile=EGIFile,
             temperatureFile=temperatureFile,
             min_snow_ddf=min_snow_ddf,
             max_snow_ddf=max_snow_ddf,
             min_ice_ddf=min_ice_ddf,
             max_ice_ddf=max_ice_ddf,
             verbose=verbose)
    except:
        print(" %s : Error running model "
              "(%s, %s, %s, %s, %f, %f, %f, %f)." %
              (__name__, temperatureFile,
               SOLFile, SOIFile, EGIFile,
               min_snow_ddf, max_snow_ddf,
               min_ice_ddf, max_ice_ddf),
              file=sys.stderr)
        raise

    # Aggregate melt by month
    # Coerce to a data frame, so multiple columns can be concatenated
    melt_by_doy = (
        SOL_melt_hyps.data_by_doy() +
        SOI_melt_hyps.data_by_doy() +
        EGI_melt_hyps.data_by_doy())
    melt_by_month = melt_by_doy.groupby([pd.TimeGrouper('M')]).sum()
    df = melt_by_month.to_frame(name="melt")

    # Use rainfall and runoff to calculate calibration statistics
    # Even though these are monthly data, do the groupby/sum so that
    # the date index is properly set up for the last day of each month
    try:
        rainfallSeries = TimeSeries(filename=rainfallFile)
        rainfall_by_month = rainfallSeries.data[rainfall_col].groupby(
            [pd.TimeGrouper('M')]).sum()
    except:
        print(" %s : Error reading col=%s from %s" %
              (__name__, rainfall_col, rainfallFile),
              file=sys.stderr)
        raise
    df["rainfall"] = rainfall_by_month
    df["melt+rainfall"] = df["melt"] + df["rainfall"]

    try:
        runoffSeries = TimeSeries(filename=runoffFile)
        runoff_by_month = runoffSeries.data[runoff_col].groupby(
            [pd.TimeGrouper('M')]).sum()
    except:
        print(" %s : Error reading col=%s from %s" %
              (__name__, runoff_col, runoffFile),
              file=sys.stderr)
        raise
    df["runoff"] = runoff_by_month

    result = {}
    result['monthly_rmse_km3'] = _rmse(df)
    result['annual_voldiff_pcent'] = _volumetric_difference_pcent(df)
    result['rainfall_km3'] = df["rainfall"].sum()
    result['SOL_melt_km3'] = SOL_melt_hyps.data_by_doy().sum()
    result['SOI_melt_km3'] = SOI_melt_hyps.data_by_doy().sum()
    result['EGI_melt_km3'] = EGI_melt_hyps.data_by_doy().sum()
    result['runoff_km3'] = df["runoff"].sum()

    if verbose:
        print("%s : Calibration Data:" % (__name__), file=sys.stderr)
        print(df, file=sys.stderr)
        print(df.describe(), file=sys.stderr)

    return result


def CalibrationStats(
        myEnv, drainageid, years, nstrikes,
        min_snow_ddf, max_snow_ddf, min_ice_ddf, max_ice_ddf,
        rainfall_col, runoff_col,
        ice_source='modice_min05yr',
        snow_source='modscag_gf',
        temperature_source='temperature',
        ablation_method='albedo_mod10a1',
        threshold=0.40,
        use_daily_threshold_file=False,
        rainfall_source='rainfall',
        runoff_source='runoff'):
    """
    Uses the specified inputs to run the ti-melt model for a set
    of years, and calculates calibration statistics (monthly
    RMSE, annual voldiff %).

    Args:
      myEnv: initialized model configuration object

      drainageid: drainage name string, used for wildcard %DRAINAGEID%
        e.g. "IN_Hunza_at_Danyour" or "IN_Hunza_GDBD"

      years: array of years to process

      nstrikes: modice files differ by number of strikes, 1, 2 or 3.

      min_snow_ddf: float, minimum snow DDF (DDF at winter solstice),
        mm per degC per day

      max_snow_ddf: float, maximum snow DDF (DDF at summer solstice),
        mm per degC per day

      min_ice_ddf: float, minimum ice DDF (DDF at winter solstice),
        mm per degC per day

      max_ice_ddf: float, maximum ice DDF (DDF at summer solstice),
        mm per degC per day

      rainfall_col: string, rainfall file column name to use for stats

      runoff_col: string, runoff file column name to use for stats

      ice_source: string, ice source to get ice data id from, default
        'modice_min05yr'

      snow_source: string, snow source to get snow data id from, default
        'modscag_gf'

      temperature_source: string, temperature source to get temperature
        id from, default 'temperature'

      ablation_method: string, ablation method to use to lookup model input
        files, default 'grsize_scag'

      threshold: float, threshold (grsize or albedo) to use to lookup model
        input files, default=0.40

      use_daily_threshold_file: boolean, use file for daily ablation
        thresholds, default False

      rainfall_source: string rainfall source to lookup filename, default
        'rainfall',

      runoff_source: string, runoff source to lookup filename, default
        'runoff'

    Returns: DataFrame with calibration outputs, one row for each year

    """
    this_func_name = sys._getframe().f_code.co_name
    if use_daily_threshold_file:
        threshold = 'fromFile'

    # Fetch the comparison rainfall and runoff files for comparison
    rainfallFile = myEnv.calibration_filename(
        type=rainfall_source,
        drainageID=drainageid)
    runoffFile = myEnv.calibration_filename(
        type=runoff_source,
        drainageID=drainageid)

    result = {}
    for year in years:
        try:
            inputs = myEnv.model_inputs(drainageID=drainageid,
                                        year=year,
                                        modice_nstrikes=nstrikes,
                                        ablation_method=ablation_method,
                                        threshold=threshold)
        except:
            print("%s : Error in model_inputs for %s, %d, %d, %s, %s" %
                  (this_func_name, drainageid, year, nstrikes, ablation_method,
                   threshold),
                  file=sys.stderr)
            raise

        try:
            result[year] = CalibrateTriSurfTempIndexMelt(
                SOLFile=inputs['snow_on_land_by_elevation_filename'],
                SOIFile=inputs['snow_on_ice_by_elevation_filename'],
                EGIFile=inputs['exposed_glacier_ice_by_elevation_filename'],
                temperatureFile=inputs['temperature_by_elevation_filename'],
                rainfallFile=rainfallFile,
                rainfall_col=rainfall_col,
                runoffFile=runoffFile,
                runoff_col=runoff_col,
                min_snow_ddf=min_snow_ddf,
                max_snow_ddf=max_snow_ddf,
                min_ice_ddf=min_ice_ddf,
                max_ice_ddf=max_ice_ddf)
            result[year]["min_snow_ddf"] = min_snow_ddf
            result[year]["max_snow_ddf"] = max_snow_ddf
            result[year]["min_ice_ddf"] = min_ice_ddf
            result[year]["max_ice_ddf"] = max_ice_ddf
        except Exception as e:
            print(this_func_name + ": Error({0})".format(e), file=sys.stderr)
            print("%s : Error in calibration for %s, %d, %s, %s" %
                  (this_func_name, drainageid, year, ablation_method, threshold),
                  file=sys.stderr)
            raise

    # Coerce dict to DataFrame
    return _calibration_dict_to_dataframe(result)


def SaveCalibrationStats(
        myEnv, drainageid, nstrikes,
        result,
        ice_source='modice_min05yr',
        snow_source='modscag_gf',
        temperature_source='temperature',
        ablation_method='albedo_mod10a1',
        threshold=0.40,
        use_daily_threshold_file=False,
        rainfall_source='rainfall',
        runoff_source='runoff',
        file=sys.stdout,
        header=False):
    """
    Outputs formatted calibration statistics to the requested file,
    with an optional column header line

    Args: (see args for calibration_stats)

      result: DataFrame returned from CalibrationStats

      file: open file handle

      header: Boolean, write a line with header columns

    Returns:
      No return, calibration stats are written to requested file handle,
      one line for each year in result
    """
    if header:
        print("%s %s %s %s %s %s %s %s %s %s %s "
              "%s %s %s %s %s %s %s %s %s %s %s %s" %
              ("DRAINAGEID", "YYYY", "ICE", "NSTRIKES", "SNOW", "ABLATION",
               "ABLATION_METHOD", "THRESHOLD", "TEMPERATURE",
               "RAINFALL", "RUNOFF",
               "model", "min_snow_ddf", "max_snow_ddf", "min_ice_ddf", "max_ice_ddf",
               "rainfall_km3", "SOI_melt_km3", "SOL_melt_km3", "EGI_melt_km3",
               "runoff_km3",
               "monthly_rmse_km3", "annual_voldiff_pcent"),
              file=file)
        file.flush()

    # FIXME:  hardcoding the data type entries for now, but these should
    # be passed back from model_inputs to make sure they really are what was
    # used.  the problem is that the snow type and albedo or grsize type
    # is from the raster-to-hyps conversion step and is only available
    # in the hypsometry file metadata and/or filename.  So I need to come up with
    # a better way to pass this history along
    if use_daily_threshold_file:
        threshold = 'fromFile'
    for i in result.index:
        print("%s %d %s %d %s %s %s %s %s %s %s %s "
              "%12.4f %12.4f %12.4f %12.4f "
              "%12.4f "
              "%12.4f %12.4f %12.4f "
              "%12.4f "
              "%12.4f %12.4f" %
              (drainageid,
               result.at[i, 'year'],
               myEnv.tileConfig['input']['fixed'][ice_source]['id'],
               nstrikes,
               myEnv.tileConfig['input']['forcing'][snow_source]['id'],
               myEnv.tileConfig['input']['forcing'][ablation_method]['id'],
               ablation_method, threshold,
               myEnv.tileConfig['input']['forcing'][temperature_source]['id'],
               myEnv.tileConfig['calibration'][rainfall_source]['id'],
               myEnv.tileConfig['calibration'][runoff_source]['id'],
               result.at[i, 'model'],
               result.at[i, 'min_snow_ddf'],
               result.at[i, 'max_snow_ddf'],
               result.at[i, 'min_ice_ddf'],
               result.at[i, 'max_ice_ddf'],
               result.at[i, 'rainfall_km3'],
               result.at[i, 'SOI_melt_km3'],
               result.at[i, 'SOL_melt_km3'],
               result.at[i, 'EGI_melt_km3'],
               result.at[i, 'runoff_km3'],
               result.at[i, 'monthly_rmse_km3'],
               result.at[i, 'annual_voldiff_pcent']),
              file=file)
        file.flush()


def CalibrationCost(df, verbose=False):
    """
    Given a calibration stats data frame, calculates cost of each
    model.
    """
    # Filter for just the columns we will need
    df = df.loc[:, [
        'model', 'min_snow_ddf', 'max_snow_ddf', 'min_ice_ddf', 'max_ice_ddf',
        'runoff_km3', 'monthly_rmse_km3', 'annual_voldiff_pcent']]

    # Remove duplicates that may have been produced on multiple calibration runs
    nrows_before = df.shape[0]
    dups = df.duplicated(keep='first')
    df = df[~dups]
    if verbose > 0:
        print("\nIgnoring %d duplicate rows" % (
            nrows_before - df.shape[0]), file=sys.stderr)

    # In order to equally weight monthly RMSE (km^3) with
    # volDiff (which is a percent of measured runoff),
    # make a new column of Monthly_rmse_pcent, which is
    # Monthly_rmse_km3 / runoff_km3 * 100:
    # Multiply the RMSE by 12 to normalize to an annual number
    # so the magnitudes are comparable to the annual voldiff
    # This will always be positive, because rmse is positive
    df["monthly_rmse_pcent"] = 12 * 100. * (
        df["monthly_rmse_km3"] / df["runoff_km3"])

    # Calculate average volDiff and RMSE by modelid (over multiple years)
    # Collect the averaged stats into a new DataFrame
    mean_vol_diff_pcent = df.groupby(['model']).mean()['annual_voldiff_pcent']
    mean_rmse_km3 = df.groupby(['model']).mean()['monthly_rmse_km3']
    mean_rmse_pcent = df.groupby(['model']).mean()['monthly_rmse_pcent']
    stats_df = mean_rmse_km3.to_frame()
    stats_df['annual_voldiff_pcent'] = mean_vol_diff_pcent
    stats_df['abs_voldiff_pcent'] = np.abs(stats_df['annual_voldiff_pcent'])
    stats_df['monthly_rmse_pcent'] = mean_rmse_pcent

    # Cost is sum of | voldiff_pcent | and (annualized) rmse pcent
    stats_df['z'] = (
        stats_df['abs_voldiff_pcent'] + stats_df['monthly_rmse_pcent'])

    if verbose:
        print("\nStatistics ranges in this file:", file=sys.stderr)
        print(stats_df.describe().loc[['max', 'min'],
                                      ['monthly_rmse_km3',
                                       'monthly_rmse_pcent',
                                       'annual_voldiff_pcent',
                                       'z']],
              file=sys.stderr)

    stats_df.sort_values(by=['z'], ascending=True, inplace=True)

    # Summarize results
    if verbose:
        print("\nDDF ranges included in this file:", file=sys.stderr)
        print(df.describe().loc[['max', 'min'],
                                ['min_snow_ddf',
                                 'max_snow_ddf',
                                 'min_ice_ddf',
                                 'max_ice_ddf']],
              file=sys.stderr)

    # Convert index to a set to get the unique values
    # and dump the stats for the model that minimizes z
    uniq_models = set(stats_df.index)
    if verbose > 0:
        print(file=sys.stderr)
        print(stats_df.iloc[0], file=sys.stderr)
        print("\nNumber of models considered: %d" % len(uniq_models),
              file=sys.stderr)

    # Extract the individual ddf values into separate columns in stats_df
    num_records = stats_df.index.shape
    stats_df["min_snow_ddf"] = np.zeros(num_records)
    stats_df["max_snow_ddf"] = np.zeros(num_records)
    stats_df["min_ice_ddf"] = np.zeros(num_records)
    stats_df["max_ice_ddf"] = np.zeros(num_records)
    for model in stats_df.index:
        parts = model.split('_')
        stats_df.loc[model, 'min_snow_ddf'] = parts[0]
        stats_df.loc[model, 'max_snow_ddf'] = parts[1]
        stats_df.loc[model, 'min_ice_ddf'] = parts[2]
        stats_df.loc[model, 'max_ice_ddf'] = parts[3]

    # Convert all ddf values from strings to floats
    cols = ['min_snow_ddf', 'max_snow_ddf', 'min_ice_ddf', 'max_ice_ddf']
    stats_df[cols] = stats_df[cols].apply(pd.to_numeric, errors='coerce')

    return df, stats_df


def RandomNewDDFs(min_snow_ddf=None,
                  max_snow_ddf=None,
                  min_ice_ddf=None,
                  max_ice_ddf=None,
                  neighborhood_mm=10.,
                  min_DDF_mm_pday_pdegC=0.,
                  max_DDF_mm_pday_pdegC=60.,
                  initialize=False):
    """
    Given a set of DDFs, chooses a new random set in the specified
    neighborhood_mm, subject to constraints:

    Each set should be higher in summer than winter:
    min_snow_ddf <= max_snow_ddf
    min_ice_ddf <= max_ice_ddf

    min snow DDF should be <= min ice DDF:
    min_snow_ddf <= min_ice_ddf

    At least one DDF should be > 0.

    DDFs units are mm per degC per day.

    """
    # If initializing, just pick a random starting point
    # subject to relative constraints
    # Make sure at least one max value is greater than
    # range minimum, don't allow any to be exactly range
    # maximum
    if initialize:
        min_snow_ddf = 0.
        done = False
        while not done:
            min_snow_ddf = _random_ddf(
                min_DDF_mm_pday_pdegC,
                min_DDF_mm_pday_pdegC + neighborhood_mm)
            orig_max_snow_ddf = min_snow_ddf
            orig_min_ice_ddf = min_snow_ddf
            orig_max_ice_ddf = min_snow_ddf
            max_snow_ddf = _random_ddf(
                np.max([min_DDF_mm_pday_pdegC,
                        orig_max_snow_ddf - neighborhood_mm,
                        min_snow_ddf]),
                np.min([orig_max_snow_ddf + neighborhood_mm,
                        max_DDF_mm_pday_pdegC]))
            min_ice_ddf = _random_ddf(
                np.max([min_DDF_mm_pday_pdegC,
                        orig_min_ice_ddf - neighborhood_mm,
                        min_snow_ddf]),
                np.min([orig_min_ice_ddf + neighborhood_mm,
                        max_DDF_mm_pday_pdegC]))
            max_ice_ddf = _random_ddf(
                np.max([min_DDF_mm_pday_pdegC,
                        orig_max_ice_ddf - neighborhood_mm,
                        min_ice_ddf]),
                np.min([orig_max_ice_ddf + neighborhood_mm,
                        max_DDF_mm_pday_pdegC]))
            if (min_DDF_mm_pday_pdegC < max_snow_ddf or
                min_DDF_mm_pday_pdegC < max_ice_ddf) and (
                    min_snow_ddf < max_DDF_mm_pday_pdegC and
                    max_snow_ddf < max_DDF_mm_pday_pdegC and
                    min_ice_ddf < max_DDF_mm_pday_pdegC and
                    max_ice_ddf < max_DDF_mm_pday_pdegC):
                done = True
    else:
        # Otherwise, choose random new points "nearby" original
        # points, subject to relative constraints
        orig_min_snow_ddf = min_snow_ddf
        orig_max_snow_ddf = max_snow_ddf
        orig_min_ice_ddf = min_ice_ddf
        orig_max_ice_ddf = max_ice_ddf
        done = False
        while not done:
            min_snow_ddf = _random_ddf(
                np.max([min_DDF_mm_pday_pdegC,
                        orig_min_snow_ddf - neighborhood_mm]),
                np.min([orig_min_snow_ddf + neighborhood_mm,
                        max_DDF_mm_pday_pdegC]))
            max_snow_ddf = _random_ddf(
                np.max([min_DDF_mm_pday_pdegC,
                        orig_max_snow_ddf - neighborhood_mm,
                        min_snow_ddf]),
                np.min([orig_max_snow_ddf + neighborhood_mm,
                        max_DDF_mm_pday_pdegC]))
            min_ice_ddf = _random_ddf(
                np.max([min_DDF_mm_pday_pdegC,
                        orig_min_ice_ddf - neighborhood_mm,
                        min_snow_ddf]),
                np.min([orig_min_ice_ddf + neighborhood_mm,
                        max_DDF_mm_pday_pdegC]))
            max_ice_ddf = _random_ddf(
                np.max([min_DDF_mm_pday_pdegC,
                        orig_max_ice_ddf - neighborhood_mm,
                        min_ice_ddf]),
                np.min([orig_max_ice_ddf + neighborhood_mm,
                        max_DDF_mm_pday_pdegC]))
            if (min_DDF_mm_pday_pdegC < max_snow_ddf or
                min_DDF_mm_pday_pdegC < max_ice_ddf) and (
                    min_snow_ddf < max_DDF_mm_pday_pdegC and
                    max_snow_ddf < max_DDF_mm_pday_pdegC and
                    min_ice_ddf < max_DDF_mm_pday_pdegC and
                    max_ice_ddf < max_DDF_mm_pday_pdegC):
                done = True

    return min_snow_ddf, max_snow_ddf, min_ice_ddf, max_ice_ddf


def PlotTriSurfInput(ax,
                     SOL_hyps,
                     SOI_hyps,
                     EGI_hyps,
                     temperature_hyps,
                     ice_hyps=None,
                     title=None,
                     SOL_color='seagreen',
                     SOI_color='royalblue',
                     EGI_color='darkorchid',
                     temperature_color=[0.8, 0.8, 0.8],
                     ice_color=[0.2, 0.2, 0.2],
                     hline_temperature_C=0.,
                     linewidth=3,
                     boxwidth_scale=0.75,
                     verbose=False):
    """Creates a time series plot of melt model inputs by day-of-year.

    Creates a time series of model inputs snow-on-land, snow-on-ice,
    exposed-glacier-ice, and ice area and average temperature by day-of-year.

    Args:
      ax: Axes object to add the plot to

      SOL_hyps: Hypsometry, snow-on-land area, km^2

      SOI_hyps: Hypsometry, snow-on-ice area, km^2

      EGI_hyps: Hypsometry, exposed-glacier-ice area, km^2

      temperature_hyps: Hypsometry, temperature, deg C

      ice_hyps: Hypsometry, ice area, km^2

      title: string, Axes title

      SOL_color: color for snow-on-land line

      SOI_color: color for snow-on-ice line

      EGI_color: color for exposed-glacier-ice line

      temperature_color: color for average temperature line

      ice_color: color for ice line

      hline_temperature_C: float, temperature value of horizontal line,
        degrees C

      line_width: line width

      verbose: boolean to turn on verbose output to stderr.

    Result:
      Returns Axes object

    """
    if verbose:
        print("%s : begin." % (__name__), file=sys.stderr)

    # Assume that SOL and temperatures are non-empty
    # Allow EGI and SOI to be empty (in basins with no glaciers)
    # and handle them
    sol_by_doy = SOL_hyps.data_by_doy()
    temperature_by_doy = temperature_hyps.data[
        temperature_hyps.data.columns].mean(axis=1)
    if ice_hyps:
        ice_by_doy = ice_hyps.data_by_doy()

    if not SOI_hyps.data.empty:
        soi_by_doy = SOI_hyps.data_by_doy()
        soi_by_doy.plot(ax=ax, label='Snow on ice',
                        color=SOI_color,
                        linewidth=linewidth)

    if not EGI_hyps.data.empty:
        egi_by_doy = EGI_hyps.data_by_doy()
        egi_by_doy.plot(ax=ax, label='Exposed glacier ice',
                        color=EGI_color,
                        linewidth=linewidth)

    sol_by_doy.plot(ax=ax, label='Snow on land',
                    color=SOL_color,
                    linewidth=linewidth)
    if ice_hyps:

        if not SOI_hyps.data.empty and not EGI_hyps.data.empty:
            (egi_by_doy + soi_by_doy).plot(ax=ax,
                                           label='SOI + EGI',
                                           color=ice_color,
                                           linewidth=linewidth)

        ax.axhline(ice_by_doy['NoDate'],
                   label='ice',
                   color=ice_color,
                   linestyle=':',
                   linewidth=linewidth)

    ax1 = ax.twinx()
    temperature_by_doy.plot(ax=ax1,
                            label='Temperature', color=temperature_color,
                            linewidth=linewidth)
    ax1.axhline(hline_temperature_C,
                color=temperature_color, linestyle=':',
                linewidth=linewidth)

    ax.set_ylabel('Area (' + r'$km^2$' + ')')
    ax1.set_ylabel('Temperature (' + r'$^{\circ}C$' + ')',
                   color=temperature_color)
    for ticklabel in ax1.get_yticklabels():
        ticklabel.set_color(temperature_color)

    if title:
        ax.set_title(title)

    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0, box.width * boxwidth_scale,
    # box.height])
    # ax1.set_position([box.x0, box.y0, box.width * boxwidth_scale,
    # box.height])

    h, l = ax.get_legend_handles_labels()
    h1, l1 = ax1.get_legend_handles_labels()
    ax.legend(h+h1, l+l1)

    return ax, ax1


def PlotTriSurfMelt(ax,
                    SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps,
                    title=None,
                    SOL_color='seagreen',
                    SOI_color='royalblue',
                    EGI_color='darkorchid',
                    sum_color=[0.8, 0.8, 0.8],
                    linewidth=3,
                    verbose=False):
    """Creates time series plot of melt model data by day-of-year.

    Creates time series plot with melt by day-of-year rom snow-on-land,
    snow-on-ice and exposed-glacier-ice surfaces.

    Args:
      ax: Axes to add lines to

      SOL_melt_hyps: Hypsometry, snow-on-land melt, cubic km

      SOI_melt_hyps: Hypsometry, snow-on-ice melt, cubic km

      EGI_melt_hyps: Hypsometry, exposed-glacier-ice melt, cubic km

      title: string, Axes title

      SOL_color: color for snow-on-land line

      SOI_color: color for snow-on-ice line

      EGI_color: color for exposed-glacier-ice line

      sum_color: color for sum of all melt types

      line_width: line width

      verbose: boolean to turn on verbose output to stderr.

    Result:
      Returns list of Axes objects

    """
    if verbose:
        print("%s : begin." % (__name__), file=sys.stderr)

    sol_by_doy = SOL_melt_hyps.data_by_doy()
    soi_by_doy = SOI_melt_hyps.data_by_doy()
    egi_by_doy = EGI_melt_hyps.data_by_doy()
    total_melt_by_doy = sol_by_doy + soi_by_doy + egi_by_doy

    ax.set_ylabel('Volume (' + r'$km^3$' + ')')
    total_melt_by_doy.plot(ax=ax, label='Total Melt',
                           color=sum_color, linewidth=linewidth)
    soi_by_doy.plot(ax=ax, label='Snow on ice', color=SOI_color,
                    linewidth=linewidth)
    egi_by_doy.plot(ax=ax, label='Exposed glacier ice', color=EGI_color,
                    linewidth=linewidth)
    sol_by_doy.plot(ax=ax, label='Snow on land', color=SOL_color,
                    linewidth=linewidth)

    if title:
        ax.set_title(title)

    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0, box.width * 0.75, box.height])

    # h, l = ax.get_legend_handles_labels()
    # ax.legend(h, l, bbox_to_anchor=(1.5, 1.05))
    ax.legend()

    return ax


def ImshowTriSurfMelt(axs,
                      SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps,
                      SOL_cmap='Greens',
                      SOI_cmap='Blues',
                      EGI_cmap='Purples',
                      verbose=False):
    """Creates triptych imshow displays of melt model data.

    Args:
      axs: Axes to add lines to

      SOL_melt_hyps: Hypsometry, snow-on-land melt, cubic km

      SOI_melt_hyps: Hypsometry, snow-on-ice melt, cubic km

      EGI_melt_hyps: Hypsometry, exposed-glacier-ice melt, cubic km

      SOL_color: colormap for snow-on-land line

      SOI_color: colormap for snow-on-ice line

      EGI_color: colormap for exposed-glacier-ice line

      verbose: boolean to turn on verbose output to stderr.

    Result:
      Returns list of Axes objects

    """
    if verbose:
        print("%s : begin." % (__name__), file=sys.stderr)

    axs[0] = SOI_melt_hyps.imshow(ax=axs[0], title='Snow-on-Ice Melt',
                                  cmap=SOI_cmap)
    axs[0].set_xticks([])
    axs[1] = EGI_melt_hyps.imshow(ax=axs[1], title='Exposed-Glacier-Ice Melt',
                                  cmap=EGI_cmap)

    axs[1].set_xticks([])
    axs[2] = SOL_melt_hyps.imshow(ax=axs[2], title='Snow-on-Land Melt',
                                  cmap=SOL_cmap)

    return axs


def _seasonal_ddfs(date_index, min_ddf=2.0, max_ddf=7.0):
    """Calculate seasonally-adjusted DDF by day of year.

    Calculate a seasonally-varying degree-day factor (DDF), according to
    Anderson, Eric. 2006. "Snow Accumulation and Ablation Model - SNOW-17"
    Uses a sine wave to modify a DDF between max at summer solstice and
    min at winter solstice.

    As currently implemented, works for Northern Hemisphere locations.
    Would need to be modified to run in Southern Hemisphere.

    Args:
      date_index : Pandas DataFrame index of dates (pandas DataTimeIndex)

      min_ddf: float, minimum DDF (DDF at winter solstice),
        mm per degC per day

      max_ddf: float, maximum DDF (DDF at summer solstice),
        mm per degC per day

    Returns:
      Pandas Series object with seasonally-adjusted DDF at each input date.

    """
    # Convert input dates to day-of-year
    # shift day-of-year so that sine function will set
    # midpoint between min and max ddfs at the spring equinox,
    # Mar 21, which is:
    # leap years: Mar 21 doy is 81
    # non-leap years: Mar 21 doy is 80
    doys = date_index.dayofyear
    leaptest = [calendar.isleap(year) for year in date_index.year]
    days_per_year = [366. if x else 365. for x in leaptest]
    equinox_doy = [81 if x else 80 for x in leaptest]

    # Calculate the sine wave-adjusted DDF that aligns
    # y=0 to the spring equinox
    theta = ((doys - equinox_doy) * 2.0 * math.pi) / days_per_year
    ddfs = (0.5 + (0.5 * np.sin(theta))) * (max_ddf - min_ddf) + min_ddf

    # Return a new Series with these ddfs
    return pd.Series(data=ddfs, index=date_index,
                     name="DDF_mm_pday_pdegC")


def _custom_ddfs(filename, contour_m=100.):
    """Reads a custom-defined DDF data frame from external file.

    Reads a set of custom DDFs from the specified file, and unfurls
    it to populate any missing dates and/or elevations.

    Args:
      filename : Filename of custom DDF file with DDF hypsometry.

    Returns:
      Hypsometry object with custom-defined DDFs at each input date
    and elevation.

    """
    ddf_hyps = Hypsometry(filename=filename)
    ddf_hyps.unfurl(contour_m=contour_m)

    return ddf_hyps


def _rmse(df, computed_col='melt+rainfall', observed_col='runoff'):
    """Calculate RMSE for computed vs. observed values.

    Root mean squared error is a goodness-of-fit metric for model
    performance on a periodic basis.

    RMSE measures how large the periodic (daily or monthly) differences
    are, regardless of sign.

                    ( sum(i=1,n) [ ( Qc - Qo )^2 ] )
       RMSE =  sqrt ( -----------------------------)
                    (              n               )

    Qc = computed volume for each time step
    Qo = observed volume for each time step

    Assumes input DataFrame is limited to the time span being considered

    RMSE values are positive, increasing from zero (perfect match)
    Between 2 choices of a model, the one with smaller RMSE is a "better"
    match.

    Args:
      df : Pandas DataFrame with data by date

      computed_col : column name with df melt+precip

      observed_col : column name with df runoff

    Returns:
      Calculated RMSE.

    """
    return math.sqrt(
        ((df[computed_col] - df[observed_col]) ** 2.).mean())


def _volumetric_difference_pcent(
        df, computed_col='melt+rainfall', observed_col='runoff'):
    """Calculate Volumetric difference percentage for computed
    vs. observed total volumes

    Volumetric difference (%) is another goodness-of-fit metric for
    accumulated seasonal streamflow simulation accuracy.

    Per Martinec and Rango 1989, JHydrology:
              (Vo - Vc)
         Dv = --------- x 100
                  Vo

    Vc = computed (modeled) volume for the time period
    Vo = observed volume for the time period
    Dv = "volumetric difference"

    N.B. Dey at al 1989 reverses the sign in the numerator

    Returned Dv is a percentage.

    Args:
      df : Pandas DataFrame with data by date

      computed_col : column name with df melt+precip

      observed_col : column name with df runoff

    Returns:
      Calculated volumetric difference (%)

    """
    computed_volume = float(df[computed_col].sum())
    observed_volume = float(df[observed_col].sum())
    return (observed_volume - computed_volume) / observed_volume * 100.


def _calibration_dict_to_dataframe(dict):
    """Coerce the calibration dict (by year) of dicts (calibration
    stats) into a reasonable DataFrame, indexed by modelid.

    Args:
      dict : calibration dict (by year) of dicts (calibration stats)

    Returns:
      DataFrame by modelid

    """
    # Convert the dict of dicts to df
    df = pd.DataFrame.from_dict(dict, orient='index')

    # Move the year from index to a column
    df.reset_index(level=0, inplace=True)
    df.rename(columns={'index': 'year'}, inplace=True)

    # Make a new column with modelid (string of DDFs)
    df.loc[:, "model"] = (
        df["min_snow_ddf"].map(str) + "_" +
        df["max_snow_ddf"].map(str) + "_" +
        df["min_ice_ddf"].map(str) + "_" +
        df["max_ice_ddf"].map(str))

    return df


def _random_ddf(min_ddf, max_ddf):
    """
    Calculate a random DDF in the specified range
    Value will be rounded to 2 decimal places.
    """
    assert(min_ddf <= max_ddf)
    return round(
        random.uniform(min_ddf, max_ddf), 2)
