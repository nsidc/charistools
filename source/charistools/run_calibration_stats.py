#! /usr/bin/env python
from __future__ import print_function
import click
import sys

from modelEnv import ModelEnv
from meltModels import CalibrationStats
from meltModels import SaveCalibrationStats


@click.command()
@click.argument('modelenvfile', type=click.Path(exists=True))
@click.option('-d', '--drainageid', default='IN_Hunza_at_DainyorBridge',
              help="DrainageID, e.g. 'IN_Hunza_at_DainyorBridge'")
@click.option('-y', '--year', default=2001,
              help="4-digit year to run")
@click.option('-n', '--nstrikes', default=1,
              help="MODICE nstrikes, 1-3")
@click.option('-m', '--ablation_method', default='albedo_mod10a1',
              help="ablation_method, "
              "e.g. 'albedo_mod10a1' or 'grsize_scag'")
@click.option('-t', '--threshold', default=0.40,
              help="ablation_method threshold, "
              "e.g. 0.0 - 1.0 for albedo, 0 - 1100 for grsize")
@click.option('-f', '--use_daily_threshold_file', default=False,
              help="use forcings from daily thresholds from threshold file")
@click.option('--min_snow_ddf', default=2.0,
              help='min snow ddf (mm / degC / day)')
@click.option('--max_snow_ddf', default=7.0,
              help='max snow ddf (mm / degC / day)')
@click.option('--min_ice_ddf', default=2.0,
              help='min ice ddf (mm / degC / day)')
@click.option('--max_ice_ddf', default=9.0,
              help='max ice ddf (mm / degC / day)')
@click.option('--rainfall_col', default='rainfall',
              help='rainfall column name')
@click.option('--runoff_col', default='runoff',
              help='runoff column name')
@click.option('-h', '--header', default=False,
              help="output column headers")
@click.option('-v', '--verbose', count=True,
              help="increase verbosity (may be repeated)")
def run_calibration_stats(modelenvfile, year, drainageid,
                          nstrikes,
                          ablation_method, threshold,
                          use_daily_threshold_file,
                          rainfall_col, runoff_col,
                          min_snow_ddf, max_snow_ddf,
                          min_ice_ddf, max_ice_ddf,
                          header, verbose):

    if (verbose > 1):
        print(">> %s : begin: %s %s %d %d %s %0.2f %s "
              "%s %s %.2f %.2f %.2f %.2f" % (
                  __file__, modelenvfile, drainageid, year, nstrikes,
                  ablation_method,
                  threshold,
                  use_daily_threshold_file,
                  rainfall_col, runoff_col,
                  min_snow_ddf, max_snow_ddf, min_ice_ddf, max_ice_ddf),
              file=sys.stderr)

    myEnv = ModelEnv(tileConfigFile=modelenvfile)
    result = CalibrationStats(
        myEnv, drainageid, [year], nstrikes,
        min_snow_ddf, max_snow_ddf, min_ice_ddf, max_ice_ddf,
        rainfall_col, runoff_col,
        ablation_method=ablation_method,
        threshold=threshold,
        use_daily_threshold_file=use_daily_threshold_file)

    SaveCalibrationStats(
        myEnv, drainageid, nstrikes,
        result,
        ablation_method=ablation_method,
        threshold=threshold,
        use_daily_threshold_file=use_daily_threshold_file,
        file=sys.stdout,
        header=header)


if __name__ == '__main__':
    run_calibration_stats()
