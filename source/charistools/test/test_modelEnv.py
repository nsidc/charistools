'''
Nose tests for modelEnv

To run tests : nosetests    test_modelEnv.py
Verbose (-v) : nosetests -v test_modelEnv.py

2015-09-25 M. J. Brodzik 303-492-8263 brodzik@nsidc.org
National Snow & Ice Data Center, University of Colorado at Boulder
Copyright (c) 2015 Regents of the University of Colorado

'''
from __future__ import print_function

from nose.tools import assert_equals
from nose.tools import assert_raises

import numpy as np
import os
import re

from charistools.modelEnv import ModelEnv

verbose = False
testConfigFile = os.path.join(os.path.dirname(__file__),
                              'modis_tiles_config.ini')
calibrationConfigFile = os.path.join(os.path.dirname(__file__),
                                     'calibration_modelEnv_config.ini')
etConfigFile = os.path.join(os.path.dirname(__file__),
                            'et_modelEnv_config.ini')
customConfigFile = os.path.join(os.path.dirname(__file__),
                                'custom_config.ini')
topDir = os.path.join(os.path.dirname(__file__))


def test_init_modelEnv_bogus_file():
    assert_raises(IOError, ModelEnv, tileConfigFile='bogus.ini')


def test_init_modelEnv_no_config_file():
    # No configFile should still make state data about the MODIS tile
    # available
    myEnv = ModelEnv()
    assert_equals(myEnv.modis_tile_500m_cols, 2400)


def test_init_modelEnv_no_filename():
    # No configFile should still make state data about the MODIS tile
    # available, and should error for any filename inquiries
    myEnv = ModelEnv()
    assert_raises(LookupError, myEnv.fixed_filename)


def test_init_modelEnv_with_bogus_topDir():
    assert_raises(OSError, ModelEnv, tileConfigFile=testConfigFile,
                  topDir='/test')


def test_init_modelEnv_with_good_topDir():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir='/vagrant')
    assert_equals(myEnv.tileConfig['model_top_dir'],
                  "/vagrant")


def test_set_model_config_file():
    myEnv = ModelEnv()
    assert_equals(myEnv.set_model_config_file(tileConfigFile=testConfigFile),
                  True)
    assert_equals(myEnv.tileConfig['model_top_dir'],
                  "/path/to/model/data")


def test_set_model_top_dir():
    myEnv = ModelEnv(tileConfigFile=testConfigFile)
    assert_equals(myEnv.set_model_top_dir(topDir=topDir), True)
    assert_equals(myEnv.tileConfig['model_top_dir'],
                  topDir)


def test_modice_filename_no_tileID():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_raises(ValueError, myEnv.fixed_filename)


def test_basin_filename_no_drainageID():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_raises(ValueError, myEnv.fixed_filename, type='basin_mask')


def test_modice_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.fixed_filename(type='modice_min05yr', tileID='h23v05',
                                       verbose=True),
                  topDir +
                  '/modicev04/MODICE.v0.4.h23v05.1strike.min05yr.mask.nc')


def test_dem_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.fixed_filename(type='dem', tileID='h23v05'),
                  topDir + '/SRTMGL3/SRTMGL3.v0.1.h23v05.tif')


def test_basin_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.fixed_filename(type='basin_mask', tileID='h23v05',
                                       drainageID='IN_Indus_at_Kotri'),
                  topDir +
                  '/basin_masks/IN_Indus_at_Kotri.basin_mask.h23v05.tif')


def test_scag_filename_no_tileID():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_raises(ValueError, myEnv.forcing_filename, type='modscag_gf')


def test_scag_filename_no_yyyy():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_raises(ValueError,
                  myEnv.forcing_filename,
                  type='modscag_gf', tileID='h23v05')


def test_scag_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.forcing_filename(type='modscag_gf',
                                         tileID='h23v05',
                                         year=2005,
                                         verbose=True),
                  topDir +
                  '/snow_cover/MODSCAG_GF/MODSCAG_GF_Snow.v0.5.h23v05_2005.h5')


def test_mod10a1_snow_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.forcing_filename(type='mod10a1_gf',
                                         tileID='h23v05',
                                         year=2005),
                  topDir +
                  '/snow_cover/mod10a1_snow_gf/MOD10A1_GF_Snow.v0.5.h23v05_2005.h5')


def test_temperature_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.forcing_filename(type='temperature',
                                         tileID='h23v05',
                                         year=2001),
                  topDir +
                  '/temperature/h23v05/' +
                  'ERA_Interim_downscale_mx_tsurf.v0.2.h23v05_2001.h5')


def test_grsize_scag_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.forcing_filename(type='grsize_scag',
                                         tileID='h23v05',
                                         year=2005),
                  topDir +
                  '/grain_size/MODSCAG_GF/MODSCAG_GF_GrainSize.v0.6.h23v05_2005.h5')


def test_modice_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.hypsometry_filename(
        type='modice_min05yr_by_elevation',
        drainageID='IN_Indus_at_Kotri',
        verbose=True),
                  topDir +
                  '/modicev04/by_elevation/' +
                  'IN_Indus_at_Kotri.0100m.modicev04_1strike_area_by_elev.txt')


def test_snow_on_land_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.hypsometry_filename(
        type='snow_on_land_by_elevation',
        drainageID='IN_Indus_at_Kotri',
        year=2001),
                  topDir +
                  '/surface_area/by_elevation/' +
                  'IN_Indus_at_Kotri.2001.0100m.' +
                  'snow_on_land_area_by_elev.txt')


def test_snow_on_ice_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.hypsometry_filename(
        type='snow_on_ice_by_elevation',
        drainageID='IN_Indus_at_Kotri',
        year=2001,
        ablation_method='grsize_scag',
        threshold=200),
                  topDir +
                  '/surface_area/by_elevation/' +
                  'IN_Indus_at_Kotri.2001.0100m.' +
                  'GRSIZE_SCAG.0200.snow_on_ice_area_by_elev.txt')


def test_exposed_glacier_ice_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.hypsometry_filename(
        type='exposed_glacier_ice_by_elevation',
        drainageID='IN_Indus_at_Kotri',
        year=2001,
        ablation_method='grsize_scag',
        threshold=200),
                  topDir +
                  '/surface_area/by_elevation/' +
                  'IN_Indus_at_Kotri.2001.0100m.' +
                  'GRSIZE_SCAG.0200.exposed_glacier_ice_area_by_elev.txt')


def test_exposed_glacier_ice_var_thresh_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.hypsometry_filename(
        type='exposed_glacier_ice_by_elevation',
        drainageID='IN_Indus_at_Kotri',
        year=2001,
        ablation_method='grsize_scag',
        threshold='fromFile'),
                  topDir +
                  '/surface_area/by_elevation/' +
                  'IN_Indus_at_Kotri.2001.0100m.' +
                  'GRSIZE_SCAG.fromFile.exposed_glacier_ice_area_by_elev.txt')


def test_exposed_glacier_ice_from_albedo_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    threshold = 0.46
    assert_equals(myEnv.hypsometry_filename(
        type='exposed_glacier_ice_by_elevation',
        drainageID='IN_Indus_at_Kotri',
        year=2001,
        ablation_method='albedo_mod10a1',
        threshold=threshold),
                  topDir +
                  '/surface_area/by_elevation/' +
                  'IN_Indus_at_Kotri.2001.0100m.' +
                  'ALBEDO_MOD10A1.0046.exposed_glacier_ice_area_by_elev.txt')


def test_temperature_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.hypsometry_filename(
        type='temperature_by_elevation',
        drainageID='IN_Indus_at_Kotri',
        year=2001),
                  topDir +
                  '/temperature/by_elevation/' +
                  'IN_Indus_at_Kotri.2001.0100m.' +
                  'corrected.v2.temperature_by_elev.txt')


def test_area_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir=topDir)
    assert_equals(myEnv.hypsometry_filename(
        type='area_by_elevation',
        drainageID='IN_Indus_at_Kotri'),
                  topDir +
                  '/surface_area/by_elevation/' +
                  'IN_Indus_at_Kotri.0100m.area_by_elev.txt')


def test_et_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=etConfigFile)
    filename = myEnv.hypsometry_filename(
        type='et_by_elevation',
        drainageID='IN_Indus_at_Kotri',
        et_source='mod16')
    assert_equals(os.path.basename(filename),
                  'IN_Indus_at_Kotri.0100m.mod16.ET_by_elev.txt')


def test_custom_ddf_hypsometry_filename():
    myEnv = ModelEnv(tileConfigFile=customConfigFile,
                     topDir=os.path.join(topDir, 'test_files'))

    filename = myEnv.hypsometry_filename(
        type='custom_ice_ddf',
        drainageID='test_basin',
        contour_m=500)
    assert_equals(os.path.basename(filename),
                  'test_basin.0500m.custom.iceDDFs.dat')


def test_calibration_filename():
    myEnv = ModelEnv(tileConfigFile=calibrationConfigFile)
    assert_equals(myEnv.calibration_filename(
        type='rainfall',
        drainageID='IN_Hunza_at_DainyorBridge'),
                  '/projects/CHARIS/charistools_test_data/rainfall/' +
                  'IN_Hunza_at_DainyorBridge.APHRODITE_rainfall_km3.monthly.csv')


def test_tileIDs_for_drainage():
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.new_masknames.ini')
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir="/projects/CHARIS/charistools_test_data")
    assert_equals(myEnv.tileIDs_for_drainage(drainageID="IN_Hunza_at_Danyour"),
                  ['h23v05', 'h24v05'])


def test_tileIDs_for_bogus_drainage():
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir="/projects/CHARIS/charistools_test_data")
    assert_raises(RuntimeError, myEnv.tileIDs_for_drainage,
                  drainageID="Bogus")


def test_bogus_model_inputs():
    myEnv = ModelEnv(tileConfigFile=calibrationConfigFile)

    assert_raises(TypeError,
                  myEnv.model_inputs)


def test_model_inputs():
    myEnv = ModelEnv(tileConfigFile=calibrationConfigFile)

    out = myEnv.model_inputs(
        drainageID="IN_Hunza_at_DainyorBridge",
        year=2001,
        ablation_method="albedo_mod10a1",
        threshold=0.40)
    assert_equals(np.shape(out['temperature_by_elevation_hyps'].data), (365, 64))


def test_model_inputs_for_3strikes():
    myEnv = ModelEnv(tileConfigFile=calibrationConfigFile)

    nstrikes = 3
    out = myEnv.model_inputs(
        drainageID="IN_Hunza_at_DainyorBridge",
        year=2001,
        modice_nstrikes=nstrikes,
        ablation_method="albedo_mod10a1",
        threshold=0.40)
    assert_equals(
        re.search('(modicev04_.strike)',
                  out["exposed_glacier_ice_by_elevation_filename"]).group(1),
        "modicev04_%dstrike" % nstrikes)


def test_model_inputs_bad_drainage():
    myEnv = ModelEnv(tileConfigFile=calibrationConfigFile)

    assert_raises(IOError, myEnv.model_inputs,
                  drainageID="IN_Hunza",
                  year=2001,
                  ablation_method="albedo_mod10a1",
                  threshold=0.40)


def test_model_inputs_custom_ddfs():
    myEnv = ModelEnv(tileConfigFile=customConfigFile,
                     topDir=os.path.join(topDir, 'test_files'))

    out = myEnv.model_inputs(
        drainageID="test_basin",
        year=2001,
        ablation_method="albedo_mod10a1",
        threshold=0.40,
        contour_m=500,
        get_custom_snow_ddf=True)
    assert_equals(np.shape(out['custom_snow_ddf_hyps'].data), (6, 5))


def test_threshold_filename():
    myEnv = ModelEnv(tileConfigFile=calibrationConfigFile)
    assert_equals(myEnv.forcing_filename(
        type='threshold',
        verbose=True),
                  '/projects/CHARIS/charistools_test_data/snow_ice_partition/' +
                  'p149r035_MODSCAG_GS_threshold.csv')


def test_et_filename():
    myEnv = ModelEnv(tileConfigFile=calibrationConfigFile)
    yyyy = 2001
    mm = 1
    tileID = 'h24v05'
    assert_equals(
        myEnv.forcing_filename(type='mod16a2',
                               tileID=tileID,
                               year=yyyy,
                               month=mm),
        "%s/%s/MOD16A2.A%dM%02d.%s.105.2013121032237.hdf" % (
            "/projects/CHARIS/charistools_test_data",
            "evapotranspiration/mod16/monthly",
            yyyy, mm, tileID))
