'''
Nose tests for charistools utilities

To run tests : fab [-v] test.all

'''
from __future__ import print_function

from nose.tools import assert_equals
import os

from charistools.modelEnv import ModelEnv
from charistools.run_forcings_by_drainageid import run_forcings_by_drainageid


def test_forcings_for_gilgit():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'test_gilgit_forcings_env.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile)

    modelLabel = 'TestModel'
    drainageID = 'IN_Gilgit_GDBD'
    year = 2001
    fsca_type = 'modscag_gf'
    ablation_method = 'grsize_scag'
    thresholds = [205]

    # Remove any leftover test files
    temperature_hyps_filename = modelEnv.hypsometry_filename(
        type='temperature_by_elevation',
        drainageID=drainageID,
        year=year)
    sol_hyps_filename = modelEnv.hypsometry_filename(
        type='snow_on_land_by_elevation',
        drainageID=drainageID,
        year=year)
    soi_hyps_filename = modelEnv.hypsometry_filename(
        type='snow_on_ice_by_elevation',
        drainageID=drainageID,
        year=year,
        ablation_method=ablation_method,
        threshold=thresholds[0])
    egi_hyps_filename = modelEnv.hypsometry_filename(
        type='exposed_glacier_ice_by_elevation',
        drainageID=drainageID,
        year=year,
        ablation_method=ablation_method,
        threshold=thresholds[0])

    try:
        os.remove(temperature_hyps_filename)
        os.remove(sol_hyps_filename)
        os.remove(soi_hyps_filename)
        os.remove(egi_hyps_filename)
    except OSError:
        pass

    assert_equals(
        run_forcings_by_drainageid(
            modelLabel,
            drainageID=drainageID,
            year=year,
            modelEnv=modelEnv,
            fsca_type=fsca_type,
            ablation_method=ablation_method,
            thresholds=thresholds,
            stop_doy=1),
        True)

    # Clean up test files
    os.remove(temperature_hyps_filename)
    os.remove(sol_hyps_filename)
    os.remove(soi_hyps_filename)
    os.remove(egi_hyps_filename)
