'''
Nose tests for hypsometry class

To run tests, cd to /vagrant/source and do:
fab [-v] test.all

'''
from __future__ import print_function

import filecmp  # noqa
import matplotlib.pyplot as plt  # noqa
from nose.tools import assert_is, assert_equals  # noqa
from nose.tools import assert_almost_equals  # noqa
from nose.tools import assert_raises  # noqa
import numpy as np  # noqa
import os  # noqa
import pandas as pd  # noqa
import shutil  # noqa
import sys  # noqa

from charistools.hypsometry import Hypsometry  # noqa
from charistools.meltModels import TempIndexMelt
from charistools.meltModels import TriSurfTempIndexMelt


def test_read():
    SOLFile = os.path.join(
        os.path.dirname(__file__),
        'test_files',
        'GA_Langtang_at_Kyanjin.2012.0100m.modicev04_3strike.snow_on_land_area_by_elev.txt')
    SOIFile = os.path.join(
        os.path.dirname(__file__),
        'test_files',
        'GA_Langtang_at_Kyanjin.2012.0100m.modicev04_3strike.GRSIZE_SCAG.fromFile.snow_on_ice_area_by_elev.txt')
    EGIFile = os.path.join(
        os.path.dirname(__file__),
        'test_files',
        'GA_Langtang_at_Kyanjin.2012.0100m.modicev04_3strike.GRSIZE_SCAG.fromFile.exposed_glacier_ice_area_by_elev.txt')
    tempFile = os.path.join(
        os.path.dirname(__file__),
        'test_files',
        'Langtang2012_AWS_temps.txt')
    customIceDDFFile = os.path.join(
        os.path.dirname(__file__),
        'test_files',
        'GA_Langtang_at_Kyanjin.custom_ice_ddf.txt')
    customSnowDDFFile = os.path.join(
        os.path.dirname(__file__),
        'test_files',
        'GA_Langtang_at_Kyanjin.custom_snow_ddf.txt')
    SOL_hyps = Hypsometry(SOLFile)
    SOI_hyps = Hypsometry(SOIFile)
    EGI_hyps = Hypsometry(EGIFile)
    temp_hyps = Hypsometry(tempFile)
    custom_ice_hyps = Hypsometry(customIceDDFFile)
    custom_snow_hyps = Hypsometry(customSnowDDFFile)
    Hypsometry.unfurl(custom_ice_hyps)
    Hypsometry.unfurl(custom_snow_hyps)

    EGI_melt_hyps = TempIndexMelt(EGI_hyps, temp_hyps, custom_ddf_hyps=custom_ice_hyps)
    print(EGI_melt_hyps.data.get_value(pd.to_datetime("2012-05-08"), 4800), file=sys.stderr)

    (SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps) = TriSurfTempIndexMelt(
        SOLFile=SOLFile,
        SOIFile=SOIFile,
        EGIFile=EGIFile,
        temperatureFile=tempFile,
        customSnowDDFFile=customSnowDDFFile,
        customIceDDFFile=customIceDDFFile,
        verbose=True)
