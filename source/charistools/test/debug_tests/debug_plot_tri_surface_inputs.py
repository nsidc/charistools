'''
Nose tests for charistools temperature-index melt model

To run tests : fab [-v] test.all

'''
from __future__ import print_function

import matplotlib  # noqa
# matplotlib.use('Agg')
from nose.tools import assert_equals  # noqa
from nose.tools import assert_almost_equals  # noqa
from nose.tools import assert_raises  # noqa
import numpy as np  # noqa
import os  # noqa
import pandas as pd  # noqa
import sys  # noqa

from charistools.hypsometry import Hypsometry  # noqa
from charistools.modelEnv import ModelEnv  # noqa
from charistools.meltModels import _rmse  # noqa
from charistools.meltModels import _seasonal_ddfs  # noqa
from charistools.meltModels import _volumetric_difference_pcent  # noqa
from charistools.meltModels import CalibrateTriSurfTempIndexMelt  # noqa
from charistools.meltModels import TempIndexMelt  # noqa
from charistools.meltModels import TriSurfTempIndexMelt  # noqa
from charistools.meltModels import PlotTriSurfInput  # noqa


calibrationConfigFile = os.path.join(os.path.dirname(__file__),
                                     'calibration_modelEnv_config.ini')


def test_plot_tri_surface_inputs():

    # Read hypsometry inputs for SOL, SOI, EGI, temperatures
    SOLFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
    SOIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.snow_on_ice.dat')
    EGIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.ablation.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_basin.500.temperature.dat')
    iceFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ice.dat')
    SOL_hyps = Hypsometry(filename=SOLFile)
    SOI_hyps = Hypsometry(filename=SOIFile)
    EGI_hyps = Hypsometry(filename=EGIFile)
    temperature_hyps = Hypsometry(filename=temperatureFile)
    ice_hyps = Hypsometry(filename=iceFile)

    outFile = os.path.join(os.path.dirname(__file__),
                           'test_basin.500.input_plot.png')
    try:
        os.remove(outFile)
    except OSError:
        pass

    fig, ax = matplotlib.pyplot.subplots(1, 1)
    title = "Test basin inputs"
    ax, ax1 = PlotTriSurfInput(ax=ax,
                               title=title,
                               SOL_hyps=SOL_hyps,
                               SOI_hyps=SOI_hyps,
                               EGI_hyps=EGI_hyps,
                               temperature_hyps=temperature_hyps,
                               ice_hyps=ice_hyps,
                               ice_color='Red',
                               hline_temperature_C=2.)
    assert_equals(ax.get_title(), title)

    fig.tight_layout()
    fig.savefig(outFile)


def test_plot_tri_surface_inputs_no_ice():

    # Read hypsometry inputs for SOL, SOI, EGI, temperatures
    SOLFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
    SOIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.no_snow_on_ice.dat')
    EGIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.no_exposed_glacier_ice.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_basin.500.temperature.dat')
    iceFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ice.dat')
    SOL_hyps = Hypsometry(filename=SOLFile)
    SOI_hyps = Hypsometry(filename=SOIFile)
    EGI_hyps = Hypsometry(filename=EGIFile)
    temperature_hyps = Hypsometry(filename=temperatureFile)
    ice_hyps = Hypsometry(filename=iceFile)

    outFile = os.path.join(os.path.dirname(__file__),
                           'test_basin.500.no_ice.input_plot.png')
    try:
        os.remove(outFile)
    except OSError:
        pass

    fig, ax = matplotlib.pyplot.subplots(1, 1)
    title = "Test basin inputs"
    ax, ax1 = PlotTriSurfInput(ax=ax,
                               title=title,
                               SOL_hyps=SOL_hyps,
                               SOI_hyps=SOI_hyps,
                               EGI_hyps=EGI_hyps,
                               temperature_hyps=temperature_hyps,
                               ice_hyps=ice_hyps,
                               ice_color='Red',
                               hline_temperature_C=2.)
    assert_equals(ax.get_title(), title)

    fig.tight_layout()
    fig.savefig(outFile)
