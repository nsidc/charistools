'''
Nose tests for charistools utilities

To run tests : fab [-v] test.all

'''
from __future__ import print_function

import os
import sys

from charistools.convertors import Fsca2Hypsometry
from charistools.hypsometry import Hypsometry
from charistools.modelEnv import ModelEnv
from charistools.meltModels import TempIndexMelt


def test_on_ice_forcings():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'test_wrong_columns.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile)

    drainageID = 'AM_OBJECTID72'
    year = 2001
    stop_doy = 2
    fsca_type = 'modscag_gf'
    fsca_area = 'SOI/EGI'
    ablation_method = 'grsize_scag'
    threshold = 205
    modice_nstrikes = 3

    # Run the convertor to hyps
    out = Fsca2Hypsometry(drainageID=drainageID,
                          year=year,
                          modelEnv=modelEnv,
                          stop_doy=stop_doy,
                          fsca_type=fsca_type,
                          fsca_area=fsca_area,
                          ablation_method=ablation_method,
                          threshold=threshold,
                          modice_nstrikes=modice_nstrikes,
                          soi_outfile='test_soi.out',
                          egi_outfile='test_egi.out',
                          verbose=True)

    print("EGI Hyps data", file=sys.stderr)
    print(out["EGI"].data, file=sys.stderr)

    print("SOI Hyps data", file=sys.stderr)
    print(out["SOI"].data, file=sys.stderr)


def test_wrong_columns():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'test_wrong_columns.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile)

    drainageID = 'AM_OBJECTID72'
    year = 2001

    # Read a temperature hypsometry with missing temperatures
    # Read an area file for SOLFile
    temperature_hyps_filename = modelEnv.hypsometry_filename(
        type='temperature_by_elevation',
        drainageID=drainageID,
        year=year,
        modice_nstrikes=3)
    temperature_hyps = Hypsometry(filename=temperature_hyps_filename)

    egi_hyps_filename = modelEnv.hypsometry_filename(
        type='exposed_glacier_ice_by_elevation',
        drainageID=drainageID,
        year=year,
        threshold=205,
        ablation_method='grsize_scag',
        modice_nstrikes=3)
    egi_hyps = Hypsometry(filename=egi_hyps_filename)

    # Run the model and see how many columns are returned
    melt_hyps = TempIndexMelt(
        area_hyps=egi_hyps,
        temperature_hyps=temperature_hyps,
        verbose=True)

    # set_trace()
    print("Hyps data", file=sys.stderr)
    print(melt_hyps.data, file=sys.stderr)

    melt_hyps.write(filename='test.txt', verbose=True)

    new_hyps = Hypsometry('test.txt')
    print("new hyps is", file=sys.stderr)
    print(new_hyps.data, file=sys.stderr)

    print("temp hyps is", file=sys.stderr)
    print(temperature_hyps.data, file=sys.stderr)
    # print(egi_hyps.data, file=sys.stderr)
    # print(melt_hyps.data, file=sys.stderr)
