'''
Nose tests for find_best_model.py

'''
from __future__ import print_function

from nose.tools import assert_equals

from charistools.find_best_model import find_best_model
from charistools.find_best_model import get_calibration_stats
from click.testing import CliRunner
import numpy as np
import os


# def test_find_best_model_Naryn_0():
#     runner = CliRunner()
#     infile = os.path.join(os.path.dirname(__file__),
#                           "test_files/SY_Naryn_calibration_2strikes/Naryn_0.out")
#     result = runner.invoke(find_best_model, [infile,
#                                              '--verbose'])
#     assert_equals(result.exit_code, 0)
#     print(result.output_bytes, file=sys.stderr)
#     assert "Best model is 4.78_5.78_26.89_31.39" in result.output


# def test_find_best_model_Naryn_01():
#     runner = CliRunner()
#     infile = os.path.join(os.path.dirname(__file__),
#                           "test_files/SY_Naryn_calibration_2strikes/Naryn_01.out")
#     result = runner.invoke(find_best_model, [infile,
#                                              '--verbose'])
#     assert_equals(result.exit_code, 0)
#     print(result.output_bytes, file=sys.stderr)


def test_find_best_model():
    runner = CliRunner()
    infile = os.path.join(os.path.dirname(__file__),
                          "test_files/cal_test_compare.out")
    result = runner.invoke(find_best_model, [infile, '--verbose'])
    assert_equals(result.exit_code, 0)
    assert "old-style" in result.output_bytes
    assert "Best model is 0.1_1.56_2.03_3.33" in result.output


def test_return_stats():
    infile = os.path.join(os.path.dirname(__file__),
                          "test_files/cal_test_compare.out")
    out = get_calibration_stats(file=infile)
    assert_equals("0.1_1.56_2.03_3.33", out["stats"].index[0])
    assert isinstance(out["stats"].min_snow_ddf.iloc[0],
                      np.float64)
