# Data processing configuration for MODIS-tile data
# used by CHARIS melt modeling
#
# First section contains general information
# about MODIS tiles (dimensions/resolutions)
#
# Each section thereafter contains information for reading
# a type of MODIS tile data
# directory, filename pattern and expected data type (byte, float)
# Expected data type is only needed for flat binary data,
# since tif, h5 and nc file readers will take care of type issues

model_top_dir = /projects/CHARIS/charistools_test_data

# Input data filename patterns   
[input]

    # Model forcing data that varies by time (annual, daily)
    [[forcing]]

        # Location of annual cubes of daily MODSCAG_GF
	# (gap-filled modscag) snow raster data
	[[[modscag_gf]]]
	dir = %MODEL_TOP_DIR%/snow_cover/MODSCAG_GF
	pattern = MODSCAG_GF_Snow.v0.5.%TILEID%_%YYYY%.h5
	id = MODSCAG_GF_Snow.v0.5
	
        # Location of annual cubes of daily MOD10A1_GF
	# (gap-filled mod10a1 fractional) snow raster data
	[[[mod10a1_gf]]]
	dir = %MODEL_TOP_DIR%/snow_cover/mod10a1_snow_gf
	pattern = MOD10A1_GF_Snow.v0.5.%TILEID%_%YYYY%.h5
	id = MOD10A1_GF_Snow.v0.5
	
        # Location of annual cubes of daily grain size from
	# MODSCAG_GF (gap-filled modscag) snow raster data
	[[[grsize_scag]]]
	dir = %MODEL_TOP_DIR%/grain_size/MODSCAG_GF
	pattern = MODSCAG_GF_GrainSize.v0.5.%TILEID%_%YYYY%.h5
	id = MODSCAG_GF_GrainSize.v0.5
	
        # Location of annual cubes of daily albedo from
	# MOD10A1 snow data
	[[[albedo_mod10a1]]]
	dir = %MODEL_TOP_DIR%/albedo/MOD10A1_GF
	pattern = MOD10A1_GF_Albedo_shortwave.v0.5.%TILEID%_%YYYY%.h5
	id = MOD10A1_GF_Albedo_shortwave.v0.5
	
	# Location of downscaled temperature data
	# 	dir = %MODEL_TOP_DIR%/temperature/corrected/v2/%TILEID%/%YYYY%
	# 	pattern = tsurf_%TILEID%_day_noinversion.%YYYYMMDD%.corrected.v2.bin
	[[[temperature]]]
	dir = %MODEL_TOP_DIR%/temperature/%TILEID%
	pattern = ERA_Interim_downscale_mx_tsurf.v0.2.%TILEID%_%YYYY%.h5
	id = ERA_Interim_dscale_mx_tsurf.v0.2

    [[fixed]]
        [[[basin_mask]]]
	dir = %MODEL_TOP_DIR%/basin_masks
	pattern = %DRAINAGEID%.basin_mask.%TILEID%.tif
	id = CHARIS

	[[[dem]]]
	dir = %MODEL_TOP_DIR%/SRTMGL3
	pattern = SRTMGL3.v0.1.%TILEID%.tif
	id = CHARIS_SRTMGL3.v0.1

	[[[modice_min05yr]]]
	# Location of MODICE v04 5-year intersection data
	dir = %MODEL_TOP_DIR%/modicev04
	pattern = MODICE.v0.4.%TILEID%.%NSTRIKES%strike.min05yr.mask.nc
	id = MODICE.v0.4

[hypsometry]
    
    [[modice_min05yr_by_elevation]]
    dir = %MODEL_TOP_DIR%/modicev04/by_elevation
    pattern = %DRAINAGEID%.%CONTOUR%.modicev04_%NSTRIKES%strike_area_by_elev.txt

    [[snow_on_land_by_elevation]]
    dir = %MODEL_TOP_DIR%/derived_hypsometries/%DRAINAGEID%
    pattern = %DRAINAGEID%.%YYYY%.%CONTOUR%.modicev04_%NSTRIKES%strike.%SURFACE%_area_by_elev.txt

    [[snow_on_ice_by_elevation]]
    dir = %MODEL_TOP_DIR%/derived_hypsometries/%DRAINAGEID%
    pattern = %DRAINAGEID%.%YYYY%.%CONTOUR%.modicev04_%NSTRIKES%strike.%ABLATION_METHOD%.%THRESHOLD%.%SURFACE%_area_by_elev.txt

    [[exposed_glacier_ice_by_elevation]]
    dir = %MODEL_TOP_DIR%/derived_hypsometries/%DRAINAGEID%
    pattern = %DRAINAGEID%.%YYYY%.%CONTOUR%.modicev04_%NSTRIKES%strike.%ABLATION_METHOD%.%THRESHOLD%.%SURFACE%_area_by_elev.txt

    [[temperature_by_elevation]]
    dir = %MODEL_TOP_DIR%/derived_hypsometries/%DRAINAGEID%
    pattern = %DRAINAGEID%.%YYYY%.%CONTOUR%.ERA_Interim_downscale_uncorrected_tsurf.v0.2_by_elev.txt
    
    [[area_by_elevation]]
    dir = %MODEL_TOP_DIR%/surface_area/by_elevation
    pattern = %DRAINAGEID%.%CONTOUR%.area_by_elev.txt

    [[snow_on_land_melt_by_elevation]]
    dir = %MODEL_TOP_DIR%/melt_volume/by_elevation
    pattern = %DRAINAGEID%.%YYYY%.%CONTOUR%.%SURFACE%_melt_by_elev.txt

    [[snow_on_ice_melt_by_elevation]]
    dir = %MODEL_TOP_DIR%/melt_volume/by_elevation
    pattern = %DRAINAGEID%.%YYYY%.%CONTOUR%.%ABLATION_METHOD%.%THRESHOLD%.%SURFACE%_melt_by_elev.txt

    [[exposed_glacier_ice_melt_by_elevation]]
    dir = %MODEL_TOP_DIR%/melt_volume/by_elevation
    pattern = %DRAINAGEID%.%YYYY%.%CONTOUR%.%ABLATION_METHOD%.%THRESHOLD%.%SURFACE%_melt_by_elev.txt

[calibration]

    [[rainfall]]
    dir = %MODEL_TOP_DIR%/rainfall
    pattern = %DRAINAGEID%.APHRODITE_rainfall_adj_merra_ETclim.txt
    id = 'APHRODITE_rainfall_less_MERRA_ET'

    [[runoff]]
    dir = %MODEL_TOP_DIR%/streamflow
    pattern = %DRAINAGEID%.month_runoff.dat
    id = 'CHARIS_streamflow'




