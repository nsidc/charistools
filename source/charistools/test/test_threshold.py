'''
Nose tests for threshold class

To run tests : nosetests    test_threshold.py
Verbose (-v) : nosetests -v test_threshold.py

2016-11-27 M. J. Brodzik 303-492-8263 brodzik@nsidc.org
National Snow & Ice Data Center, University of Colorado at Boulder
Copyright (c) 2016 Regents of the University of Colorado

'''
from __future__ import print_function

from nose.tools import assert_equals
from nose.tools import assert_raises

from charistools.threshold import Threshold

verbose = False


def test_init():
    threshold = Threshold()
    assert_equals(threshold.value(doy=1), 205.)
    assert_equals(threshold.value(doy=366), 205.)
    assert_raises(KeyError, threshold.value, doy=0)
    assert_raises(KeyError, threshold.value, doy=367)


def test_init_for_value():
    value = 300.
    threshold = Threshold(threshold=value)
    assert_equals(threshold.value(doy=1), value)
    assert_equals(threshold.value(doy=366), value)


def test_init_from_file():
    filename = "/projects/CHARIS/charistools_test_data/snow_ice_partition/" \
               "p149r035_MODSCAG_GS_threshold.csv"
    threshold = Threshold(filename=filename)
    assert_equals(124, threshold.value(doy=1))
    assert_equals(204, threshold.value(doy=248))
    assert_equals(124, threshold.value(doy=366))
    assert_raises(KeyError, threshold.value, doy=0)
    assert_raises(KeyError, threshold.value, doy=367)
