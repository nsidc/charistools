'''
Nose tests for figuring out missing_ablation_data bug

To run tests : fab [-v] test.all

'''
from __future__ import print_function

from nose.tools import assert_raises
import os
import pandas as pd

from charistools.convertors import Fsca2Hypsometry
from charistools.convertors import Modice2Hypsometry
from charistools.modelEnv import ModelEnv


def test_default_proportion():

    pd.options.display.float_format = '{:,.2f}'.format
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    drainageid = "IN_Hunza_at_Danyour"
    modice_by_elev = Modice2Hypsometry(
        drainageid,
        modelEnv)

    by_elev = Fsca2Hypsometry(
        drainageid,
        2001,
        fsca_type='mod10a1_gf',
        fsca_area='SOI/EGI',
        ablation_method='albedo_mod10a1',
        threshold=0.40,
        modelEnv=modelEnv, start_doy=180, stop_doy=180)

    all = _combine_all(modice_by_elev, by_elev)
    compare_filename = os.path.join(os.path.dirname(__file__),
                                    'compare',
                                    'IN_Hunza.soi_egi.2001-06-29.default.pkl')
    compare = pd.read_pickle(compare_filename)
    compare = compare.ix[:, ["MODICE", "SOI", "EGI", "sum", "remain"]]

    # comparison data were created in July, when I was adding an extra
    # elevation band that was unnecessary.  Do the comparison against all
    # but the last elevation band
    assert(compare.iloc[:-1].equals(all))


def test_unclass_to_egi():

    pd.options.display.float_format = '{:,.2f}'.format
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    drainageid = "IN_Hunza_at_Danyour"
    modice_by_elev = Modice2Hypsometry(
        drainageid,
        modelEnv)

    by_elev = Fsca2Hypsometry(
        drainageid,
        2001,
        fsca_type='mod10a1_gf',
        fsca_area='SOI/EGI',
        ablation_method='albedo_mod10a1',
        unclass_to_egi=1.,
        threshold=0.40,
        modelEnv=modelEnv, start_doy=180, stop_doy=180)

    all = _combine_all(modice_by_elev, by_elev)
    compare_filename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'IN_Hunza.soi_egi.2001-06-29.unclass_to_egi.pkl')
    compare = pd.read_pickle(compare_filename)
    compare = compare.ix[:, ["MODICE", "SOI", "EGI", "sum", "remain"]]

    # comparison data were created in July, when I was adding an extra
    # elevation band that was unnecessary.  Do the comparison against all
    # but the last elevation band
    assert(compare.iloc[:-1].equals(all))


def test_unclass_split5050():

    pd.options.display.float_format = '{:,.2f}'.format
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    drainageid = "IN_Hunza_at_Danyour"
    modice_by_elev = Modice2Hypsometry(
        drainageid,
        modelEnv)

    by_elev = Fsca2Hypsometry(
        drainageid,
        2001,
        fsca_type='mod10a1_gf',
        fsca_area='SOI/EGI',
        ablation_method='albedo_mod10a1',
        unclass_to_egi=0.5,
        threshold=0.40,
        modelEnv=modelEnv, start_doy=180, stop_doy=180)

    all = _combine_all(modice_by_elev, by_elev)
    compare_filename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'IN_Hunza.soi_egi.2001-06-29.unclass_5050.pkl')
    compare = pd.read_pickle(compare_filename)
    compare = compare.ix[:, ["MODICE", "SOI", "EGI", "sum", "remain"]]

    # comparison data were created in July, when I was adding an extra
    # elevation band that was unnecessary.  Do the comparison against all
    # but the last elevation band
    assert(compare.iloc[:-1].equals(all))


def test_bad_input():

    pd.options.display.float_format = '{:,.2f}'.format
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    drainageid = "IN_Hunza_at_Danyour"
    assert_raises(ValueError, Fsca2Hypsometry,
                  drainageid,
                  2001,
                  fsca_type='mod10a1_gf',
                  fsca_area='SOI/EGI',
                  ablation_method='albedo_mod10a1',
                  unclass_to_egi=1.1,
                  threshold=0.40,
                  modelEnv=modelEnv, start_doy=180, stop_doy=180)


def _combine_all(modice_df, by_elev):

    all = modice_df.data.transpose()
    all = pd.concat([all, by_elev['SOI'].data.loc['2001-06-29']],
                    axis=1, ignore_index=True)
    all = pd.concat([all, by_elev['EGI'].data.loc['2001-06-29']],
                    axis=1, ignore_index=True)
    all.columns = ['MODICE', 'SOI', 'EGI']
    all['sum'] = all['SOI'] + all['EGI']
    all['remain'] = all['MODICE'] - all['sum']

    return(all)
