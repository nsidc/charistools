'''
Nose tests for run_calibration_stats

'''
from __future__ import print_function

from nose.tools import assert_equals

from charistools.run_calibration_stats import run_calibration_stats
from click.testing import CliRunner
import os
import re


def test_run_calibration_stats_bad_modelenv():
    runner = CliRunner()
    result = runner.invoke(run_calibration_stats, ["bogus_env.ini"])
    assert_equals(result.exit_code, 2)


def test_run_calibration_stats_bad_drainageid():
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_modelEnv_config.ini')
    runner = CliRunner()
    result = runner.invoke(run_calibration_stats, [testConfigFile,
                                                   '--drainageid', "IN_Hunza"])
    assert_equals(result.exit_code, -1)


def test_run_calibration_stats_good_drainageid():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_modelEnv_config.ini')
    drainageid = "IN_Hunza_at_DainyorBridge"
    runner = CliRunner()
    result = runner.invoke(run_calibration_stats, [
        testConfigFile,
        '-d', drainageid,
        '--header', True])
    # print(result.output, file=sys.stderr)
    assert_equals(result.exit_code, 0)
    assert_equals(re.search('(' + drainageid + ')', result.output).group(1),
                  drainageid)
    assert(re.search(" (2.0000\s+9.0000\s+1.8765) ", result.output).group(1))


def test_run_calibration_stats_ddf():
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_modelEnv_config.ini')
    drainageid = "IN_Hunza_at_DainyorBridge"
    year = 2003
    runner = CliRunner()
    result = runner.invoke(run_calibration_stats, [testConfigFile,
                                                   '--drainageid', drainageid,
                                                   '--header', True,
                                                   '-y', year,
                                                   '--min_snow_ddf', 4.0])
    # print(result.output, file=sys.stderr)
    assert_equals(result.exit_code, 0)
    assert_equals(re.search('(' + drainageid + ')', result.output).group(1),
                  drainageid)
    assert_equals(re.search('(DRAINAGEID)', result.output).group(1),
                  'DRAINAGEID')
    assert_equals(re.search(" (%d) " % year, result.output).group(1),
                  str(year))


def test_run_calibration_stats_with_bogus_et():
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_et_modelEnv_config.ini')
    drainageid = "IN_Hunza_at_DainyorBridge"
    year = 2003
    runner = CliRunner()
    result = runner.invoke(run_calibration_stats, [
        testConfigFile,
        '--drainageid', drainageid,
        '--header', True,
        '-y', year,
        '--min_snow_ddf', 4.0,
        '--rainfall_col', 'bogus',
        '--runoff_col', 'runoff'])
    assert_equals(result.exit_code, -1)


def test_run_calibration_stats_with_et():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_et_modelEnv_config.ini')
    drainageid = "IN_Hunza_at_DainyorBridge"
    year = 2001
    runner = CliRunner()
    result = runner.invoke(run_calibration_stats, [
        testConfigFile,
        '--drainageid', drainageid,
        '--header', True,
        '-y', year,
        '--rainfall_col', 'rainfall-et_km3',
        '--runoff_col', 'runoff'])
    # print(result.output, file=sys.stderr)
    assert_equals(result.exit_code, 0)
    assert_equals(re.search('(' + drainageid + ')', result.output).group(1),
                  drainageid)
    assert_equals(re.search('(MERRA)', result.output).group(1),
                  'MERRA')
    assert_equals(re.search(" (%d) " % year, result.output).group(1),
                  str(year))
    # Check for default nstrikes
    assert_equals(re.search('MODICE\.v0\.4 (.)', result.output).group(1),
                  "1")
    assert(re.search(" (2.0000\s+9.0000\s+0.0409) ", result.output).group(1))


def test_run_calibration_stats_with_3strikes():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_et_modelEnv_config.ini')
    drainageid = "IN_Hunza_at_DainyorBridge"
    year = 2001
    nstrikes = 3
    runner = CliRunner()
    result = runner.invoke(run_calibration_stats, [
        testConfigFile,
        '--drainageid', drainageid,
        '--nstrikes', nstrikes,
        '--header', True,
        '-y', year,
        '--rainfall_col', 'rainfall-et_km3',
        '--runoff_col', 'runoff'])
    assert_equals(result.exit_code, 0)
    assert_equals(re.search('MODICE\.v0\.4 (.)', result.output).group(1),
                  "%d" % nstrikes)


def test_run_calibration_stats_with_fromFile():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_fromFile_config.ini')
    drainageid = "IN_Hunza_at_DainyorBridge"
    year = 2001
    nstrikes = 3
    runner = CliRunner()
    result = runner.invoke(run_calibration_stats, [
        testConfigFile,
        '--drainageid', drainageid,
        '--nstrikes', nstrikes,
        '--header', True,
        '--ablation_method', 'grsize_scag',
        '--use_daily_threshold_file', True,
        '-y', year,
        '--rainfall_col', 'rainfall-et_km3',
        '--runoff_col', 'runoff'])
    assert_equals(result.exit_code, 0)
    assert ' fromFile ' in result.output
