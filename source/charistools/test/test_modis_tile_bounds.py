from __future__ import print_function

import charistools.modis_tile_bounds as mtb

epsilon = 0.001


def test_ul_x_from_h():
    h = 24
    expected = 6671703.118
    answer = mtb.ul_x_from_h(h)
    print(expected, answer)
    assert(answer - expected < epsilon)


def test_ul_y_from_v():
    v = 5
    expected = 4447802.079
    answer = mtb.ul_y_from_v(v)
    print(expected, answer)
    assert(answer - expected < epsilon)


def test_corners_from_label():
    label = 'h23V05'
    expected = ((5559752.598333499, 4447802.0786668),
                (5559752.598333499, 3335851.5590001),
                (6671703.1180002, 3335851.5590001),
                (6671703.1180002, 4447802.0786668))
    answer = mtb.corners_from_label(label)
    assert (expected == answer)


def test_corners_from_h_v():
    h = 23
    v = 5
    expected = ((5559752.598333499, 4447802.0786668),
                (5559752.598333499, 3335851.5590001),
                (6671703.1180002, 3335851.5590001),
                (6671703.1180002, 4447802.0786668))
    answer = mtb.corners_from_h_v(h, v)
    assert (expected == answer)


def test_label_from_h_v():
    h = 24
    v = 5
    expected = 'h24v05'
    answer = mtb.label_from_h_v(h, v)
    assert (expected == answer)


def test_h_v_from_label_lc():
    label = 'h24v05'
    expected = (24, 5)
    answer = mtb.h_v_from_label(label)
    assert (expected == answer)


def test_h_v_from_label_uc():
    label = 'H03V15'
    expected = (3, 15)
    answer = mtb.h_v_from_label(label)
    assert (expected == answer)
