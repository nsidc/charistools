'''
Nose tests for merge_calibration_files.py

'''
from __future__ import print_function

from nose.tools import assert_equals

from charistools.merge_calibration_files import merge_calibration_files
from click.testing import CliRunner
import filecmp
import os


def test_merge_calibration_files_bogus_files():
    runner = CliRunner()
    inFileList = [os.path.join(os.path.dirname(__file__),
                               "test/test_files/bogus1.txt")]
    result = runner.invoke(merge_calibration_files, inFileList)
    assert_equals(result.exit_code, 2)
    assert "Invalid value" in result.output


def test_merge_calibration_files():
    runner = CliRunner()
    inFileList = [os.path.join(os.path.dirname(__file__),
                               "test_files/cal_test1.out"),
                  os.path.join(os.path.dirname(__file__),
                               "test_files/cal_test2.out"),
                  os.path.join(os.path.dirname(__file__),
                               "test_files/cal_test3.out")]
    outfile = os.path.join(os.path.dirname(__file__), "test_cal.out")
    result = runner.invoke(merge_calibration_files,
                           inFileList + [
                               '--outfile', outfile,
                               '-v'])
    assert_equals(result.exit_code, 0)
    assert "Deleting header at position=2" in result.output
    assert "Deleting header at position=5" in result.output
    assert "No header found" in result.output

    comparefile = os.path.join(os.path.dirname(__file__), "test_files/cal_test_compare.out")
    assert filecmp.cmp(outfile, comparefile)

    # Clean up testfile
    os.remove(outfile)
