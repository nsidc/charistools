'''
Nose tests for hypsometry class

To run tests, cd to /vagrant/source and do:
fab [-v] test.all

'''
from __future__ import print_function

import filecmp  # noqa
import matplotlib.pyplot as plt  # noqa
from nose.tools import assert_is, assert_equals  # noqa
from nose.tools import assert_almost_equals  # noqa
from nose.tools import assert_raises  # noqa
import numpy as np  # noqa
import os  # noqa
import pandas as pd  # noqa
import shutil  # noqa
import sys  # noqa

from charistools.hypsometry import Hypsometry  # noqa


def test_init():
    sca = Hypsometry(verbose=True)
    assert_is(len(sca.comments), 0)


def test_init_with_comments():
    sca = Hypsometry(comments=['first', 'second'])
    assert_is(len(sca.comments), 2)


def test_init_from_file():
    sca = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))
    assert_equals(len(sca.comments), 3)
    assert_almost_equals(sca.data.get_value(pd.to_datetime("2001-01-01"), 1500),
                         0.350881, places=6)


def test_init_from_file_no_zero():
    sca = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.no_zero.txt'))
    assert_equals(len(sca.comments), 3)
    assert_almost_equals(sca.data.get_value(pd.to_datetime("2001-01-01"), 1500),
                         0.350881, places=6)


def test_init_from_file_no_decimal():
    sca = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.no_decimal.txt'))
    assert_equals(len(sca.comments), 3)
    assert_almost_equals(sca.data.get_value(pd.to_datetime("2001-01-01"), 1500),
                         0.350881, places=6)


def test_unfurl():
    ddf = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.custom.DDFs.dat'))
    status = ddf.unfurl()
    assert_equals(status, True)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-05-31"), 3000),
                         6.0, places=6)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-08-31"), 3000),
                         5.0, places=6)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-12-31"), 3000),
                         6.0, places=6)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-05-31"), 4400),
                         6.0, places=6)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-08-31"), 4400),
                         5.0, places=6)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-12-31"), 4400),
                         6.0, places=6)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-05-31"), 8700),
                         7.5, places=6)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-07-31"), 8700),
                         4.5, places=6)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-08-31"), 8700),
                         7.5, places=6)
    assert_almost_equals(ddf.data.get_value(pd.to_datetime("2000-12-31"), 8700),
                         7.5, places=6)


def test_get_contour_with_errors():
    ddf = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test_basin.500.ALBEDO_MCD.0035.contour_error.ablation.dat'))
    assert_raises(ValueError,
                  ddf.get_contour)


def test_get_contour():
    ddf = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test_basin.500.ALBEDO_MCD.0035.ablation.dat'))
    assert_equals(ddf.get_contour(), 500)


def test_write():
    sca = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))
    assert_equals(len(sca.comments), 3)

    out_filename = os.path.join(os.path.dirname(__file__),
                                'out.sca_by_elev.txt')
    try:
        os.remove(out_filename)
    except OSError:
        pass

    compare_filename = os.path.join(os.path.dirname(__file__),
                                    'compare',
                                    'compare.sca_by_elev.no_decimal.txt')
    assert_is(sca.write(out_filename), True)
    assert_is(filecmp.cmp(out_filename, compare_filename), True)

    os.remove(out_filename)


def test_write_and_read_empty_data_frame():
    sca = Hypsometry(comments=['first', 'second'])

    out_filename = os.path.join(os.path.dirname(__file__),
                                'out.sca_by_elev.txt')
    try:
        os.remove(out_filename)
    except OSError:
        pass

    assert_is(sca.write(out_filename), True)

    new = Hypsometry(filename=out_filename)
    assert_equals(len(new.comments), 2)
    assert_equals(len(new.data.columns), 0)

    os.remove(out_filename)


def test_compare():
    hyps1 = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))
    hyps2 = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))
    assert_is(hyps1.compare(hyps2), True)


def test_compare_different_comments():
    hyps1 = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))
    hyps1.comments = ['New comment']
    hyps2 = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))
    assert_is(hyps1.compare(hyps2), False)
    assert_is(hyps1.compare(hyps2, ignore_comments=True), True)


def test_compare_different_data():
    hyps1 = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))
    hyps2 = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))

    # Swap the first 2 elements of hyps2
    hyps2.data[[1400, 1500]] = hyps2.data[[1500, 1400]]
    assert_is(hyps1.compare(hyps2), False)


def test_compare_same_data_different_elevations():
    hyps1 = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))
    hyps2 = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))

    # Add an extra elevation in hyps2 with all values set to zeros
    hyps2.data[hyps2.data.columns[0] - 100] = np.zeros(
        hyps2.data.shape[0])

    # Confirm that normal compare still returns false
    assert_is(hyps1.compare(hyps2), False)

    # Confirm that compare for collapse_zero_columns returns true
    assert_is(hyps1.compare(hyps2, collapse_zero_columns=True), True)


def test_hypsometry_write_to_new_dir():
    hyps = Hypsometry(
        filename=os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test.sca_by_elev.txt'))

    # Make sure the output directory doesn't exist
    testdir = os.path.join(os.path.dirname(__file__),
                           'test_files')
    outdir = os.path.join(testdir,
                          'new')
    shutil.rmtree(outdir, ignore_errors=True)
    outfile = os.path.join(outdir, 'by_elev.txt')

    assert_is(hyps.write(outfile), True)


# def test_imshow():
#     hyps = Hypsometry(
#         filename=os.path.join(os.path.dirname(__file__),
#                               'test_files',
#                               'test.sca_by_elev.txt'))
#     title = "Test SCA by elev"
#     fig, axes = plt.subplots(2, 1)
#     ax0 = hyps.imshow(ax=axes[0], title=title, cmap='Greens',
#                       vmin=50.0, vmax=250.0)
#     assert_equals(ax0.get_title(), title)

#     hyps.imshow(ax=axes[1], title=title, cmap='Blues',
#                 vmin=50.0, vmax=250.0)

#     out_filename = os.path.join(os.path.dirname(__file__),
#                                 'sca_by_elev.imshow.png')
#     try:
#         os.remove(out_filename)
#     except OSError:
#         pass

#     fig.tight_layout()
#     fig.savefig(out_filename, dpi=150)

# def test_barplot():
#     hyps = Hypsometry(
#         filename=os.path.join(os.path.dirname(__file__),
#                               'test_files',
#                               'test.sca_by_elev.txt'))
#     title = "Test SCA by elev"
#     fig, ax = plt.subplots()
#     ax = hyps.barplot(ax=ax, index='2001-01-01', title=title, color='g')
#     assert_equals(ax.get_title(), title)

#     out_filename = os.path.join(os.path.dirname(__file__),
#                                 'sca_by_elev.barplot.png')
#     try:
#         os.remove(out_filename)
#     except OSError:
#         pass
#     fig.savefig(out_filename, dpi=150)
