#!/usr/bin/env python
from __future__ import print_function

import datetime as dt  # noqa
from nose.tools import assert_is  # noqa
import os  # noqa
import subprocess  # noqa

import charistools.timeSeries as cts  # noqa


def test_full_date_left_cols():
    testfile = os.path.join(os.path.dirname(__file__),
                            'test_files',
                            'charisTimeseries_testfile1.txt')

    d = cts.TimeSeries(testfile)

    expected_comments = ['# header 1', '# header 2', '# COLUMNS: year month day doy precip_mm']

    assert(d.comments == expected_comments)
    assert(len(d.data) == 4)
    assert(isinstance(d.data.index[0], dt.datetime))


def test_full_date_mixed_cols():
    testfile = os.path.join(os.path.dirname(__file__),
                            'test_files',
                            'charisTimeseries_testfile2.txt')

    d = cts.TimeSeries(testfile)

    expected_comments = \
        ['# header 1', '# header 2', '# COLUMNS: Year dummy Month Day Doy Precip_mm']

    assert(d.comments == expected_comments)
    assert(len(d.data) == 4)
    assert(isinstance(d.data.index[0], dt.datetime))


def test_year_month_date_cols_only():
    testfile = os.path.join(os.path.dirname(__file__),
                            'test_files',
                            'charisTimeseries_testfile3.txt')

    d = cts.TimeSeries(testfile)

    expected_comments = ['# header 1', '# header 2', '# COLUMNS: Year dummy Month Doy Precip_mm']

    assert(d.comments == expected_comments)
    assert(len(d.data) == 4)
    assert(isinstance(d.data.index[0], dt.datetime))


def test_append_comments():
    testfile = os.path.join(os.path.dirname(__file__),
                            'test_files',
                            'charisTimeseries_testfile3.txt')

    d = cts.TimeSeries(testfile)
    d.comments = d.comments + ["New comment line"]

    expected_comments = [
        '# header 1',
        '# header 2',
        '# COLUMNS: Year dummy Month Doy Precip_mm',
        'New comment line']

    assert(d.comments == expected_comments)
    assert(len(d.comments) == 4)


def test_timeSeries_writer():
    testfile = os.path.join(os.path.dirname(__file__),
                            'test_files',
                            'charisTimeseries_testfile1.txt')
    outfile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test.timeSeries_write.txt')

    d = cts.TimeSeries(testfile)

    try:
        os.remove(outfile)
    except OSError:
        pass

    assert_is(d.write(outfile), True)
    assert_is(subprocess.check_call(["diff", testfile, outfile]), 0)
    os.remove(outfile)


if __name__ == '__main__':

    testfile = os.path.join(os.path.dirname(__file__),
                            'test_files',
                            'charisTimeseries_testfile2.txt')
    d = cts.TimeSeries(testfile)

    print("Comments:")
    print(d.comments)

    print("\nData:")
    print(d.as_text())

    print("\nDescribe:")
    print(d.summary())
