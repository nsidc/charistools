'''
Nose tests for charistools convertor utility sum_and_count

To run tests : fab [-v] test.all

'''
from __future__ import print_function

from nose.tools import assert_equals
import numpy as np
import sys


from charistools.convertors import Raster2SumCountByElevation


def test_sum_and_count():

    # Make up some raster data with ones and zeros
    data = np.zeros(shape=(4, 5), dtype='u1')
    data[2:, 1] = 1
    data[1:, 2] = 1
    data[:, 3] = 1

    # with corresponding elevations in the interval [1000,1300]
    elevation = np.reshape(np.arange(0, 20, dtype="i") / 5 * 100. + 1000.,
                           (4, 5))
    elevation[0, :] = 950.

    print("data:", file=sys.stderr)
    print(data, file=sys.stderr)
    print("elevation:", file=sys.stderr)
    print(elevation, file=sys.stderr)

    (my_sum, my_count) = Raster2SumCountByElevation(data, elevation,
                                                    min_contour_m=800.)

    print("my_sum:", file=sys.stderr)
    print(my_sum, file=sys.stderr)
    print("my_count:", file=sys.stderr)
    print(my_count, file=sys.stderr)

    assert_equals(len(my_sum), 6)
    assert_equals(len(my_count), 6)
    assert_equals(my_sum[800.0][0], 0)
    assert_equals(my_sum[900.0][0], 1)
    assert_equals(my_sum[1000.0][0], 0)
    assert_equals(my_sum[1100.0][0], 2)
    assert_equals(my_sum[1200.0][0], 3)
    assert_equals(my_sum[1300.0][0], 3)
    assert_equals(my_count[800.0][0], 0)
    assert_equals(my_count[900.0][0], 5)
    assert_equals(my_count[1000.0][0], 0)
    assert_equals(my_count[1100.0][0], 5)
    assert_equals(my_count[1200.0][0], 5)
    assert_equals(my_count[1300.0][0], 5)


def test_sum_and_count_with_values_off_contour():

    # Make up some raster data with ones and zeros
    data = np.zeros(shape=(4, 5), dtype='u1')
    data[2:, 1] = 1
    data[1:, 2] = 1
    data[:, 3] = 1

    # with corresponding elevations in the interval [1000,1300]
    elevation = np.reshape(np.arange(0, 20, dtype="i") / 5 * 100. + 1000.,
                           (4, 5))
    elevation[0, :] = 950.
    elevation[3, :] = elevation[3, :] + 1.

    print("data:", file=sys.stderr)
    print(data, file=sys.stderr)
    print("elevation:", file=sys.stderr)
    print(elevation, file=sys.stderr)

    (my_sum, my_count) = Raster2SumCountByElevation(data, elevation,
                                                    min_contour_m=800.)

    print("my_sum:", file=sys.stderr)
    print(my_sum, file=sys.stderr)
    print("my_count:", file=sys.stderr)
    print(my_count, file=sys.stderr)

    assert_equals(len(my_sum), 6)
    assert_equals(len(my_count), 6)
    assert_equals(my_sum[800.0][0], 0)
    assert_equals(my_sum[900.0][0], 1)
    assert_equals(my_sum[1000.0][0], 0)
    assert_equals(my_sum[1100.0][0], 2)
    assert_equals(my_sum[1200.0][0], 3)
    assert_equals(my_sum[1300.0][0], 3)
    assert_equals(my_count[800.0][0], 0)
    assert_equals(my_count[900.0][0], 5)
    assert_equals(my_count[1000.0][0], 0)
    assert_equals(my_count[1100.0][0], 5)
    assert_equals(my_count[1200.0][0], 5)
    assert_equals(my_count[1300.0][0], 5)


def test_sum_and_count_with_mask():

    # Make up some raster data with ones and zeros
    data = np.zeros(shape=(4, 5), dtype='u1')
    data[2:, 1] = 1
    data[1:, 2] = 1
    data[:, 3] = 1

    # with corresponding elevations in the interval [1000,1300]
    elevation = np.reshape(np.arange(0, 20, dtype="i") / 5 * 100. + 1000.,
                           (4, 5))
    elevation[0, :] = 950.
    elevation[:, 3:] = np.nan

    print("data:", file=sys.stderr)
    print(data, file=sys.stderr)
    print("elevation:", file=sys.stderr)
    print(elevation, file=sys.stderr)

    (my_sum, my_count) = Raster2SumCountByElevation(data, elevation,
                                                    min_contour_m=800.)

    print("my_sum:", file=sys.stderr)
    print(my_sum, file=sys.stderr)
    print("my_count:", file=sys.stderr)
    print(my_count, file=sys.stderr)

    assert_equals(len(my_sum), 6)
    assert_equals(len(my_count), 6)
    assert_equals(my_sum[800.0][0], 0)
    assert_equals(my_sum[900.0][0], 0)
    assert_equals(my_sum[1000.0][0], 0)
    assert_equals(my_sum[1100.0][0], 1)
    assert_equals(my_sum[1200.0][0], 2)
    assert_equals(my_sum[1300.0][0], 2)
    assert_equals(my_count[800.0][0], 0)
    assert_equals(my_count[900.0][0], 3)
    assert_equals(my_count[1000.0][0], 0)
    assert_equals(my_count[1100.0][0], 3)
    assert_equals(my_count[1200.0][0], 3)
    assert_equals(my_count[1300.0][0], 3)
