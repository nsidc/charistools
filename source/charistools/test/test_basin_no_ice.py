'''
Nose tests for charistools temperature-index melt model

To run tests : fab [-v] test.all

'''
from __future__ import print_function

import matplotlib  # noqa
# matplotlib.use('Agg')
from nose.tools import assert_equals  # noqa
from nose.tools import assert_almost_equals  # noqa
from nose.tools import assert_raises  # noqa
import numpy as np  # noqa
import os  # noqa
import pandas as pd  # noqa
import sys  # noqa

from charistools.hypsometry import Hypsometry  # noqa
from charistools.modelEnv import ModelEnv  # noqa
from charistools.meltModels import _rmse  # noqa
from charistools.meltModels import _seasonal_ddfs  # noqa
from charistools.meltModels import _volumetric_difference_pcent  # noqa
from charistools.meltModels import CalibrateTriSurfTempIndexMelt  # noqa
from charistools.meltModels import TempIndexMelt  # noqa
from charistools.meltModels import TriSurfTempIndexMelt  # noqa

calibrationConfigFile = os.path.join(os.path.dirname(__file__),
                                     'calibration_modelEnv_config.ini')


def test_tri_surface_model():

    # Define test input files and read hypsometries for
    # SOL, SOI, EGI, temperatures
    SOLFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
    SOIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.no_snow_on_ice.dat')
    EGIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.no_exposed_glacier_ice.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_basin.500.temperature.dat')

    (SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps) = TriSurfTempIndexMelt(
        SOLFile=SOLFile,
        SOIFile=SOIFile,
        EGIFile=EGIFile,
        temperatureFile=temperatureFile,
        min_snow_ddf=2.,
        max_snow_ddf=7.,
        min_ice_ddf=2.,
        max_ice_ddf=9.)

    # Compare to previously-produced output
    SOLCompareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.correct_sDDF.ALBEDO_MCD.0035.snow_off_ice_melt.dat')
    SOL_compare_hyps = Hypsometry(filename=SOLCompareFile)

    tolerance = 1E-5

    diff = SOL_melt_hyps.data.values - SOL_compare_hyps.data.values
    assert(np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)

    assert(SOI_melt_hyps.data.empty)
    assert(EGI_melt_hyps.data.empty)
