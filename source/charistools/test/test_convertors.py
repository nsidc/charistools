'''
Nose tests for charistools convertors utilities

To run tests : fab [-v] test.all

'''
from __future__ import print_function

from nose.tools import assert_is, assert_almost_equals, assert_raises
from nose.tools import assert_equals
import os
import sys


from charistools.convertors import Csv2Hypsometry
from charistools.convertors import Dem2Hypsometry
from charistools.convertors import Fsca2Hypsometry
from charistools.convertors import Modice2Hypsometry
from charistools.convertors import Temperature2Hypsometry
from charistools.convertors import ET2Hypsometry
from charistools.hypsometry import Hypsometry
from charistools.modelEnv import ModelEnv


def test_csv2hypsometry_wrong_cols():
    csv_filename = os.path.join(os.path.dirname(__file__),
                                'test_files',
                                'Kta_hypso_100m.3col.csv')
    hyps_filename = os.path.join(os.path.dirname(__file__),
                                 'Kta_hypso_100m.txt')
    assert_raises(IndexError,
                  Csv2Hypsometry, csv_filename, hyps_filename)


def test_csv2hypsometry():
    csv_filename = os.path.join(os.path.dirname(__file__),
                                'test_files',
                                'Kta_hypso_100m.csv')
    hyps_filename = os.path.join(os.path.dirname(__file__),
                                 'Kta_hypso_100m.txt')
    try:
        os.remove(hyps_filename)
    except OSError:
        pass

    assert_is(Csv2Hypsometry(csv_filename, hyps_filename, comments=['test comment']),
              True)

    hyps = Hypsometry(hyps_filename)
    assert_almost_equals(hyps.data[500][0], 85., places=3)

    # Clean up the test file
    os.remove(hyps_filename)


def test_init_modice_for_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    nstrikes = 1
    by_elev = Modice2Hypsometry(
        'IN_Hunza_at_Danyour',
        modelEnv,
        modice_nstrikes=nstrikes)

    assert_equals(len(by_elev.data.index), 1,
                  "Expected to have 1 modice_by_elev row.")
    # by_elev.print()

    # Open comparison hypsometry created in IDL
    # Data from IDL has string floats for columns and
    # data are rounded to nearest 0.01 km^2
    orig_filename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        "IN_Hunza_at_Danyour.0100m."
        "modicev04_%1dstrike_area_by_elev.txt" % nstrikes)
    orig = Hypsometry(filename=orig_filename)
    by_elev.data = by_elev.data.round(2)
    assert_is(by_elev.compare(orig, ignore_comments=True),
              True)


def test_init_temperature_for_bogus_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    assert_raises(RuntimeError, Temperature2Hypsometry,
                  drainageID='bogus_drainage',
                  year=2001,
                  modelEnv=modelEnv)


def test_init_temperature_for_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    by_elev = Temperature2Hypsometry(
        drainageID='IN_Hunza_at_Danyour',
        year=2001,
        modelEnv=modelEnv,
        stop_doy=3)
    assert_equals(len(by_elev.data.index), 3,
                  "Expected to have 3 by_elev rows.")
    # by_elev.print()
    assert_almost_equals(by_elev.data[1400]['2001-01-01'], 10.7, places=1)
    assert_almost_equals(by_elev.data[1400]['2001-01-02'], 9.8, places=1)
    assert_almost_equals(by_elev.data[1400]['2001-01-03'], 7.6, places=1)
    assert_almost_equals(by_elev.data[7700]['2001-01-01'], -35.4, places=1)
    assert_almost_equals(by_elev.data[7700]['2001-01-02'], -34.6, places=1)
    assert_almost_equals(by_elev.data[7700]['2001-01-03'], -39.4, places=1)


def test_init_bogus_area_type():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    assert_raises(ValueError, Fsca2Hypsometry,
                  'IN_Hunza_at_Danyour',
                  2001,
                  fsca_area='bogus',
                  modelEnv=modelEnv,
                  stop_doy=3)


def test_init_scag_fsca_total_for_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    by_elev = Fsca2Hypsometry('IN_Hunza_at_Danyour', 2001, modelEnv=modelEnv,
                              fsca_type='modscag_gf',
                              stop_doy=3)
    assert_equals(len(by_elev["total"].data.index), 3,
                  "Expected to have 3 by_elev rows.")

    assert_almost_equals(by_elev["total"].data[6900]['2001-01-01'], 11.973661, places=5)
    assert_almost_equals(by_elev["total"].data[6900]['2001-01-02'], 11.829840, places=5)
    assert_almost_equals(by_elev["total"].data[6900]['2001-01-03'], 11.784761, places=5)


def test_init_mod10_fsca_total_for_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    by_elev = Fsca2Hypsometry('IN_Hunza_at_Danyour', 2001, modelEnv=modelEnv,
                              fsca_type='mod10a1_gf',
                              stop_doy=3)
    assert_equals(len(by_elev["total"].data.index), 3,
                  "Expected to have 3 by_elev rows.")

    assert_almost_equals(by_elev["total"].data[6900]['2001-01-01'], 14.107368, places=5)
    assert_almost_equals(by_elev["total"].data[6900]['2001-01-02'], 14.167472, places=5)
    assert_almost_equals(by_elev["total"].data[6900]['2001-01-03'], 14.221137, places=5)


def test_init_snow_on_land_for_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    testFilename = "SOL_by_elev.2001.0100m.txt"
    try:
        os.remove(testFilename)
    except OSError:
        pass

    by_elev = Fsca2Hypsometry(
        'IN_Hunza_at_Danyour', 2001,
        fsca_type='modscag_gf',
        fsca_area='SOL',
        modelEnv=modelEnv, stop_doy=3,
        outfile=testFilename,
        decimal_places=2)

    assert_equals(len(by_elev["SOL"].data.index), 3,
                  "Expected to have 3 by_elev rows.")

    # Read the test back to get the floatingpoint values to the same
    # decimal_places
    test_by_elev = Hypsometry(filename=testFilename)
    compareFilename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'IN_Hunza_at_Danyour.2001.0100m.GRSIZE_SCAG.0200.snow_off_ice_area.txt')
    compare_by_elev = Hypsometry(filename=compareFilename)

    # Ignore comments and confirm the rest match what we've produced before
    assert_is(test_by_elev.compare(other=compare_by_elev, ignore_comments=True),
              True)

    # Clean up the test file
    os.remove(testFilename)


def test_init_on_ice_by_grsize_for_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    by_elev = Fsca2Hypsometry(
        'IN_Hunza_at_Danyour', 2001,
        fsca_type='modscag_gf',
        fsca_area='SOI/EGI',
        ablation_method='grsize_scag',
        threshold=200,
        modelEnv=modelEnv, stop_doy=3)
    SOItestFilename = "SOI_by_elev.grsize.2001.0100m.txt"
    EGItestFilename = "EGI_by_elev.grsize.2001.0100m.txt"
    try:
        os.remove(SOItestFilename)
        os.remove(EGItestFilename)
    except OSError:
        pass

    by_elev["SOI"].write(SOItestFilename, decimal_places=2)
    by_elev["EGI"].write(EGItestFilename, decimal_places=2)
    assert_equals(len(by_elev["SOI"].data.index), 3,
                  "Expected to have 3 by_elev rows.")
    assert_equals(len(by_elev["EGI"].data.index), 3,
                  "Expected to have 3 by_elev rows.")

    # SOI: Read the test back to get the floatingpoint values to the same
    # decimal_places
    test_by_elev = Hypsometry(filename=SOItestFilename)
    compareFilename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.SOI_by_elev.grsize.2001.0100m.txt')
    compare_by_elev = Hypsometry(filename=compareFilename)

    # Ignore comments and confirm the rest match what we've produced before
    assert_is(test_by_elev.compare(other=compare_by_elev, ignore_comments=True),
              True)

    # EGI: Read the test back to get the floatingpoint values to the same
    # decimal_places
    test_by_elev = Hypsometry(filename=EGItestFilename)
    compareFilename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.EGI_by_elev.grsize.2001.0100m.txt')
    compare_by_elev = Hypsometry(filename=compareFilename)

    # Ignore comments and confirm the rest match what we've produced before
    assert_is(test_by_elev.compare(other=compare_by_elev, ignore_comments=True),
              True)

    # Clean up the test files
    os.remove(SOItestFilename)
    os.remove(EGItestFilename)


def test_init_on_ice_by_daily_threshold():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    by_elev = Fsca2Hypsometry(
        'IN_Hunza_at_Danyour', 2001,
        fsca_type='modscag_gf',
        fsca_area='SOI/EGI',
        ablation_method='grsize_scag',
        use_daily_threshold_file=True,
        modelEnv=modelEnv, stop_doy=3)
    SOItestFilename = "SOI_by_elev.grsize.2001.0100m.txt"
    EGItestFilename = "EGI_by_elev.grsize.2001.0100m.txt"
    try:
        os.remove(SOItestFilename)
        os.remove(EGItestFilename)
    except OSError:
        pass

    by_elev["SOI"].write(SOItestFilename, decimal_places=2)
    by_elev["EGI"].write(EGItestFilename, decimal_places=2)
    assert_equals(len(by_elev["SOI"].data.index), 3,
                  "Expected to have 3 by_elev rows.")
    assert_equals(len(by_elev["EGI"].data.index), 3,
                  "Expected to have 3 by_elev rows.")

    # SOI: Read the test back to get the floatingpoint values to the same
    # decimal_places
    test_by_elev = Hypsometry(filename=SOItestFilename)
    compareFilename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.SOI_by_elev.grsize.varThresh.2001.0100m.txt')
    compare_by_elev = Hypsometry(filename=compareFilename)

    # Ignore comments and confirm the rest match what we've produced before
    assert_is(test_by_elev.compare(other=compare_by_elev, ignore_comments=True),
              True)

    # EGI: Read the test back to get the floatingpoint values to the same
    # decimal_places
    test_by_elev = Hypsometry(filename=EGItestFilename)
    compareFilename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.EGI_by_elev.grsize.varThresh.2001.0100m.txt')
    compare_by_elev = Hypsometry(filename=compareFilename)

    # Ignore comments and confirm the rest match what we've produced before
    assert_is(test_by_elev.compare(other=compare_by_elev, ignore_comments=True),
              True)

    # Clean up the test files
    os.remove(SOItestFilename)
    os.remove(EGItestFilename)


def test_init_on_ice_by_albedo_for_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    by_elev = Fsca2Hypsometry(
        'IN_Hunza_at_Danyour', 2001,
        fsca_type='modscag_gf',
        fsca_area='SOI/EGI',
        ablation_method='albedo_mod10a1',
        threshold=0.46,
        modelEnv=modelEnv, stop_doy=3)
    SOItestFilename = "SOI_by_elev.albedo.2001.0100m.txt"
    EGItestFilename = "EGI_by_elev.albedo.2001.0100m.txt"
    try:
        os.remove(SOItestFilename)
        os.remove(EGItestFilename)
    except OSError:
        pass

    by_elev["SOI"].write(SOItestFilename, decimal_places=2)
    by_elev["EGI"].write(EGItestFilename, decimal_places=2)
    assert_equals(len(by_elev["SOI"].data.index), 3,
                  "Expected to have 3 by_elev rows.")
    assert_equals(len(by_elev["EGI"].data.index), 3,
                  "Expected to have 3 by_elev rows.")

    # Read the test back to get the floatingpoint values to the same
    # decimal_places
    test_by_elev = Hypsometry(filename=SOItestFilename)
    compareFilename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.SOI_by_elev.albedo.2001.0100m.txt')
    compare_by_elev = Hypsometry(filename=compareFilename)

    # Ignore comments and confirm the rest match what we've produced before
    assert_is(test_by_elev.compare(
        other=compare_by_elev,
        collapse_zero_columns=True,
        ignore_comments=True),
              True)

    # Read the test back to get the floatingpoint values to the same
    # decimal_places
    test_by_elev = Hypsometry(filename=EGItestFilename)
    compareFilename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.EGI_by_elev.albedo.2001.0100m.txt')
    compare_by_elev = Hypsometry(filename=compareFilename)

    # Ignore comments and confirm the rest match what we've produced before
    assert_is(test_by_elev.compare(
        other=compare_by_elev,
        collapse_zero_columns=True,
        ignore_comments=True),
              True)

    # Clean up the test file
    os.remove(SOItestFilename)
    os.remove(EGItestFilename)


def test_init_dem_for_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    by_elev = Dem2Hypsometry(
        'IN_Hunza_at_Danyour',
        modelEnv)

    assert_equals(len(by_elev.data.index), 1,
                  "Expected to have 1 by_elev row.")
    # by_elev.print()

    # Open comparison hypsometry created in IDL
    # Data from IDL has string floats for columns and
    # data are rounded to nearest 0.01 km^2
    orig_filename = os.path.join(
        os.path.dirname(__file__),
        'compare',
        "IN_Hunza_at_Danyour.0100m.area_by_elev.txt")
    orig = Hypsometry(filename=orig_filename)
    by_elev.data = by_elev.data.round(2)

    assert_is(by_elev.compare(orig, ignore_comments=True),
              True)


def test_et_for_drainage_bogus_dates():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile,
                        topDir='/projects/CHARIS/charistools_test_data')

    with assert_raises(ValueError) as cm:
        by_elev = ET2Hypsometry(drainageID='IN_Hunza_at_Danyour',
                                start_yyyymm=200301,
                                stop_yyyymm=200103,
                                modelEnv=modelEnv)
    print("Exception:", file=sys.stderr)
    print(cm.exception, file=sys.stderr)
    if not cm.exception:
        by_elev.print()


def test_et_for_drainage():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_modelEnv_config.ini')
    modelEnv = ModelEnv(tileConfigFile=testConfigFile)

    by_elev = ET2Hypsometry(drainageID='IN_Hunza_at_Danyour',
                            start_yyyymm=200101,
                            stop_yyyymm=200102,
                            modelEnv=modelEnv)
    assert_equals(len(by_elev.data.index), 2,
                  "Expected to have 2 by_elev rows.")
    assert_almost_equals(by_elev.data[2100]['2001-02-01'], 0.000611,
                         places=6)
