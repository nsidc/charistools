'''
Nose tests for charistools temperature-index melt model

To run tests : fab [-v] test.all

'''
from __future__ import print_function

import matplotlib  # noqa
from nose.tools import assert_equals  # noqa
from nose.tools import assert_almost_equals  # noqa
from nose.tools import assert_raises  # noqa
from nose.tools import assert_true  # noqa
import numpy as np  # noqa
import os  # noqa
import pandas as pd  # noqa
import sys  # noqa

from charistools.hypsometry import Hypsometry  # noqa
from charistools.modelEnv import ModelEnv  # noqa
from charistools.meltModels import _rmse  # noqa
from charistools.meltModels import _seasonal_ddfs  # noqa
from charistools.meltModels import _custom_ddfs  # noqa
from charistools.meltModels import _volumetric_difference_pcent  # noqa
from charistools.meltModels import CalibrateTriSurfTempIndexMelt  # noqa
from charistools.meltModels import CalibrationCost  # noqa
from charistools.meltModels import CalibrationStats  # noqa
from charistools.meltModels import SaveCalibrationStats  # noqa
from charistools.meltModels import RandomNewDDFs  # noqa
from charistools.meltModels import TempIndexMelt  # noqa
from charistools.meltModels import TriSurfTempIndexMelt  # noqa
# from charistools.meltModels import PlotTriSurfMelt  # noqa
# from charistools.meltModels import ImshowTriSurfMelt  # noqa


calibrationConfigFile = os.path.join(os.path.dirname(__file__),
                                     'calibration_modelEnv_config.ini')


def test_seasonal_ddf():

    SOLFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
    SOL_hyps = Hypsometry(filename=SOLFile)
    ddfs = _seasonal_ddfs(date_index=SOL_hyps.data.index)
    assert_almost_equals(ddfs['2001-04-01'], 4.970567, places=6)
    assert_almost_equals(ddfs['2001-05-01'], 6.121574, places=6)
    assert_almost_equals(ddfs['2001-06-01'], 6.863991, places=6)
    assert_almost_equals(ddfs['2001-07-01'], 6.957316, places=6)
    assert_almost_equals(ddfs['2001-08-01'], 6.381667, places=6)
    assert_almost_equals(ddfs['2001-09-01'], 5.282768, places=6)


def test_custom_ddf():

    DDFFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.0500m.custom.iceDDFs.dat')
    ddf_hyps = _custom_ddfs(filename=DDFFile)
    assert_almost_equals(ddf_hyps.data.get_value(
        pd.to_datetime("2000-04-01"), 4000), 8.0, places=6)
    assert_almost_equals(ddf_hyps.data.get_value(
        pd.to_datetime("2000-06-01"), 4000), 7.0, places=6)
    assert_almost_equals(ddf_hyps.data.get_value(
        pd.to_datetime("2000-04-01"), 5000), 9.5, places=6)
    assert_almost_equals(ddf_hyps.data.get_value(
        pd.to_datetime("2000-08-01"), 5000), 9.5, places=6)


def test_ti_model():

    # Define input area-by-elevation files and read hypsometries for
    # surface of snow-on-land and temperatures
    areaFile = os.path.join(os.path.dirname(__file__),
                            'test_files',
                            'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_basin.500.temperature.dat')
    area_hyps = Hypsometry(filename=areaFile)
    t_hyps = Hypsometry(filename=temperatureFile)

    melt_hyps = TempIndexMelt(
        area_hyps=area_hyps,
        temperature_hyps=t_hyps,
        min_ddf=2.,
        max_ddf=7.)

    # Compare to previously-produced output
    compareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.correct_sDDF.ALBEDO_MCD.0035.snow_off_ice_melt.dat')
    compare_hyps = Hypsometry(filename=compareFile)

    tolerance = 1E-5

    diff = melt_hyps.data.values - compare_hyps.data.values
    assert(np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)


def test_ti_model_custom_ddf():

    # Define input area-by-elevation files and read hypsometries for
    # surface of snow-on-land and temperatures
    areaFile = os.path.join(os.path.dirname(__file__),
                            'test_files',
                            'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_basin.500.temperature.dat')
    area_hyps = Hypsometry(filename=areaFile)
    t_hyps = Hypsometry(filename=temperatureFile)
    DDFFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.0500m.custom.iceDDFs.dat')
    ddf_hyps = _custom_ddfs(filename=DDFFile, contour_m=500.)

    melt_hyps = TempIndexMelt(
        area_hyps=area_hyps,
        temperature_hyps=t_hyps,
        custom_ddf_hyps=ddf_hyps)

    # Remove rows with all NaNs
    # Remove rows/cols with all zeroes
    # on the ice_melt array
    melt_hyps.data.dropna(axis=0, how='all', inplace=True)

    melt_hyps.data = melt_hyps.data.loc[
        (melt_hyps.data != 0.).any(axis=1),
        (melt_hyps.data != 0.).any(axis=0)]

    # Compare to previously-produced output
    compareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.custom_ddfs.dat')
    compare_hyps = Hypsometry(filename=compareFile)

    tolerance = 1E-5

    diff = melt_hyps.data.values - compare_hyps.data.values
    assert_true(
        np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)


def test_ti_model_differing_columns():

    # Define input area-by-elevation files and read hypsometries for
    # surface of snow-on-land and temperatures
    areaFile = os.path.join(os.path.dirname(__file__),
                            'test_files',
                            'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_basin.500.temperature.dat')
    area_hyps = Hypsometry(filename=areaFile)
    t_hyps = Hypsometry(filename=temperatureFile)

    # Remove some columns from input area
    area_hyps.data = area_hyps.data.drop([4500, 5000], axis=1)

    melt_hyps = TempIndexMelt(
        area_hyps=area_hyps,
        temperature_hyps=t_hyps,
        min_ddf=2.,
        max_ddf=7.)

    # Compare to previously-produced output
    compareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.correct_sDDF.ALBEDO_MCD.0035.snow_off_ice_melt.partial.dat')
    compare_hyps = Hypsometry(filename=compareFile)

    tolerance = 1E-5

    diff = melt_hyps.data.values - compare_hyps.data.values
    assert(np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)


def test_tri_surface_model():

    # Define test input files and read hypsometries for
    # SOL, SOI, EGI, temperatures
    SOLFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
    SOIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.snow_on_ice.dat')
    EGIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.ablation.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_basin.500.temperature.dat')

    (SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps) = TriSurfTempIndexMelt(
        SOLFile=SOLFile,
        SOIFile=SOIFile,
        EGIFile=EGIFile,
        temperatureFile=temperatureFile,
        min_snow_ddf=2.,
        max_snow_ddf=7.,
        min_ice_ddf=2.,
        max_ice_ddf=9.)

    # Compare to previously-produced output
    SOLCompareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.correct_sDDF.ALBEDO_MCD.0035.snow_off_ice_melt.dat')
    SOICompareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.correct_sDDF.ALBEDO_MCD.0035.snow_on_ice_melt.dat')
    EGICompareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.correct_sDDF.ALBEDO_MCD.0035.ice_melt.dat')
    SOL_compare_hyps = Hypsometry(filename=SOLCompareFile)
    SOI_compare_hyps = Hypsometry(filename=SOICompareFile)
    EGI_compare_hyps = Hypsometry(filename=EGICompareFile)

    tolerance = 1E-5

    diff = SOL_melt_hyps.data.values - SOL_compare_hyps.data.values
    assert(np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)

    diff = SOI_melt_hyps.data.values - SOI_compare_hyps.data.values
    assert(np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)

    diff = EGI_melt_hyps.data.values - EGI_compare_hyps.data.values
    assert(np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)


def test_tri_surface_model_custom_ddf():

    # Define test input files and read hypsometries for
    # SOL, SOI, EGI, temperatures
    SOLFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
    SOIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.snow_on_ice.dat')
    EGIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_basin.500.ALBEDO_MCD.0035.ablation.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_basin.500.temperature.dat')
    snow_DDFFile = os.path.join(os.path.dirname(__file__),
                                'test_files',
                                'test_basin.0500m.custom.snowDDFs.dat')
    ice_DDFFile = os.path.join(os.path.dirname(__file__),
                               'test_files',
                               'test_basin.0500m.custom.iceDDFs.dat')

    (SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps) = TriSurfTempIndexMelt(
        SOLFile=SOLFile,
        SOIFile=SOIFile,
        EGIFile=EGIFile,
        temperatureFile=temperatureFile,
        customSnowDDFFile=snow_DDFFile,
        customIceDDFFile=ice_DDFFile)

    # Remove rows with all NaNs
    # Remove rows/cols with all zeroes
    SOL_melt_hyps.data.dropna(axis=0, how='all', inplace=True)
    SOL_melt_hyps.data = SOL_melt_hyps.data.loc[
        (SOL_melt_hyps.data != 0.).any(axis=1),
        (SOL_melt_hyps.data != 0.).any(axis=0)]
    SOI_melt_hyps.data.dropna(axis=0, how='all', inplace=True)
    SOI_melt_hyps.data = SOI_melt_hyps.data.loc[
        (SOI_melt_hyps.data != 0.).any(axis=1),
        (SOI_melt_hyps.data != 0.).any(axis=0)]
    EGI_melt_hyps.data.dropna(axis=0, how='all', inplace=True)
    EGI_melt_hyps.data = EGI_melt_hyps.data.loc[
        (EGI_melt_hyps.data != 0.).any(axis=1),
        (EGI_melt_hyps.data != 0.).any(axis=0)]

    # Compare to previously-produced output
    SOLCompareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.customDDF.sol_melt.dat')
    SOICompareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.customDDF.soi_melt.dat')
    EGICompareFile = os.path.join(
        os.path.dirname(__file__),
        'compare',
        'compare.test_basin.500.customDDF.egi_melt.dat')
    SOL_compare_hyps = Hypsometry(filename=SOLCompareFile)
    SOI_compare_hyps = Hypsometry(filename=SOICompareFile)
    EGI_compare_hyps = Hypsometry(filename=EGICompareFile)

    tolerance = 1E-5

    diff = SOL_melt_hyps.data.values - SOL_compare_hyps.data.values
    assert(np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)

    diff = SOI_melt_hyps.data.values - SOI_compare_hyps.data.values
    assert(np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)

    diff = EGI_melt_hyps.data.values - EGI_compare_hyps.data.values
    assert(np.fabs(np.amin(diff)) < tolerance and np.fabs(np.amax(diff)) < tolerance)


def test_tri_surf_bug():

    configFile = os.path.join(os.path.dirname(__file__),
                              'trisurf_bug_config.ini')

    myEnv = ModelEnv(tileConfigFile=configFile)
    drainageID = 'GA_Langtang_at_Kyanjin'

    # Define test input files and read hypsometries for SOL, SOI, EGI, temperatures
    SOLFile = myEnv.hypsometry_filename(type='snow_on_land_by_elevation',
                                        drainageID=drainageID,
                                        year=2013,
                                        modice_nstrikes=3)
    SOIFile = myEnv.hypsometry_filename(type='snow_on_ice_by_elevation',
                                        drainageID=drainageID,
                                        year=2013,
                                        ablation_method='grsize_scag',
                                        threshold='fromFile',
                                        modice_nstrikes=3)
    EGIFile = myEnv.hypsometry_filename(type='exposed_glacier_ice_by_elevation',
                                        drainageID=drainageID,
                                        year=2013,
                                        ablation_method='grsize_scag',
                                        threshold='fromFile',
                                        modice_nstrikes=3)
    temperatureFile = myEnv.hypsometry_filename(type='temperature_by_elevation',
                                                drainageID=drainageID,
                                                year=2013)
    snow_DDFFile = myEnv.hypsometry_filename(type='custom_snow_ddf',
                                             drainageID=drainageID)
    ice_DDFFile = myEnv.hypsometry_filename(type='custom_ice_ddf',
                                            drainageID=drainageID)

    EGI_hyps = Hypsometry(filename=EGIFile)
    temp_hyps = Hypsometry(filename=temperatureFile)
    custom_hyps = Hypsometry(filename=ice_DDFFile)
    Hypsometry.unfurl(custom_hyps)

    hyps = TempIndexMelt(EGI_hyps, temp_hyps, custom_ddf_hyps=custom_hyps)
    expected_value_0620 = 0.0000172786
    assert_almost_equals(
        hyps.data.get_value(pd.to_datetime("2013-06-20"), 5500),
        expected_value_0620, places=10)

    (SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps) = TriSurfTempIndexMelt(
        SOLFile=SOLFile,
        SOIFile=SOIFile,
        EGIFile=EGIFile,
        temperatureFile=temperatureFile,
        customSnowDDFFile=snow_DDFFile,
        customIceDDFFile=ice_DDFFile)

    assert_almost_equals(
        EGI_melt_hyps.data.get_value(pd.to_datetime("2013-06-20"), 5500),
        expected_value_0620, places=10)


def test_rmse_with_nans():
    data = np.arange(15).reshape((5, 3))
    df = pd.DataFrame(data=data,
                      columns=['test', 'melt+rainfall', 'runoff'])
    df['runoff'][3] = float('NaN')
    assert_equals(_rmse(df), 1.)


def test_rmse():
    data = np.arange(15).reshape((5, 3))
    df = pd.DataFrame(data=data,
                      columns=['test', 'melt+rainfall', 'runoff'])
    assert_equals(_rmse(df), 1.)


def test_volumetric_difference_pcent():
    data = np.arange(15).reshape((5, 3))
    df = pd.DataFrame(data=data,
                      columns=['test', 'melt+rainfall', 'runoff'])
    assert_equals(_volumetric_difference_pcent(df), 12.5)


def test_calibration_bad_input_file():

    # Define test input files and read hypsometries for
    # SOL, SOI, EGI, temperatures
    # Give a bad SOLFile name
    SOLFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_skew_basin.500.csv')
    SOIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_skew_basin.500.snow_on_ice.dat')
    EGIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_skew_basin.500.exposed_glacier_ice.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_basin.500.temperature.dat')
    rainfallFile = os.path.join(os.path.dirname(__file__),
                                'test_files',
                                'test_skew_basin.500.rainfall.dat')
    runoffFile = os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test_skew_basin.500.runoff.dat')

    assert_raises(IOError,
                  CalibrateTriSurfTempIndexMelt,
                  SOLFile=SOLFile,
                  SOIFile=SOIFile,
                  EGIFile=EGIFile,
                  temperatureFile=temperatureFile,
                  rainfallFile=rainfallFile,
                  runoffFile=runoffFile)


def test_calibration_bad_rainfall_file():

    # Define test input files and read hypsometries for
    # SOL, SOI, EGI, temperatures
    # Give a bad rainfall name
    SOLFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_skew_basin.500.snow_on_land.dat')
    SOIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_skew_basin.500.snow_on_ice.dat')
    EGIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_skew_basin.500.exposed_glacier_ice.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_skew_basin.500.temperature.dat')
    rainfallFile = os.path.join(os.path.dirname(__file__),
                                'test_files',
                                'bogus.dat')
    runoffFile = os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test_skew_basin.500.runoff.dat')

    # Define test input files and read hypsometries for
    # SOL, SOI, EGI, temperatures
    # Give a bad rainfallFile name
    assert_raises(IOError,
                  CalibrateTriSurfTempIndexMelt,
                  SOLFile=SOLFile,
                  SOIFile=SOIFile,
                  EGIFile=EGIFile,
                  temperatureFile=temperatureFile,
                  rainfallFile=rainfallFile,
                  runoffFile=runoffFile)


def test_calibration():

    # Define test input files and read hypsometries for
    # SOL, SOI, EGI, temperatures
    SOLFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_skew_basin.500.snow_on_land.dat')
    SOIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_skew_basin.500.snow_on_ice.dat')
    EGIFile = os.path.join(os.path.dirname(__file__),
                           'test_files',
                           'test_skew_basin.500.exposed_glacier_ice.dat')
    temperatureFile = os.path.join(os.path.dirname(__file__),
                                   'test_files',
                                   'test_skew_basin.500.temperature.dat')
    rainfallFile = os.path.join(os.path.dirname(__file__),
                                'test_files',
                                'test_skew_basin.500.rainfall.dat')
    runoffFile = os.path.join(os.path.dirname(__file__),
                              'test_files',
                              'test_skew_basin.500.runoff.dat')

    # Define test input files and read hypsometries for
    # SOL, SOI, EGI, temperatures
    result = CalibrateTriSurfTempIndexMelt(
        SOLFile=SOLFile,
        SOIFile=SOIFile,
        EGIFile=EGIFile,
        temperatureFile=temperatureFile,
        rainfallFile=rainfallFile,
        runoffFile=runoffFile,
        verbose=False)

    assert_almost_equals(result['monthly_rmse_km3'], 1.466297, places=6)
    assert_almost_equals(result['annual_voldiff_pcent'], 2.728310, places=6)
    assert_almost_equals(result['rainfall_km3'], 8.224000, places=6)
    assert_almost_equals(result['SOL_melt_km3'], 2.341036, places=6)
    assert_almost_equals(result['SOI_melt_km3'], 1.781905, places=6)
    assert_almost_equals(result['EGI_melt_km3'], 0.454013, places=6)
    assert_almost_equals(result['runoff_km3'], 13.160000, places=6)


def test_calibration_stats():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_modelEnv_config.ini')
    drainageid = "IN_Hunza_at_DainyorBridge"
    years = [2001, 2002, 2003]
    nstrikes = 1
    min_snow_ddf = 2.0
    max_snow_ddf = 7.0
    min_ice_ddf = 2.0
    max_ice_ddf = 9.0
    rainfall_col = 'rainfall'
    runoff_col = 'runoff'
    myEnv = ModelEnv(tileConfigFile=testConfigFile)
    result = CalibrationStats(
        myEnv, drainageid, years, nstrikes,
        min_snow_ddf, max_snow_ddf, min_ice_ddf, max_ice_ddf,
        rainfall_col=rainfall_col,
        runoff_col=runoff_col)

    SaveCalibrationStats(
        myEnv, drainageid, nstrikes,
        result,
        file=sys.stderr,
        header=True)

    assert_almost_equals(result.at[0, 'monthly_rmse_km3'], 0.642160, places=6)
    assert_almost_equals(result.at[1, 'annual_voldiff_pcent'], 12.927124, places=6)
    assert_almost_equals(result.at[2, 'runoff_km3'], 9.740000, places=6)
    assert_equals(result.at[2, 'model'], '2.0_7.0_2.0_9.0')


def test_calibration_cost():

    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'calibration_modelEnv_config.ini')
    drainageid = "IN_Hunza_at_DainyorBridge"
    years = [2001, 2002, 2003]
    nstrikes = 1
    min_snow_ddf = 2.0
    max_snow_ddf = 7.0
    min_ice_ddf = 2.0
    max_ice_ddf = 9.0
    rainfall_col = 'rainfall'
    runoff_col = 'runoff'
    myEnv = ModelEnv(tileConfigFile=testConfigFile)
    result = CalibrationStats(
        myEnv, drainageid, years, nstrikes,
        min_snow_ddf, max_snow_ddf, min_ice_ddf, max_ice_ddf,
        rainfall_col=rainfall_col,
        runoff_col=runoff_col)

    df, stats_df = CalibrationCost(result)
    cost = stats_df.z.iloc[0]

    assert_almost_equals(cost, 84.412960, places=6)


def test_choose_new_ddfs():

    num = 50

    (min_snow_ddf, max_snow_ddf,
     min_ice_ddf, max_ice_ddf) = RandomNewDDFs(initialize=True)
    print("\ninitial DDFs: %.2f %.2f %.2f %.2f" % (
        min_snow_ddf, max_snow_ddf, min_ice_ddf, max_ice_ddf), file=sys.stderr)

    for i in np.arange(num):
        (min_snow_ddf, max_snow_ddf,
         min_ice_ddf, max_ice_ddf) = RandomNewDDFs(
             min_snow_ddf, max_snow_ddf, min_ice_ddf, max_ice_ddf,
             neighborhood_mm=5.)
        print("     new DDFs: %.2f %.2f %.2f %.2f" % (
            min_snow_ddf, max_snow_ddf, min_ice_ddf, max_ice_ddf), file=sys.stderr)
        assert(min_snow_ddf <= max_snow_ddf)
        assert(min_ice_ddf <= max_ice_ddf)
        assert(min_snow_ddf <= min_ice_ddf)


# def test_plot_tri_surface_melt():

#     # Define test input files and read hypsometries for
#     # SOL, SOI, EGI, temperatures
#     SOLFile = os.path.join(os.path.dirname(__file__),
#                            'test_files',
#                            'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
#     SOIFile = os.path.join(os.path.dirname(__file__),
#                            'test_files',
#                            'test_basin.500.ALBEDO_MCD.0035.snow_on_ice.dat')
#     EGIFile = os.path.join(os.path.dirname(__file__),
#                            'test_files',
#                            'test_basin.500.ALBEDO_MCD.0035.ablation.dat')
#     temperatureFile = os.path.join(os.path.dirname(__file__),
#                                    'test_files',
#                                    'test_basin.500.temperature.dat')
#     outFile = os.path.join(os.path.dirname(__file__),
#                            'test_basin.500.melt.png')

#     (SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps) = TriSurfTempIndexMelt(
#         SOLFile=SOLFile,
#         SOIFile=SOIFile,
#         EGIFile=EGIFile,
#         temperatureFile=temperatureFile,
#         min_snow_ddf=2.,
#         max_snow_ddf=7.,
#         min_ice_ddf=2.,
#         max_ice_ddf=9.)

#     fig, ax = matplotlib.pyplot.subplots(1, 1)
#     ax = PlotTriSurfMelt(ax,
#                          SOL_melt_hyps=SOL_melt_hyps,
#                          SOI_melt_hyps=SOI_melt_hyps,
#                          EGI_melt_hyps=EGI_melt_hyps)

#     ax.set_title('test basin title')
#     fig.tight_layout()
#     fig.savefig(outFile)


# def test_plot_tri_surface_melt():

#     # Define test input files and read hypsometries for
#     # SOL, SOI, EGI, temperatures
#     SOLFile = os.path.join(os.path.dirname(__file__),
#                            'test_files',
#                            'test_basin.500.ALBEDO_MCD.0035.snow_off_ice.dat')
#     SOIFile = os.path.join(os.path.dirname(__file__),
#                            'test_files',
#                            'test_basin.500.ALBEDO_MCD.0035.snow_on_ice.dat')
#     EGIFile = os.path.join(os.path.dirname(__file__),
#                            'test_files',
#                            'test_basin.500.ALBEDO_MCD.0035.ablation.dat')
#     temperatureFile = os.path.join(os.path.dirname(__file__),
#                                    'test_files',
#                                    'test_basin.500.temperature.dat')
#     outFile = os.path.join(os.path.dirname(__file__),
#                            'test_basin.500.tri.imshow.png')

#     (SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps) = TriSurfTempIndexMelt(
#         SOLFile=SOLFile,
#         SOIFile=SOIFile,
#         EGIFile=EGIFile,
#         temperatureFile=temperatureFile,
#         min_snow_ddf=2.,
#         max_snow_ddf=7.,
#         min_ice_ddf=2.,
#         max_ice_ddf=9.)

#     fig, ax = matplotlib.pyplot.subplots(3, 1)
#     ax = ImshowTriSurfMelt(ax,
#                            SOL_melt_hyps=SOL_melt_hyps,
#                            SOI_melt_hyps=SOI_melt_hyps,
#                            EGI_melt_hyps=EGI_melt_hyps)

#     fig.tight_layout()
#     fig.savefig(outFile)
