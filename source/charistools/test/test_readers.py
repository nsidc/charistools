'''
Nose tests for readers routines

To run tests, cd to /vagrant/source and do:
fab [-v] test.all

'''
from __future__ import print_function

from nose.tools import assert_almost_equals
from nose.tools import assert_equals
from nose.tools import assert_raises
import numpy as np
import os

from charistools.readers import ModisTileCube
from charistools.readers import read_tile
from charistools.readers import read_evapotranspiration_tile
from charistools.modelEnv import ModelEnv


def test_read_bin():
    testFile = os.path.join(os.path.dirname(__file__),
                            'test_files/test.3x4.bin')
    data = read_tile(testFile, rows=4, cols=3)

    assert_equals(data.shape, (4, 3))
    assert_equals(data[0][0], 0.)
    assert_equals(data[0][1], 1.)
    assert_equals(data[0][2], 2.)
    assert_equals(data[3][2], 11.)


def test_init_cube():
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir='/projects/CHARIS/charistools_test_data')
    filename = myEnv.forcing_filename(type='modscag_gf', tileID='h23v05',
                                      year=2001)
    cube = ModisTileCube(filename, 'fsca')
    assert_equals(cube.ndays, 365)


def test_read_off_cube():
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir='/projects/CHARIS/charistools_test_data')
    filename = myEnv.forcing_filename(type='modscag_gf', tileID='h23v05',
                                      year=2001)
    cube = ModisTileCube(filename, 'fsca')
    assert_equals(cube.ndays, 365)
    assert_raises(IndexError, cube.read, doy=400)


def test_read_on_cube():
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'modis_tiles_config.ini')
    myEnv = ModelEnv(tileConfigFile=testConfigFile,
                     topDir='/projects/CHARIS/charistools_test_data')
    filename = myEnv.forcing_filename(type='modscag_gf', tileID='h23v05',
                                      year=2001)
    cube = ModisTileCube(filename, 'fsca')
    assert_equals(cube.ndays, 365)
    data = cube.read(doy=100)
    assert_equals(data.shape, (2400, 2400))
    assert_equals(np.amin(data), 0)
    assert_equals(np.amax(data), 1.0)


def test_read_scagv8():

    # Only MODSCAG_GF v8 files do not use the fsca varname, these
    # are a different name ('scf');  before/after v8, the varname is
    # fsca.  This test exercises the cube reader line to detect this
    # and properly return the data cube anyway.
    filename = ''.join(
        ['/projects/CHARIS/charistools_test_data/',
         'snow_cover/MODSCAG_GF/',
         'MODSCAG_GF_Snow.v0.8.h23v05_2001.h5'])
    cube = ModisTileCube(filename, 'fsca')
    assert_equals(cube.ndays, 365)
    cube.close()


def test_read_et():

    # MOD16A2 MODIS ET files
    filename = ''.join(
        ['/projects/CHARIS/charistools_test_data/',
         'evapotranspiration/mod16/monthly/',
         'MOD16A2.A2001M01.h24v05.105.2013121032237.hdf'])
    data = read_evapotranspiration_tile(filename)
    assert_equals(data.shape, (2400, 2400))
    assert_equals(np.amin(data), 0)
    assert_almost_equals(np.amax(data), 2.606e-05, places=8)
