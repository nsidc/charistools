'''
Nose tests for run_forcings
'''
from __future__ import print_function

from nose.tools import assert_equals

from charistools.hypsometry import Hypsometry
from charistools.modelEnv import ModelEnv
from charistools.run_forcings import run_forcings
from click.testing import CliRunner
# from nose.tools import set_trace
import os
import sys


def test_run_forcings_no_modelenv():
    runner = CliRunner()
    result = runner.invoke(run_forcings, ['IN_Hunza_at_Danyour'])
    assert_equals(result.exit_code, -1)
    assert "Requires environment CHARIS_CONFIG_FILE" in result.output


def test_run_forcings_bogus_drainageid():
    # Set up the shell environment for the script to use
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'test_run_forcings_env.ini')
    topDir = '/projects/CHARIS/charistools_test_data'
    os.environ['CHARIS_CONFIG_FILE'] = testConfigFile
    os.environ['CHARIS_DATA_DIR'] = topDir

    runner = CliRunner()
    result = runner.invoke(run_forcings, ['Bogus_drainage'])
    assert "No drainageID mask(s) found" in result.output


def test_run_forcings():

    # Set up the shell environment for the script to use
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'test_run_forcings_env.ini')
    topDir = '/projects/CHARIS/charistools_test_data'
    os.environ['CHARIS_CONFIG_FILE'] = testConfigFile
    os.environ['CHARIS_DATA_DIR'] = topDir
    modelEnv = ModelEnv(tileConfigFile=testConfigFile, topDir=topDir)

    # Choose options to run some forcings
    drainageID = 'AM_Vakhsh_at_Komsomolabad'
    year = 2001
    ablation_method = 'grsize_scag'
    threshold = 205

    # Remove any test files that may be leftover from a previous failed test
    modelEnv = ModelEnv(tileConfigFile=testConfigFile, topDir=topDir)
    temperature_hyps_filename = modelEnv.hypsometry_filename(
        type='temperature_by_elevation',
        drainageID=drainageID,
        year=year)
    sol_hyps_filename = modelEnv.hypsometry_filename(
        type='snow_on_land_by_elevation',
        drainageID=drainageID,
        year=year)
    soi_hyps_filename = modelEnv.hypsometry_filename(
        type='snow_on_ice_by_elevation',
        drainageID=drainageID,
        year=year,
        ablation_method=ablation_method,
        threshold=threshold)
    egi_hyps_filename = modelEnv.hypsometry_filename(
        type='exposed_glacier_ice_by_elevation',
        drainageID=drainageID,
        year=year,
        ablation_method=ablation_method,
        threshold=threshold)
    try:
        os.remove(temperature_hyps_filename)
        os.remove(sol_hyps_filename)
        os.remove(soi_hyps_filename)
        os.remove(egi_hyps_filename)
    except OSError:
        pass

    # Run the forcings script
    runner = CliRunner()
    result = runner.invoke(run_forcings, [drainageID,
                                          '--stop_doy', 1])
    print(result.output, file=sys.stderr)
    assert_equals(result.exit_code, 0)

    # Just check for expected number of columns in output files
    # (Actual values will have been checked in lower-level tests)
    hyps = Hypsometry(filename=temperature_hyps_filename)
    assert_equals(len(hyps.data.columns), 63)
    hyps = Hypsometry(filename=sol_hyps_filename)
    assert_equals(len(hyps.data.columns), 56)
    hyps = Hypsometry(filename=soi_hyps_filename)
    assert_equals(len(hyps.data.columns), 44)
    hyps = Hypsometry(filename=egi_hyps_filename)
    assert_equals(len(hyps.data.columns), 44)

    # Clean up test files
    os.remove(temperature_hyps_filename)
    os.remove(sol_hyps_filename)
    os.remove(soi_hyps_filename)
    os.remove(egi_hyps_filename)


def test_run_forcings_var_thresholds():

    # Set up the shell environment for the script to use
    testConfigFile = os.path.join(os.path.dirname(__file__),
                                  'test_run_forcings_env.ini')
    topDir = '/projects/CHARIS/charistools_test_data'
    os.environ['CHARIS_CONFIG_FILE'] = testConfigFile
    os.environ['CHARIS_DATA_DIR'] = topDir
    modelEnv = ModelEnv(tileConfigFile=testConfigFile, topDir=topDir)

    # Choose options to run some forcings
    drainageID = 'AM_Vakhsh_at_Komsomolabad'
    year = 2001
    ablation_method = 'grsize_scag'
    threshold = 'fromFile'

    # Remove any test files that may be leftover from a previous failed test
    modelEnv = ModelEnv(tileConfigFile=testConfigFile, topDir=topDir)
    temperature_hyps_filename = modelEnv.hypsometry_filename(
        type='temperature_by_elevation',
        drainageID=drainageID,
        year=year)
    sol_hyps_filename = modelEnv.hypsometry_filename(
        type='snow_on_land_by_elevation',
        drainageID=drainageID,
        year=year)
    soi_hyps_filename = modelEnv.hypsometry_filename(
        type='snow_on_ice_by_elevation',
        drainageID=drainageID,
        year=year,
        ablation_method=ablation_method,
        threshold=threshold)
    egi_hyps_filename = modelEnv.hypsometry_filename(
        type='exposed_glacier_ice_by_elevation',
        drainageID=drainageID,
        year=year,
        ablation_method=ablation_method,
        threshold=threshold)
    try:
        os.remove(temperature_hyps_filename)
        os.remove(sol_hyps_filename)
        os.remove(soi_hyps_filename)
        os.remove(egi_hyps_filename)
    except OSError:
        pass

    # Run the forcings script
    runner = CliRunner()
    result = runner.invoke(run_forcings, [drainageID,
                                          '--stop_doy', 1,
                                          '--use_daily_threshold_file', True])
    print(result.output, file=sys.stderr)
    assert_equals(result.exit_code, 0)

    # Just check for expected number of columns in output files
    # (Actual values will have been checked in lower-level tests)
    hyps = Hypsometry(filename=temperature_hyps_filename)
    assert_equals(len(hyps.data.columns), 63)
    hyps = Hypsometry(filename=sol_hyps_filename)
    assert_equals(len(hyps.data.columns), 56)
    hyps = Hypsometry(filename=soi_hyps_filename)
    assert_equals(len(hyps.data.columns), 44)
    hyps = Hypsometry(filename=egi_hyps_filename)
    assert_equals(len(hyps.data.columns), 44)

    # Clean up test files
    os.remove(temperature_hyps_filename)
    os.remove(sol_hyps_filename)
    os.remove(soi_hyps_filename)
    os.remove(egi_hyps_filename)
