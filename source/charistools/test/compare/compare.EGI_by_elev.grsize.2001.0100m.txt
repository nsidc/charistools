# Hypsometry created : 2016-07-04 14:59:48.290892
# Elevations in meters, contour at bottom of elevation band
# Exposed_glacier_ice area in square km
# Files used to derive this hypsometry data:
# h23v05: basin_mask   : /projects/CHARIS/charistools_test_data/basin_masks/IN_Hunza_at_Danyour.basin_mask.h23v05.tif
# h23v05: dem          : /projects/CHARIS/charistools_test_data/SRTMGL3/SRTMGL3.v0.1.h23v05.tif
# h23v05: modice       : /projects/CHARIS/charistools_test_data/modicev04/MODICE.v0.4.h23v05.1strike.min05yr.mask.nc
# h23v05: fSCA         : /projects/CHARIS/charistools_test_data/snow_cover/MODSCAG_GF/MODSCAG_GF_Snow.v0.5.h23v05_2001.h5
# h23v05: ablation_method (threshold=200, unclass_to_egi=None): /projects/CHARIS/charistools_test_data/grain_size/MODSCAG_GF/MODSCAG_GF_GrainSize.v0.6.h23v05_2001.h5
# h24v05: basin_mask   : /projects/CHARIS/charistools_test_data/basin_masks/IN_Hunza_at_Danyour.basin_mask.h24v05.tif
# h24v05: dem          : /projects/CHARIS/charistools_test_data/SRTMGL3/SRTMGL3.v0.1.h24v05.tif
# h24v05: modice       : /projects/CHARIS/charistools_test_data/modicev04/MODICE.v0.4.h24v05.1strike.min05yr.mask.nc
# h24v05: fSCA         : /projects/CHARIS/charistools_test_data/snow_cover/MODSCAG_GF/MODSCAG_GF_Snow.v0.5.h24v05_2001.h5
# h24v05: ablation_method (threshold=200, unclass_to_egi=None): /projects/CHARIS/charistools_test_data/grain_size/MODSCAG_GF/MODSCAG_GF_GrainSize.v0.6.h24v05_2001.h5
49
2900.0 3000.0 3100.0 3200.0 3300.0 3400.0 3500.0 3600.0 3700.0 3800.0 3900.0 4000.0 4100.0 4200.0 4300.0 4400.0 4500.0 4600.0 4700.0 4800.0 4900.0 5000.0 5100.0 5200.0 5300.0 5400.0 5500.0 5600.0 5700.0 5800.0 5900.0 6000.0 6100.0 6200.0 6300.0 6400.0 6500.0 6600.0 6700.0 6800.0 6900.0 7000.0 7100.0 7200.0 7300.0 7400.0 7500.0 7600.0 7700.0
2001 1 1 1 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.21 0.36 0.37 0.67 0.78 1.60 1.88 2.28 1.55 3.76 4.21 3.27 2.62 4.65 2.02 3.55 4.57 3.44 3.78 2.93 2.06 2.51 3.84 2.66 1.92 2.01 0.99 1.12 1.55 0.61 0.40 0.00 0.18 0.20 0.19 0.18 0.17 0.00 0.20 0.00
2001 1 2 2 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.21 0.15 0.38 0.91 0.79 1.60 1.84 2.15 1.56 3.54 3.77 2.37 2.43 4.18 2.62 3.39 4.63 3.64 3.39 3.04 2.12 2.28 3.44 2.45 1.90 1.97 0.97 0.92 1.34 0.59 0.40 0.00 0.00 0.18 0.19 0.18 0.17 0.00 0.18 0.00
2001 1 3 3 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.15 0.38 0.92 0.57 1.21 1.49 2.17 1.70 2.70 3.30 2.19 2.40 3.61 2.41 2.70 3.78 3.61 3.07 2.67 1.72 1.94 2.94 1.69 1.68 1.59 1.11 0.92 1.15 0.58 0.39 0.17 0.20 0.17 0.19 0.18 0.17 0.00 0.17 0.00
