The .dat files in the fromIDL/ directory were produced using the IDL
gen_hypsometry routines and earlier versions of the scag_gf and
scag grain size data.

The corresponding .txt files in this directory are files that
I've now produced in python for the same period.

For the first 3 days of 2001, the IDL and python files  only differ
in a few ways:

1) Small formatting differences (IDL file column headers have a
trailing decimal point, python have "*.0" and I can't figure out
how to tell python not to do this); and IDL date columns are
zero-padded but python is not..

2) python data only produces data for elevations with non-zero
values, but IDL data were hardcoded to always produce the same
set of elevations and fill them with zeros.

--> the python way is fine, because python pandas DataFrames
    handle these differences

3) A few of the actual values in the snow-on-land file are
different, for only 6 or 7 elevation bands and at most +/- 1 km^2
different, which is entirely reasonable given the changes made to
the input data sets (based on discussion with Karl Ritter, March
2016).

4) Actual values in the SOI file are the same.

****************************
The original sDDF data files from IDL had a bug in the sDDF
calculator, they were all using 366 days in year and doy=80 for
equinox.  New, "correct_sDDF" data files are produced in python
with correct values for days in year and equinox day as function
of leap year.

