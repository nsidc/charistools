#!/usr/bin/env python
""" Create MODIS tile masks from a set of input polygon shapefiles.
    All input shapefiles should be considered as one collection of shapes to be
    burned into the masks.
"""

import sys
import argparse


import rasterio
from rasterio import features
import fiona
import shapely.geometry as shg
from affine import Affine


import modis_tile_bounds


def setup_argument_parser():
    """Set up command line options.  -h or --help for help is automatic"""
    p = argparse.ArgumentParser(description='Generate MODIS tile masks from input shapefiles')

    p.add_argument('-a', '--attribute', default=None,
                   help='Attribute name to use in mask filename. '
                   'Final mask name will be OUTBASE_<attr_name><attr_value>_<tilelabel>.tif')
    p.add_argument('-b', '--bbox',   action='store_true', default=False,
                   help="Determine tiles from shpfile BBOX rather than features therein")
    p.add_argument('-d', '--diffval',   action='store_true', default=False,
                   help="Use series of integers for mask values. Note that this can \
                         overflow and cause the program to crash for shapefiles with \
                         large numbers of polygons.")
    p.add_argument('-f', '--fillval', default=0, type=int, help='Fill value for "outside" areas')
    p.add_argument('-g', '--global_tiles',   action='store_true', default=False,
                   help="Consider all tiles on globe rather than only CHARIS region tiles")
    p.add_argument('-m', '--maskval', default=1, type=int, help='Mask value for "inside" areas')
    p.add_argument('-o', '--outbase', default='MODIS_tile',  help='Output file name base')
    p.add_argument('-s', '--separate', action='store_true',
                   help='Make a separate set of masks for each polygon in input shapefiles')
    p.add_argument('infiles', metavar='shpfile', type=str, nargs='+',
                   help='Basin shapefile, in sinusoidal projection')
    p.add_argument('-q', '--quiet',   action='store_true', default=False,
                   help="Quiet mode.  Don't print status messages")
    return(p)


def print_arg_summary(opts):
    """Print summary of command-line arguments."""
    print("Arguments object:")
    print(opts)


def make_one_mask_from_shapes(all_shapes, out_fn, opts, meta=None, rst_fn=None):
    """ given a shapefile and information about
    a single MODIS tile, make a mask for that tile from the shapefile.
    """

    print("Sanity check:  Number of input shapes:  ", len(all_shapes))

    if meta is None and rst_fn is None:
        print("Need information about a tile, either a metadata dictionary or a tile file.")
        sys.exit(1)

    if meta is None:
        rst = rasterio.open(rst_fn)
        meta = rst.meta
        meta.update(compress='lzw')

    if not opts['quiet']:
        print("Tile Info:\n", meta)

    # Open the output file, create the mask, and write it out.
    with rasterio.open(out_fn, 'w', **meta) as out:
        out_arr = out.read(1)
        burned = features.rasterize(shapes=all_shapes, fill=opts['fillval'],
                                    out=out_arr, transform=out.affine)
        out.write_band(1, burned)


def create_meta_struct_for_tile(label):
    """ Given a MODIS tile label like h24v05, create a Rasterio metadata struct
        for the tile.
    """
    corners = modis_tile_bounds.corners_from_label(label)

    (upper_left, upper_right) = corners[0]
    modis_crs = {u'a': 6371007.181, u'lon_0': 0, u'no_defs': True,
                 u'y_0': 0, u'b': 6371007.181, u'proj': u'sinu',
                 u'x_0': 0, u'units': u'm'}
    pix_size = 463.3127165279169
    affine = Affine(pix_size, 0.0, upper_left, 0.0, -pix_size, upper_right)
    transform = (upper_left, pix_size, 0.0, upper_right, 0.0, -pix_size)

    meta = {
            'count': 1,
            'crs': modis_crs,
            'dtype': 'uint8',
            'affine': affine,
            'transform': transform,
            'driver': u'GTiff',
            'height': 2400,
            'width': 2400,
            'blockxsize': 2400,
            'tiled': False,
            'blockysize': 3,
            'nodata': None,
            'compress': 'lzw',
           }
    return meta


def get_metas_from_poly(poly, global_tiles=False):
    """ Determine tiles (and their metadata) that intersect the input poly
        (sinusoidal coordinates) and return a dict of metadata objects keyed by
        label
    """
    tile_meta_dict = {}

    if global_tiles:
        tiles_to_check = modis_tile_bounds.get_all_tile_labels()
    else:
        tiles_to_check = modis_tile_bounds.get_all_charis_tile_labels()

    for label in tiles_to_check:
        corners = modis_tile_bounds.corners_from_label(label)
        tile_poly = shg.Polygon(corners)
        if tile_poly.intersects(poly):
            tile_meta_dict[label] = create_meta_struct_for_tile(label)

    return tile_meta_dict


def get_shapes_and_bbox(opts):
    # Create list of all shapes from all files, and create bounding box for all
    # at the same time

    all_shapes = []
    all_attrib = []
    all_bbox = None
    mask_val = opts['maskval']

    for infile in opts['infiles']:
        with fiona.open(infile) as fh:

            if not opts['quiet']:
                print("Number of features in {}:  {}".format(infile, len(fh)))

            corners = fh.bounds

            if all_bbox:
                all_bbox = all_bbox.union(shg.box(*corners))
            else:
                all_bbox = shg.box(*corners)

            for feature in fh:
                all_shapes.append((feature['geometry'].copy(), mask_val))
                all_attrib.append(feature['properties'].copy())
                if opts['diffval']:
                    mask_val += 1

    if not opts['quiet']:
        print("Total number of features in input shapefiles:  ", len(all_shapes))

    return(all_shapes, all_attrib, all_bbox)


def get_overlapping_tiles_from_all_bbox(all_bbox, global_tiles=False):
    tile_meta = {}
    tile_meta.update(get_metas_from_poly(all_bbox, global_tiles))
    return tile_meta


def get_overlapping_tiles_from_shapes(in_shapes, global_tiles=False):
    tile_meta = {}
    for outline in in_shapes:
        poly = shg.shape(outline['geometry'])
        tile_meta.update(get_metas_from_poly(poly, global_tiles))
    return tile_meta


def make_all_masks(opts):
    """ Given a set of input shapefiles of polygons, determine which tiles are
    overlapped and create a mask for each one.
    """

    all_shapes, all_attrib, all_bbox = get_shapes_and_bbox(opts)

    attr_name = 'HydroID'
    if opts['attribute']:
        attr_name = opts['attribute']

    if opts['separate']:
        if not opts['quiet']:
            print("Creating separate set of masks for each input shape")

        for shape, attrib in zip(all_shapes, all_attrib):
            tile_meta = get_metas_from_poly(shg.shape(shape[0]), opts['global_tiles'])
            ident = str(attrib[attr_name])
            for label in sorted(tile_meta):
                outfile = opts['outbase'] + attr_name + ident + '_' + label + '.tif'
                if not opts['quiet']:
                    print("  Creating ", outfile)
                make_one_mask_from_shapes([shape], outfile, opts, meta=tile_meta[label])

    else:
        if not opts['quiet']:
            print("Creating one set of masks for whole set of input shapes")

        # Create dictionary of tile meta information keyed by tile label
        tile_meta = {}
        if opts['bbox']:
            tile_meta = get_overlapping_tiles_from_all_bbox(all_bbox, opts['global_tiles'])
        else:
            tile_meta = get_overlapping_tiles_from_shapes(all_shapes, opts['global_tiles'])

        if not opts['quiet']:
            print("Tile metadata:\n", tile_meta)

        # Loop through tiles and create a mask from all shapes

        all_shapes = list(all_shapes)
        for label in sorted(tile_meta):
            outfile = opts['outbase'] + '_' + label + '.tif'
            if not opts['quiet']:
                print("  Creating ", outfile)
            make_one_mask_from_shapes(all_shapes, outfile, opts, meta=tile_meta[label])


def main():
    """Main entry point of the program."""
    p = setup_argument_parser()

    # Parse command-line.
    args = p.parse_args()

    # convert to dictionary for greater flexibility
    opts = vars(args)

    if not opts['quiet']:
        print_arg_summary(opts)

    make_all_masks(opts)


if __name__ == '__main__':
    main()
