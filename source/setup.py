from setuptools import setup, find_packages

setup(name='charistools',
      version='0.6.0',
      description='Python tools from the NSIDC CHARIS project',
      url='git@bitbucket.org:nsidc/charistools.git',
      author='NSIDC CHARIS Team',
      author_email='brodzik@nsidc.org',
      license='GPLv3',
      packages=find_packages(exclude=('fabfile',)))
