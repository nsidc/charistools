from fabric.api import local, task, execute


@task(default=True)
def all():
    """
    run all of the tests
    """

    execute(flake8)
    execute(unittest)


@task
def flake8():
    """
    run flake8 syntax checking
    """

    local(
        "flake8 --verbose "
        "$(find . -name '*.py' -not -path './charistools/test/debug_tests/*')")


@task
def unittest():
    """
    run unittests using nose
    """

    local("nosetests --verbose --where=charistools")
